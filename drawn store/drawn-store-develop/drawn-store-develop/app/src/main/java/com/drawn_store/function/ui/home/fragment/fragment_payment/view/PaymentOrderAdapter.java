package com.drawn_store.function.ui.home.fragment.fragment_payment.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.drawn_store.R;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.objects.PaymentOrderInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentOrderAdapter extends RecyclerView.Adapter<PaymentOrderAdapter.ViewHodel> {

    private Context context;
    private List<PaymentOrderInfo> paymentOrderInfos;
    private SetDataOrder setDataOrder;

    public PaymentOrderAdapter(Context context) {
        this.context = context;
    }

    public void swap(List<PaymentOrderInfo> paymentOrderInfos) {
        this.paymentOrderInfos = paymentOrderInfos;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PaymentOrderAdapter.ViewHodel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_payment_order, parent, false);
        return new ViewHodel(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull PaymentOrderAdapter.ViewHodel holder, int position) {
        PaymentOrderInfo paymentOrderInfo = paymentOrderInfos.get(position);
        holder.tvNameProducts.setText(paymentOrderInfo.getName());
        holder.tvSize.setText(paymentOrderInfo.getColor() + paymentOrderInfo.getSize());
        holder.tvAmount.setText("SL: " + paymentOrderInfo.getAmount());
        double price;
        double saving;
        if (paymentOrderInfo.getDiscountPercent().equals("0%")) {
            holder.tvPric.setVisibility(View.GONE);
            holder.tvDiscountPercent.setVisibility(View.GONE);
            holder.tvCountSale.setVisibility(View.GONE);
            holder.tvDiscountPric.setText(Utils.formatAmount(Double.parseDouble(paymentOrderInfo.getPrice())));
            price = Double.parseDouble(paymentOrderInfo.getPrice()) * paymentOrderInfo.getAmount();
        } else {
            holder.tvPric.setVisibility(View.VISIBLE);
            holder.tvDiscountPercent.setVisibility(View.VISIBLE);
            holder.tvCountSale.setVisibility(View.VISIBLE);
            holder.tvDiscountPric.setText(Utils.formatAmount(Double.parseDouble(paymentOrderInfo.getDiscountPrice())));
            holder.tvPric.setText(Utils.formatAmount(Double.parseDouble(paymentOrderInfo.getPrice())));
            holder.tvDiscountPercent.setText("-" + paymentOrderInfo.getDiscountPercent());
            price = Double.parseDouble(paymentOrderInfo.getDiscountPrice()) * paymentOrderInfo.getAmount();
            saving = (Double.parseDouble(paymentOrderInfo.getPrice()) * paymentOrderInfo.getAmount()) - Double.parseDouble(paymentOrderInfo.getDiscountPrice()) * paymentOrderInfo.getAmount();
            holder.tvCountSale.setText("Tiết kiệm: " + Utils.formatAmount(saving));
        }
        setDataOrder.dataOrder(price, paymentOrderInfo,paymentOrderInfo.getAmount());
        holder.tvTotal.setText(Utils.formatAmount(price));
        holder.tvSizeNote.setText(paymentOrderInfo.getAmount() + " Sản phẩm, tổng số:");
        Glide.with(context)
                .load(ConstantApi.MAIN_DNS + "/" + paymentOrderInfo.getImages())
                .transform(new CenterCrop(), new RoundedCorners(8))
                .error(R.drawable.logo)
                .into(holder.imgProducts);

    }

    @Override
    public int getItemCount() {
        return paymentOrderInfos.size();
    }

    public void setOnDataOrder(SetDataOrder setDataOrder) {
        this.setDataOrder = setDataOrder;
    }

    public class ViewHodel extends RecyclerView.ViewHolder {
        @BindView(R.id.imgProducts)
        ImageView imgProducts;
        @BindView(R.id.tvNameProducts)
        TextView tvNameProducts;
        @BindView(R.id.tvDiscountPric)
        TextView tvDiscountPric;
        @BindView(R.id.tvPric)
        TextView tvPric;
        @BindView(R.id.tvSize)
        TextView tvSize;
        @BindView(R.id.tvDiscountPercent)
        TextView tvDiscountPercent;
        @BindView(R.id.tvAmount)
        TextView tvAmount;
        @BindView(R.id.tvTotal)
        TextView tvTotal;
        @BindView(R.id.tvSizeNote)
        TextView tvSizeNote;
        @BindView(R.id.tvCountSale)
        TextView tvCountSale;

        public ViewHodel(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvPric.setPaintFlags(tvPric.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }

    public interface SetDataOrder {
        void dataOrder(double total, PaymentOrderInfo paymentOrderInfo,int amount);
    }
}
