package com.drawn_store.function.ui.home.fragment.fragment_payment.presenter;


import com.drawn_store.function.common.objects.request.LikeRep;
import com.drawn_store.function.common.objects.request.OrderReq;

public interface OrderPresenter {
    void order(OrderReq orderReq);
    void showOrder(int userId,String filter);
}
