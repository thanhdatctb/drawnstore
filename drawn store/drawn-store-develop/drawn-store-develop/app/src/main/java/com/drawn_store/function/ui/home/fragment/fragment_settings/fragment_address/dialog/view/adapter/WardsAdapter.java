package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.dialog.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.drawn_store.R;
import com.drawn_store.common.pref.PrefManager;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.function.common.constants.FConstants;
import com.drawn_store.function.common.objects.WardsInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WardsAdapter extends RecyclerView.Adapter<WardsAdapter.ViewHodel> {

    private Context context;
    private List<WardsInfo> wardsInfos;
    private ClickItemNameWards clickItemName;
    private String nameWards;
    public WardsAdapter(Context context, String nameWards) {
        this.context = context;
        this.nameWards = nameWards;
    }

    public void swap(List<WardsInfo> wardsInfos) {
        this.wardsInfos = wardsInfos;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public WardsAdapter.ViewHodel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_text_address, parent, false);
        return new ViewHodel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WardsAdapter.ViewHodel holder, int position) {
        WardsInfo wardsInfo = wardsInfos.get(position);
        PrefManager prefManager = new PrefManager(context);
        holder.tvTextAddress.setText(wardsInfo.getName());
        String personJsonString = Utils.getGsonParser().toJson(wardsInfo);
        if (wardsInfo.getName().equals(nameWards)){
            holder.imgCheck.setVisibility(View.VISIBLE);
        }else {
            holder.imgCheck.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.imgCheck.setVisibility(View.VISIBLE);
                clickItemName.setNameWards(view,position,wardsInfo.getName());
                prefManager.saveSetting(FConstants.KEY_JSON_WARDS,personJsonString);
            }
        });
    }
    public void setOnClickName(ClickItemNameWards clickName){
        this.clickItemName = clickName;
    }


    @Override
    public int getItemCount() {
        return wardsInfos.size();
    }

    public class ViewHodel extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTextAddress)
        TextView tvTextAddress;

        @BindView(R.id.imgCheck)
        ImageView imgCheck;

        public ViewHodel(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imgCheck.setVisibility(View.GONE);
        }
    }

    public interface ClickItemNameWards{
        void setNameWards(View view, int position, String name);
    }
}
