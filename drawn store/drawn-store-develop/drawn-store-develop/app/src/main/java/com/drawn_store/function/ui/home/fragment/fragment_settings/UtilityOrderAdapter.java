package com.drawn_store.function.ui.home.fragment.fragment_settings;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.drawn_store.R;
import com.drawn_store.function.common.objects.UtilityOrder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UtilityOrderAdapter extends RecyclerView.Adapter<UtilityOrderAdapter.ViewHodel> {

    private Context context;
    private List<UtilityOrder> utilityOrderList;
    private ClickOnItem clickOnItem;
    public UtilityOrderAdapter(Context context) {
        this.context = context;
    }
    public void swap(List<UtilityOrder> utilityOrderList){
        this.utilityOrderList = utilityOrderList;
    }

    @NonNull
    @Override
    public UtilityOrderAdapter.ViewHodel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_utility_order,parent,false);
        return new UtilityOrderAdapter.ViewHodel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UtilityOrderAdapter.ViewHodel holder, int position) {
        UtilityOrder utilityOrder = utilityOrderList.get(position);
        holder.tvName.setText(utilityOrder.getName());
        holder.imgIcon.setImageResource(utilityOrder.getIcon());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickOnItem.clickPositionItem(view,position,utilityOrder.getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return utilityOrderList.size();
    }
    public void setOnClickItem(ClickOnItem clickItem){
        this.clickOnItem = clickItem;
    }

    public class ViewHodel extends RecyclerView.ViewHolder {

        @BindView(R.id.imgIcon)
        ImageView imgIcon;
        @BindView(R.id.tvName)
        TextView tvName;


        public ViewHodel(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    public interface ClickOnItem{
        void clickPositionItem(View view,int position,String name);
    }
}
