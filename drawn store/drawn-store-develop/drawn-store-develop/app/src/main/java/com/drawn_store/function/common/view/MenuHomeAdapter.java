package com.drawn_store.function.common.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.drawn_store.R;
import com.drawn_store.function.common.objects.MenuHome;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuHomeAdapter extends RecyclerView.Adapter<MenuHomeAdapter.ViewHolder> {
    private Context context;
    private List<MenuHome> menuHomeList;
    private ItemMenuClickListener itemClickListener;
    public MenuHomeAdapter(Context context) {
        this.context = context;
    }

    public void swap(List<MenuHome> menuHomeList) {
        this.menuHomeList = menuHomeList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MenuHomeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_menu_home, parent, false);
        return new MenuHomeAdapter.ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MenuHomeAdapter.ViewHolder holder, int position) {
        MenuHome menuHome = menuHomeList.get(position);
        holder.tvName.setText(menuHome.getName());
        holder.imgMenu.setImageResource(menuHome.getImage());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onClickItem(view,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return menuHomeList.size();
    }
    public void setOnClickListener(ItemMenuClickListener itemClickListener){
        this.itemClickListener = itemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.imgMenu)
        ImageView imgMenu;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
   public interface  ItemMenuClickListener{
        void onClickItem(View v, int position);
    }
}
