package com.drawn_store.function.common.objects.response;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.function.common.objects.ProductsInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductsTypeLineId extends BaseObj {

    @SerializedName("data")
    @Expose
    private List<ProductsInfo> data = null;

    public List<ProductsInfo> getData() {
        return data;
    }

    public void setData(List<ProductsInfo> data) {
        this.data = data;
    }

}
