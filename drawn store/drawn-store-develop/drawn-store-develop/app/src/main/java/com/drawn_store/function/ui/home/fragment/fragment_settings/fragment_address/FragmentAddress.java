package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.drawn_store.R;
import com.drawn_store.common.api.ErrorCode;
import com.drawn_store.common.base.BaseFragment;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.function.common.objects.ListAddressInfo;
import com.drawn_store.function.common.objects.response.ListAddressRes;
import com.drawn_store.function.common.view.BackReload;
import com.drawn_store.function.ui.home.fragment.fragment_detail.FragmentDetailProduct;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.dialog.presenter.IAddressPresenterImpl;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentAddress extends BaseFragment implements View.OnClickListener,
        ListAddressAdapter.ClickEdit,BackReload {

    public FragmentAddress newInstance() {
        return this;
    }

    @BindView(R.id.rlAddress)
    RelativeLayout rlAddress;

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.rvAddressShow)
    RecyclerView rvAddressShow;
    private IAddressPresenterImpl iAddressPresenterImpl;
    private ListAddressAdapter listAddressAdapter;
    private List<ListAddressInfo> listAddressInfos;
    private boolean checkDefault = true;
    private BackReload backReload;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_address, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        isHideButtonBack(false);
        backgroudToolbar(true);
        setTitle(R.string.address);
        listAddressInfos = new ArrayList<>();
        iAddressPresenterImpl = new IAddressPresenterImpl(this);
        iAddressPresenterImpl.listAddres(loginRes.getData().getId());
        setListener();
        showProgress();
        loadSwipe();
    }
    public FragmentAddress setReloadFragment(BackReload backReload) {
        this.backReload = backReload;
        return this;
    }
    @Override
    public void success(Object x) {
        super.success(x);
        hideProgress();
        if (x instanceof ListAddressRes) {
            ListAddressRes listAddressRes = (ListAddressRes) x;
            listAddressInfos = listAddressRes.getData();
            listAddressAdapter = new ListAddressAdapter(getContext());
            rvAddressShow.setHasFixedSize(true);
            rvAddressShow.setLayoutManager(new LinearLayoutManager(getContext()));
            rvAddressShow.setAdapter(listAddressAdapter);
            listAddressAdapter.setOnClickEdt(this);
            listAddressAdapter.swap(listAddressInfos);
            checkDefault = false;
        }
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj err = (BaseObj) o;
        if (err.getStatusCode() == ErrorCode.CODE_NOT_FOUND) {
            DialogUtil.showDialogMessage(getContext(), getContext().getResources().getString(R.string.data_null));
            checkDefault = true;
        } else {
            DialogUtil.showDialogMessage(getContext(), err.getMessage() + " (" + err.getStatusCode() + ")");
        }
        swipeRefresh.setRefreshing(false);
    }

    private void setListener() {
        rlAddress.setOnClickListener(this);
    }
    private void loadSwipe() {
        swipeRefresh.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorRed,
                R.color.colorBlack);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                iAddressPresenterImpl.listAddres(loginRes.getData().getId());
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlAddress:
                replaceFragment(new FragmentEnterAddress().newInstance(checkDefault).checkAdd(true).setReloadFragment(this), true, false);
                break;
        }
    }

    @Override
    public void setEdit(View view, ListAddressInfo addressInfo, int postion) {
        replaceFragment(new FragmentEnterAddress().newInstanceUpdate(addressInfo).checkAdd(false).setReloadFragment(this), true, false);
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        backReload.reloadFragment();
    }

    @Override
    public void reloadFragment() {
        showProgress();
        iAddressPresenterImpl.listAddres(loginRes.getData().getId());
    }
}
