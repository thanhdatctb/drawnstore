package com.drawn_store.function.common.objects.response;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.function.common.objects.ListAddressInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateAddressRes extends BaseObj {

    @SerializedName("data")
    @Expose
    private ListAddressInfo data = null;

    public ListAddressInfo getData() {
        return data;
    }

    public void setData(ListAddressInfo data) {
        this.data = data;
    }
}
