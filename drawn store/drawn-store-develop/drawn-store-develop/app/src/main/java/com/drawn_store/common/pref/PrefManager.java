package com.drawn_store.common.pref;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Set;

@SuppressLint("ApplySharedPref")
public class PrefManager {
    private Context context;

    public PrefManager(Context context) {
        this.context = context;
    }

    private static final String PREF_FILE = "drawn_store_system_pref";

    private SharedPreferences getSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void saveSetting(String key, String value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void saveSetting(String key, int value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public void saveSetting(String key, boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void saveSetting(String key, float value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    public void saveSetting(String key, long value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public void saveSetting(String key, Set<String> value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putStringSet(key, value);
        editor.commit();
    }


    /**
     * ============== get method =====================
     */
    public String getString(String key) {
        return getSharedPreferences().getString(key, null);
    }

    public String getString(String key, String defaultValue) {
        return getSharedPreferences().getString(key, defaultValue);
    }

    public int getInt(String key, int defaultValue) {
        return getSharedPreferences().getInt(key, defaultValue);
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return getSharedPreferences().getBoolean(key, defaultValue);
    }

    public boolean remove() {
        try {
            SharedPreferences.Editor editor = getSharedPreferences().edit();
            editor.clear();
            return editor.commit();
        } catch (NullPointerException e) {
            return false;
        }
    }

    public boolean remove(String key) {
        try {
            SharedPreferences.Editor editor = getSharedPreferences().edit();
            editor.remove(key);
            return editor.commit();
        } catch (NullPointerException e) {
            return false;
        }
    }

}
