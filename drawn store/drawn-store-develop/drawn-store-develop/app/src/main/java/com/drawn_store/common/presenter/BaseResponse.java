package com.drawn_store.common.presenter;

public interface BaseResponse<T, E> {
    void success(T x);

    /**
     * handle error
     *
     * @param e of request. sample json format error, http error (400, 404, ...)
     */
    void error(E e, boolean isShowDialog);
}
