package com.drawn_store.function.common.objects.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckEmailRep {
    @SerializedName("email")
    @Expose
    private String email;

    public CheckEmailRep() {
    }

    public CheckEmailRep(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
