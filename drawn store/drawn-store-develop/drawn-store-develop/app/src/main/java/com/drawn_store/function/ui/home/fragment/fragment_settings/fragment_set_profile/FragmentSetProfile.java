package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_set_profile;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.drawn_store.R;
import com.drawn_store.common.base.BaseFragment;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.common.view.DialogMessage;
import com.drawn_store.common.view.ToastUtils;
import com.drawn_store.function.common.objects.response.UserRes;
import com.drawn_store.function.common.view.BackReload;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.FragmentAddress;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_info_account.FragmentInfoAccount;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_info_devices.FragmentInfoDevices;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_introduce.FragmentIntroduce;
import com.drawn_store.function.ui.login.presenter.LoginPresenterImpl;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentSetProfile extends BaseFragment implements View.OnClickListener, BackReload {

    public FragmentSetProfile newInstance() {
        return this;
    }

    @BindView(R.id.rlInfoAccount)
    RelativeLayout rlInfoAccount;
    @BindView(R.id.rlAddress)
    RelativeLayout rlAddress;
    @BindView(R.id.rlSupport)
    RelativeLayout rlSupport;
    @BindView(R.id.rlIntroduce)
    RelativeLayout rlIntroduce;
    @BindView(R.id.rlInfoDevices)
    RelativeLayout rlInfoDevices;
    @BindView(R.id.rlDeleteAccount)
    RelativeLayout rlDeleteAccount;
    private LoginPresenterImpl mLoginPresenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_set_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        isHideButtonBack(false);
        backgroudToolbar(true);
        setTitle(R.string.set_profile);
        mLoginPresenter = new LoginPresenterImpl(this);
        setListener();
    }

    private void setListener() {
        rlInfoAccount.setOnClickListener(this);
        rlAddress.setOnClickListener(this);
        rlSupport.setOnClickListener(this);
        rlIntroduce.setOnClickListener(this);
        rlInfoDevices.setOnClickListener(this);
        rlDeleteAccount.setOnClickListener(this);
    }

    @Override
    public void success(Object x) {
        super.success(x);
        hideProgress();
        if (x instanceof UserRes) {
            ToastUtils.showToast(requireContext(),"Đã xóa tài khoản",Gravity.BOTTOM);
            prefManager.remove();
            navigator.screenLogin(getActivity());
        }
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj err = (BaseObj) o;
        if (err.getStatusCode() == 400) {
            DialogUtil.showDialogMessage(requireContext(), getResources().getString(R.string.username_or_passowrd_incorrect));
        } else if (err.getStatusCode() == 404) {
            DialogUtil.showDialogMessage(requireContext(), getResources().getString(R.string.username_does_not_exist));
        } else {
            DialogUtil.showDialogMessage(requireContext(), err.getMessage());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlInfoAccount:
                replaceFragment(new FragmentInfoAccount().newInstance(), true, false);
                break;
            case R.id.rlAddress:
                replaceFragment(new FragmentAddress().newInstance().setReloadFragment(this), true, false);
                break;
            case R.id.rlSupport:
                break;
            case R.id.rlIntroduce:
                replaceFragment(new FragmentIntroduce().newInstance(), true, false);
                break;
            case R.id.rlInfoDevices:
                replaceFragment(new FragmentInfoDevices().newInstance(), true, false);
                break;
            case R.id.rlDeleteAccount:
                deleteAccount();
                break;
        }
    }

    private void deleteAccount() {
        DialogUtil.showDialogMessage(requireContext(), R.string.title_delete_account, R.string.note_delete_account,
                R.string.no,
                R.string.yes, new DialogMessage.OnDialogMessageListener() {
                    @Override
                    public void yesClick() {
                        mLoginPresenter.deleteAccount(loginRes.getData().getId());
                        showProgress();
                    }
                });
    }

    @Override
    public void reloadFragment() {

    }
}
