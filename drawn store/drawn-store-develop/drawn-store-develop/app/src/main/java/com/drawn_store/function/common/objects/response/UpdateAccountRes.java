package com.drawn_store.function.common.objects.response;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.function.common.objects.ListAddressInfo;
import com.drawn_store.function.common.objects.UpdateAccountInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateAccountRes extends BaseObj {

    @SerializedName("data")
    @Expose
    private UpdateAccountInfo data = null;

    public UpdateAccountInfo getData() {
        return data;
    }

    public void setData(UpdateAccountInfo data) {
        this.data = data;
    }
}
