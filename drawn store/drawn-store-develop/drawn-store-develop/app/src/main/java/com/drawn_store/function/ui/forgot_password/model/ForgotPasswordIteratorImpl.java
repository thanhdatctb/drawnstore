package com.drawn_store.function.ui.forgot_password.model;

import com.drawn_store.common.api.callback.MyCallback;
import com.drawn_store.common.base.BaseInteractorImpl;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.InteractorCallback;
import com.drawn_store.function.common.api.ApiUtils;
import com.drawn_store.function.common.api.request.UserRequest;
import com.drawn_store.function.common.objects.request.ChangerPasswordForgotRep;
import com.drawn_store.function.common.objects.request.EmployeeReq;
import com.drawn_store.function.common.objects.response.UserCodeRes;
import com.drawn_store.function.common.objects.response.UserRes;


import retrofit2.Call;
import retrofit2.Response;


public class ForgotPasswordIteratorImpl extends BaseInteractorImpl<BaseObj, Object> implements IForgotPasswordIterator {

    UserRequest userRequest;

    public ForgotPasswordIteratorImpl(InteractorCallback callback) {
        super(callback);
        userRequest = ApiUtils.getUserRequest();
    }

    @Override
    public void updatePassword(ChangerPasswordForgotRep changerPasswordForgotRep) {
        userRequest.changerPasswordForGot(changerPasswordForgotRep).enqueue(new MyCallback<UserRes>() {
            @Override
            public void onSuccess(Call<UserRes> call, Response<UserRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<UserRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }
}
