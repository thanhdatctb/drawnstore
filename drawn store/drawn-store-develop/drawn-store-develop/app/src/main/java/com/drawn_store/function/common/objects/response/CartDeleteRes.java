package com.drawn_store.function.common.objects.response;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.function.common.objects.CartInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartDeleteRes extends BaseObj {

    @SerializedName("data")
    @Expose
    private List<CartInfo> data = null;

    public List<CartInfo> getData() {
        return data;
    }

    public void setData(List<CartInfo> data) {
        this.data = data;
    }

}
