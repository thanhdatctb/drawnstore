package com.drawn_store.common.ultility;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.animation.Animation;

import com.drawn_store.R;

public class AnimationUtils {
    public static void animationRightToLeftActivity(Activity activity) {
        activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    public static void animationLeftToRightActivity(Activity activity) {
        activity.overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    public static void startAnimation(Context context, View view, int anim )
    {
        Animation animOpenPopup = android.view.animation.AnimationUtils.loadAnimation(context, anim);
        animOpenPopup.reset();
        view.clearAnimation();
        view.startAnimation(animOpenPopup);

    }

}
