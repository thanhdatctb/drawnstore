package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.dialog.model;

import com.drawn_store.function.common.objects.request.AddressRep;
import com.drawn_store.function.common.objects.request.UpdateAddressRep;

public interface IAddressIterator {
    void provinces();
    void district(String code );
    void wards(String code );
    void listAddres(int userId );
    void addAddress(AddressRep addressRep);
    void deleteAddress(int infoId);
    void updateAddress(UpdateAddressRep updateAddressRep);
}
