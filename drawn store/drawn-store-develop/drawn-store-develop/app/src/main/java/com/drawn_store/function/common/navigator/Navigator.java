package com.drawn_store.function.common.navigator;

import android.app.Activity;
import android.content.Intent;

import com.drawn_store.function.ui.home.MainActivity;
import com.drawn_store.function.ui.login.view.LoginActivity;
import com.drawn_store.function.ui.register.view.RegisterActivity;

public class Navigator {

    public void screenMain(Activity activity){
        Intent intent = new Intent(activity, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

    public void screenLogin(Activity activity){
        Intent intent = new Intent(activity, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
    }
    public void screenRegistration(Activity activity){
        Intent intent = new Intent(activity, RegisterActivity.class);
        activity.startActivity(intent);
    }
}
