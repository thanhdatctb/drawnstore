package com.drawn_store.function.ui.home.fragment.fragment_cart.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.drawn_store.R;
import com.drawn_store.common.other.swipe.lib.RecyclerSwipeAdapter;
import com.drawn_store.common.other.swipe.lib.SwipeLayout;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.objects.CartInfo;
import com.drawn_store.function.common.objects.PaymentOrderInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartAdapter extends RecyclerSwipeAdapter<CartAdapter.ViewHodel> {
    private List<CartInfo> cartInfoList;
    private Context context;
    private PriceCheck priceCheck;
    private List<PaymentOrderInfo> paymentOrderInfoList;

    public CartAdapter(Context context) {
        this.context = context;
        paymentOrderInfoList = new ArrayList<>();
    }

    public void swap(List<CartInfo> cartInfoList) {
        this.cartInfoList = cartInfoList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CartAdapter.ViewHodel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_cart, parent, false);
        return new ViewHodel(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull CartAdapter.ViewHodel holder, int position) {
        CartInfo cartInfo = cartInfoList.get(position);
        holder.tvNameProducts.setText(cartInfo.getName());
        holder.tvSize.setText(cartInfo.getColor() + cartInfo.getSize());
        holder.edCount.setText(String.valueOf(cartInfo.getAmount()));
        holder.checkItem.setChecked(cartInfo.isSelected());
        double price;
        if (cartInfo.getDiscountPercent().equals("0%")) {
            holder.tvPric.setVisibility(View.GONE);
            holder.tvDiscountPric.setText(Utils.formatAmount(Double.parseDouble(cartInfo.getPrice())));
            price = Double.parseDouble(cartInfo.getPrice()) * cartInfo.getAmount();

        } else {
            holder.tvPric.setVisibility(View.VISIBLE);
            holder.tvDiscountPric.setText(Utils.formatAmount(Double.parseDouble(cartInfo.getDiscountPrice())));
            holder.tvPric.setText(Utils.formatAmount(Double.parseDouble(cartInfo.getPrice())));
            price = Double.parseDouble(cartInfo.getDiscountPrice()) * cartInfo.getAmount();
        }
        if (cartInfo.getImages().size() == 0) {
            holder.imgProducts.setImageResource(R.drawable.logo);
        } else {
            Glide.with(context)
                    .load(ConstantApi.MAIN_DNS + "/" + cartInfo.getImages().get(0))
                    .transform(new CenterCrop(), new RoundedCorners(8))
                    .error(R.drawable.logo)
                    .into(holder.imgProducts);
        }
        holder.checkItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (holder.checkItem.isChecked()) {
                    priceCheck.getPriceCheck(position, price, true);
                    paymentOrderInfoList.add(new PaymentOrderInfo(cartInfo.getAmount(), cartInfo.getSize(),
                            cartInfo.getColor(), cartInfo.getId(), cartInfo.getName()
                            , cartInfo.getPrice(), cartInfo.getDiscountPrice(), cartInfo.getImages().get(0),
                            cartInfo.getDiscountPercent()));
                    priceCheck.getCheckAdd(position, true, paymentOrderInfoList);
                    cartInfo.setSelected(true);
                } else {
                    priceCheck.getPriceCheck(position, price, false);
                    cartInfo.setSelected(false);
                    if (paymentOrderInfoList.size() > 1) {
                        paymentOrderInfoList.remove(position);
                    } else {
                        paymentOrderInfoList.clear();
                    }
                    priceCheck.getCheckAdd(position, false, paymentOrderInfoList);

                }
            }
        });
        holder.swipe.setShowMode(SwipeLayout.ShowMode.PullOut);
        // Handling different events when swiping
        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemManger.removeShownLayouts(holder.swipe);
                priceCheck.clickDeleteItem(view, position, cartInfo.getCartId());
                priceCheck.getPriceCheck(position, price, false);

            }
        });
        holder.imgPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int amount = Integer.parseInt(holder.edCount.getText().toString());
                amount += 1;
                priceCheck.clickPlus(view, cartInfo.getCartId(), amount);
                holder.edCount.setText(String.valueOf(amount));
                priceCheck.getPriceCheck(position, 0, false);
//                holder.checkItem.setChecked(false);
                notifyItemRangeChanged(position, cartInfoList.size());
                mItemManger.closeAllItems();

            }
        });
        holder.imgMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int amount = Integer.parseInt(holder.edCount.getText().toString());
                if (amount == 1) {
                    amount = 1;
                } else {
                    amount -= 1;
                    priceCheck.clickPlus(view, cartInfo.getCartId(), amount);
                }
                holder.edCount.setText(String.valueOf(amount));
                priceCheck.getPriceCheck(position, 0, false);
//                holder.checkItem.setChecked(false);
                notifyItemRangeChanged(position, cartInfoList.size());
                mItemManger.closeAllItems();
            }
        });
        mItemManger.bind(holder.itemView, position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                priceCheck.recyclerViewListClickedData(view, position, cartInfo);
            }
        });

    }

    @Override
    public int getItemCount() {
        return cartInfoList.size();
    }

    public void setPriceCheck(PriceCheck priceCheck) {
        this.priceCheck = priceCheck;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public class ViewHodel extends RecyclerView.ViewHolder {

        @BindView(R.id.imgProducts)
        ImageView imgProducts;
        @BindView(R.id.tvNameProducts)
        TextView tvNameProducts;
        @BindView(R.id.tvDiscountPric)
        TextView tvDiscountPric;
        @BindView(R.id.tvPric)
        TextView tvPric;
        @BindView(R.id.tvSize)
        TextView tvSize;
        @BindView(R.id.edCount)
        TextView edCount;
        @BindView(R.id.checkItem)
        CheckBox checkItem;
        @BindView(R.id.swipe)
        SwipeLayout swipe;
        @BindView(R.id.tvDelete)
        TextView tvDelete;
        @BindView(R.id.imgMinus)
        ImageView imgMinus;
        @BindView(R.id.imgPlus)
        ImageView imgPlus;

        public ViewHodel(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvPric.setPaintFlags(tvPric.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }

    public interface PriceCheck {
        void getPriceCheck(int position, double price, boolean checkClick);

        void getDissAll(int position, boolean checkClick, double price);

        void getCheckAdd(int position, boolean checkClick, List<PaymentOrderInfo> paymentOrderInfos);

        void clickDeleteItem(View view, int position, int cartID);

        void clickPlus(View view, int cartID, int amount);

        void recyclerViewListClickedData(View v, int position, CartInfo cartInfo);
    }

    public void removeItem(int position) {
        cartInfoList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, cartInfoList.size());
        mItemManger.closeAllItems();
//        notifyDataSetChanged();
    }

}
