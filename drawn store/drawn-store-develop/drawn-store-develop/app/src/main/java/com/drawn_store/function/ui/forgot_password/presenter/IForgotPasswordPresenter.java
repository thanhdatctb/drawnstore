package com.drawn_store.function.ui.forgot_password.presenter;

import com.drawn_store.function.common.objects.request.ChangerPasswordForgotRep;
import com.drawn_store.function.common.objects.request.EmployeeReq;

public interface IForgotPasswordPresenter {
    void updatePassword(ChangerPasswordForgotRep changerPasswordForgotRep);
}
