package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_info_account;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.drawn_store.R;
import com.drawn_store.common.base.BaseFragment;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.common.view.DialogChangerInfoUser;
import com.drawn_store.common.view.DialogMessage;
import com.drawn_store.common.view.ToastUtils;
import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.constants.FConstants;
import com.drawn_store.function.common.objects.response.UpdateAccountRes;
import com.drawn_store.function.ui.login.presenter.LoginPresenterImpl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;

public class FragmentInfoAccount extends BaseFragment implements View.OnClickListener {

    public FragmentInfoAccount newInstance() {
        return this;
    }

    @BindView(R.id.imgAvatar)
    ImageView imgAvatar;
    @BindView(R.id.tvFullName)
    TextView tvFullName;
    @BindView(R.id.tvEmail)
    TextView tvEmail;
    @BindView(R.id.rlFullName)
    RelativeLayout rlFullName;
    @BindView(R.id.rlChangerPassword)
    RelativeLayout rlChangerPassword;
    @BindView(R.id.rlEmail)
    RelativeLayout rlEmail;
    @BindView(R.id.tvEditImage)
    TextView tvEditImage;
    private static final int REQUEST_CODE_CHOOSE_IMAGE = 1;
    private static final int REQUEST_CHOOSE_IMAGE = 1002;
    private String realPath;
    private Bitmap mBitmap;
    private boolean checkSave = true;
    private LoginPresenterImpl mLoginPresenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_info_account, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        isHideButtonBack(false);
        backgroudToolbar(true);
        setTitle(R.string.info_account);
        dataProfile();
        setListener();
        checkSave = false;
        mLoginPresenter = new LoginPresenterImpl(this);
    }

    private void dataProfile() {
        if (loginRes.getData().getAvatar() == null) {
            imgAvatar.setImageResource(R.drawable.avata_de);
        } else {
            if (loginRes.getData().getAvatar().contains("http")) {
                Glide.with(this)
                        .load(loginRes.getData().getAvatar())
                        .apply(RequestOptions.circleCropTransform())
                        .into(imgAvatar);
            } else {
                Glide.with(this)
                        .load(ConstantApi.MAIN_DNS + "/" + loginRes.getData().getAvatar())
                        .apply(RequestOptions.circleCropTransform())
                        .into(imgAvatar);
            }
        }
        tvFullName.setText(loginRes.getData().getName());
        tvEmail.setText(loginRes.getData().getEmail());

    }

    private void setListener() {
        rlChangerPassword.setOnClickListener(this);
        rlFullName.setOnClickListener(this);
        rlEmail.setOnClickListener(this);
        tvEditImage.setOnClickListener(this);
    }

    @Override
    public void success(Object x) {
        super.success(x);
        hideProgress();
        if (x instanceof UpdateAccountRes) {
            UpdateAccountRes updateAccountRes = (UpdateAccountRes) x;
//            loginRess.getData().setId(loginRes.getData().getId());
            loginRes.getData().setAvatar(updateAccountRes.getData().getAvatar());
            String personJsonString = Utils.getGsonParser().toJson(loginRes);
            prefManager.saveSetting(FConstants.KEY_PUT_USERINFO, personJsonString);
            DialogUtil.showDialogMessage(requireContext(), getResources().getString(R.string.update_success), R.string.ok, new DialogMessage.OnDialogMessageListener() {
                @Override
                public void yesClick() {

                }
            });
            tvEditImage.setText(R.string.edit);
            checkSave = false;
        }
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj error = (BaseObj) o;
        if (error.getStatusCode() == 404) {
            DialogUtil.showDialogMessage(requireContext(), getString(R.string.dish_does_not_exist));
        } else if (error.getStatusCode() == 400) {

        } else {
            DialogUtil.showDialogMessage(requireContext(), error.getMessage());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlChangerPassword:
                showDialogChangerInfoUser(true);
                break;
            case R.id.rlFullName:
                showDialogChangerInfoUser(false);
                break;
            case R.id.rlEmail:
                DialogUtil.showDialogMessage(requireContext(), "Không thể thay đổi");
                break;
            case R.id.tvEditImage:
                if (checkSave) {
                    showProgress();
                    multipartImageUpload();
                } else {
                    startChooseImageIntentForResult();
                }
                break;
        }
    }

    private void showDialogChangerInfoUser(boolean changerPassword) {
        DialogChangerInfoUser dialogChangerInfoUser = new DialogChangerInfoUser().newInstance(changerPassword);
        dialogChangerInfoUser.setBitmap(((BitmapDrawable) imgAvatar.getDrawable()).getBitmap());
        dialogChangerInfoUser.show(getChildFragmentManager(), "botton");
    }

    private void startChooseImageIntentForResult() {
        if (ContextCompat.checkSelfPermission(
                getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_CODE_CHOOSE_IMAGE
            );
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CHOOSE_IMAGE);
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_CHOOSE_IMAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CHOOSE_IMAGE);
                } else {
                    ToastUtils.showToast(requireContext(), getResources().getString(R.string.you_did_not_accept_choose_image), Gravity.BOTTOM);
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CHOOSE_IMAGE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            realPath = getRealPathFromURI(uri);
            try {
                InputStream inputStream = requireActivity().getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
//                imgAvatar.setImageBitmap(scaleDown(bitmap, 1500, true));
                Glide.with(getContext())
                        .asBitmap()
                        .load(uri)
                        .apply(RequestOptions.circleCropTransform())
                        .into(new CustomTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                imgAvatar.setImageBitmap(scaleDown(resource, 1500, true));
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {
                            }
                        });
                mBitmap = bitmap;
                checkSave = true;
                tvEditImage.setText("Lưu");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    public String getRealPathFromURI(Uri contentUri) {
        String path = null;
        String[] proj = {MediaStore.MediaColumns.DATA};
        Cursor cursor = requireActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    private void multipartImageUpload() {
        if (mBitmap == null) {
            mBitmap = ((BitmapDrawable) imgAvatar.getDrawable()).getBitmap();

        } else {
            mBitmap = scaleDown(mBitmap, 1500, true);
        }
        try {
            File filesDir = requireActivity().getFilesDir();
            File file = new File(filesDir, "image" + ".png");

            OutputStream os;
            try {
                os = new FileOutputStream(file);
                mBitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
                os.flush();
                os.close();
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
            }

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            mBitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), reqFile);
            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), tvFullName.getText().toString());
            RequestBody email = RequestBody.create(MediaType.parse("text/plain"), tvEmail.getText().toString());
            mLoginPresenter.updateAccount(email, name, body);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
