package com.drawn_store.function.ui.home.fragment.fragment_detail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.drawn_store.R;
import com.drawn_store.function.common.constants.ConstantApi;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SliderAdapterExample extends SliderViewAdapter<SliderAdapterExample.SliderAdapterVH> {

    private Context context;
    private List<String> imageStiles;

    public SliderAdapterExample(Context context, List<String> imageStiles) {
        this.context = context;
        this.imageStiles = imageStiles;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_slider, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
       String imageStile = imageStiles.get(position);
//        Glide.with(context).load(imageStile.getImage())
//                .into(viewHolder.imageStile);
        if (imageStiles.size() == 0) {
            viewHolder.imageStile.setImageResource(R.drawable.logo);
        } else {
            Glide.with(context)
                    .load(ConstantApi.MAIN_DNS+"/"+imageStile)
                    .error(R.drawable.logo)
                    .into(viewHolder.imageStile);
        }
    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return imageStiles.size();
    }

    static class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        @BindView(R.id.imageStile)
        ImageView imageStile;
        public SliderAdapterVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
