package com.drawn_store.function.common.objects;


public class ItemCart {
    private int id;
    private String name;
    private int count;
    private String note;
    private String thumbnail;

    public ItemCart(int id, String name, int count, String note, String thumbnail) {
        this.id = id;
        this.name = name;
        this.count = count;
        this.note = note;
        this.thumbnail = thumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
