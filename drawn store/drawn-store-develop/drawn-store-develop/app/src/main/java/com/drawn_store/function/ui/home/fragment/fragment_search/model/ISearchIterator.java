package com.drawn_store.function.ui.home.fragment.fragment_search.model;

import com.drawn_store.function.common.objects.request.CheckEmailRep;
import com.drawn_store.function.common.objects.request.LoginReq;
import com.drawn_store.function.common.objects.request.LoginReqGoogle;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface ISearchIterator {

    void search( MultipartBody.Part image,RequestBody customerId);

}
