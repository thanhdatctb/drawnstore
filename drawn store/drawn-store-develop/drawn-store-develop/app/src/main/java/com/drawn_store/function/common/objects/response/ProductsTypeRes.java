package com.drawn_store.function.common.objects.response;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.function.common.objects.ProductType;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductsTypeRes extends BaseObj {

    @SerializedName("data")
    @Expose
    private List<ProductType> data = null;

    public List<ProductType> getData() {
        return data;
    }

    public void setData(List<ProductType> data) {
        this.data = data;
    }

}
