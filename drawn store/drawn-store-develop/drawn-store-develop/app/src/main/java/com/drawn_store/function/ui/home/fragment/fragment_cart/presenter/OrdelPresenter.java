package com.drawn_store.function.ui.home.fragment.fragment_cart.presenter;

import com.drawn_store.function.common.objects.request.CartRep;
import com.drawn_store.function.common.objects.request.UpdateCartRep;

public interface OrdelPresenter {
    void addCart(CartRep cartRep);
    void getCart(int customerId);
    void deleteCart(int cartId);
    void updateCart(UpdateCartRep updateCartRep);
}
