package com.drawn_store.function.ui.register.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import com.drawn_store.R;
import com.drawn_store.common.base.BaseActivity;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.ultility.DialogUtil;;
import com.drawn_store.common.view.DialogMessage;
import com.drawn_store.common.view.ToastUtils;
import com.drawn_store.function.common.objects.request.RegisterReq;
import com.drawn_store.function.common.objects.response.RegisterRes;
import com.drawn_store.function.ui.register.presenter.RegisterPresenterImpl;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends BaseActivity implements View.OnClickListener, BaseView {

    @BindView(R.id.btn_back)
    ImageButton btnBack;
    @BindView(R.id.edFullName)
    EditText edFullname;
    @BindView(R.id.edPassword)
    EditText edPassword;
    @BindView(R.id.edConfirmPassword)
    EditText edConfirmPassword;
    @BindView(R.id.edEmail)
    EditText edEmail;
    @BindView(R.id.btnRegister)
    Button btnRegister;
    private RegisterPresenterImpl mRegisterPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        setTitle(R.string.register);
//        backgroudToolbar(false);
        btnBack.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        mRegisterPresenter = new RegisterPresenterImpl(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                finish();
                break;
            case R.id.btnRegister:
                String fullName = edFullname.getText().toString();
                String email = edEmail.getText().toString().trim();
                String password = edPassword.getText().toString();
                String confirmPassword = edConfirmPassword.getText().toString();


                if (fullName.equals("") || fullName == null) {
                    ToastUtils.showToast(this, R.string.input_your_name);
                } else if (edEmail.equals("") || edEmail == null) {
                    ToastUtils.showToast(this, R.string.input_your_email);
                }else if (!email.contains("@")){
                    ToastUtils.showToast(this, R.string.not_email);
                } else if (password.equals("") || password == null) {
                    ToastUtils.showToast(this, R.string.input_your_password);
                } else if (confirmPassword.equals("") || confirmPassword == null) {
                    ToastUtils.showToast(this, R.string.input_your_password_confirm);
                } else if (!confirmPassword.equals(password)) {
                    ToastUtils.showToast(this, R.string.password_confirm_not_correct);
                }  else {
                    handleRegister(fullName, email, confirmPassword,null);//default first employee in system is admin.
                }
                break;
        }
    }

    private void handleRegister(String name, String email, String password, String avatar) {
        RegisterReq registerReq = new RegisterReq();
        registerReq.setName(name);
        registerReq.setEmail(email);
        registerReq.setPassword(password);
        registerReq.setAvatar(avatar);
        showProgress();
        mRegisterPresenter.register(registerReq);
    }

    @Override
    public void success(Object x) {
        super.success(x);
        hideProgress();
         if (x instanceof RegisterRes) {
            DialogUtil.showDialogMessage(this, getResources().getString(R.string.register_success), R.string.ok, new DialogMessage.OnDialogMessageListener() {
                @Override
                public void yesClick() {
                    setResult(RESULT_OK);
                    finish();
                }
            });
        }
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj err = (BaseObj) o;
        if (err.getStatusCode() == 403) {
            DialogUtil.showDialogMessage(this, getResources().getString(R.string.this_username_already_exist));
        } else {
            DialogUtil.showDialogMessage(this, err.getMessage());
        }
    }

}
