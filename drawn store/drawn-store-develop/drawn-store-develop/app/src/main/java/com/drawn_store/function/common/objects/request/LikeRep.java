package com.drawn_store.function.common.objects.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LikeRep {
    @SerializedName("customerId")
    @Expose
    private Integer customerId;
    @SerializedName("productId")
    @Expose
    private Integer productId;

    public LikeRep() {
    }

    public LikeRep(Integer customerId, Integer productId) {
        this.customerId = customerId;
        this.productId = productId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }
}
