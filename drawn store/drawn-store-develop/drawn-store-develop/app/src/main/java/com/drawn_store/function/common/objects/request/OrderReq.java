package com.drawn_store.function.common.objects.request;

import com.drawn_store.function.common.objects.ItemCart;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class OrderReq {
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("totalPrice")
    @Expose
    private String totalPrice;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("products")
    @Expose
    private List<Product> products = null;

    public OrderReq() {
    }

    public OrderReq(Integer userId, String status, String totalPrice, String address, List<Product> products) {
        this.userId = userId;
        this.status = status;
        this.totalPrice = totalPrice;
        this.address = address;
        this.products = products;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}

