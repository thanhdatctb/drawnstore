package com.drawn_store.common.ultility;

import android.content.Context;

import com.drawn_store.R;
import com.drawn_store.common.constants.AppConfig;

import java.text.DecimalFormat;
import java.util.Calendar;

public class StringUtils {

    public static String CURRENTCY_FORMAT = "#,###";

    public static String formatCurrency(long value, String format) {
        try {
            DecimalFormat formatter = new DecimalFormat(format);
            String result = formatter.format(value);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "N/A";

    }

    public static String formatCurrency(double value, String format) {
        try {
            DecimalFormat formatter = new DecimalFormat(format);
            String result = formatter.format(value);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "N/A";

    }

    /**
     * remove "," when get value
     *
     * @param input
     * @return
     */
    public static String removeCommand(String input) {
        if (input.contains(",")) {
            input = input.replaceAll(",", "");
        }
        return input;

    }

    public static String formatCurrency(float value, String format) {
        try {
            DecimalFormat formatter = new DecimalFormat(format);
            String result = formatter.format(value);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "N/A";

    }

    public static String formatCurrency(String value, String format) {
        try {
            DecimalFormat formatter = new DecimalFormat(format);
            String result = formatter.format(Long.valueOf(value));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "N/A";

    }

    public static String getTimeString(Context ct) {
        AppConfig.TimeOfDay rs = TimeCalculator.GetTimeOfDay(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
        switch (rs) {

            case MORNING: {
                return ct.getString(R.string.title_morning);
            }
            case NOON: {
                return ct.getString(R.string.title_noon);
            }
            case AFTERNOON: {
                return ct.getString(R.string.title_afternoon);
            }
            default:
                break;
        }
        return "";
    }
}
