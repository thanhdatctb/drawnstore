package com.drawn_store.function.common.objects.response;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.function.common.objects.DistrictInfo;
import com.drawn_store.function.common.objects.ProvinceInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DistrictRes extends BaseObj {

    @SerializedName("data")
    @Expose
    private List<DistrictInfo> data = null;

    public List<DistrictInfo> getData() {
        return data;
    }

    public void setData(List<DistrictInfo> data) {
        this.data = data;
    }
}
