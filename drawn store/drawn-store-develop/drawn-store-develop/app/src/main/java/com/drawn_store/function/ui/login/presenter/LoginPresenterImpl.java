package com.drawn_store.function.ui.login.presenter;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.presenter.InteractorCallback;
import com.drawn_store.function.common.objects.request.ChangerPasswordRep;
import com.drawn_store.function.common.objects.request.CheckEmailRep;
import com.drawn_store.function.common.objects.request.LoginReq;
import com.drawn_store.function.common.objects.request.LoginReqGoogle;
import com.drawn_store.function.ui.login.model.LoginIteratorImpl;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class LoginPresenterImpl implements ILoginPresenter {

    private LoginIteratorImpl mLoginIterator;

    public LoginPresenterImpl(BaseView<BaseObj, Object> mBaseView) {
        InteractorCallback<BaseObj, Object> mCallback = new InteractorCallback<BaseObj, Object>() {
            @Override
            public void success(BaseObj x) {
                mBaseView.success(x);
            }

            @Override
            public void error(Object o, boolean isShowDialog) {
                mBaseView.error(o, isShowDialog);
            }
        };
        mLoginIterator = new LoginIteratorImpl(mCallback);
    }

    @Override
    public void login(LoginReq loginReq) {
        mLoginIterator.login(loginReq);
    }

    @Override
    public void loginGoogle(LoginReqGoogle loginReq) {
        mLoginIterator.loginGoogle(loginReq);
    }

    @Override
    public void checkEmail(CheckEmailRep email) {
        mLoginIterator.checkEmail(email);
    }

    @Override
    public void deleteAccount(int userId) {
        mLoginIterator.deleteAccount(userId);
    }

    @Override
    public void changerPassword(ChangerPasswordRep changerPasswordRep) {
        mLoginIterator.changerPassword(changerPasswordRep);
    }

    @Override
    public void updateAccount(RequestBody email, RequestBody name, MultipartBody.Part avatar) {
        mLoginIterator.updateAccount(email, name, avatar);
    }

}
