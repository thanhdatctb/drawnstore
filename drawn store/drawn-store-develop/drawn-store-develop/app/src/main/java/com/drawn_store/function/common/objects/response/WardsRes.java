package com.drawn_store.function.common.objects.response;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.function.common.objects.WardsInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WardsRes extends BaseObj {

    @SerializedName("data")
    @Expose
    private List<WardsInfo> data = null;

    public List<WardsInfo> getData() {
        return data;
    }

    public void setData(List<WardsInfo> data) {
        this.data = data;
    }
}
