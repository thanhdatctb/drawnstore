package com.drawn_store.common.constants;

import android.app.Activity;
import android.view.View;
import android.view.WindowManager;

public final class ScreenConfig {

    private static int currentApiVersion;

    public static void hideActionBar(Activity activity) {
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LOW_PROFILE;
        activity.getWindow().getDecorView().setSystemUiVisibility(uiOptions);
    }
}
