package com.drawn_store.function.common.constants;

public interface ConstantApi {

    /**
     * For release
     */
    String MAIN_DNS = "http://3.1.206.9:3978";
    /**
     * For Debug
     */
//    String MAIN_DNS = "https://webbotservices.tk";
    String BASE_URL = MAIN_DNS + "/api/";
    String URL_LOGIN = "login";
    String URL_LOGIN_GOOGLE = "login-with-google";
    String URL_EMPLOYEE_REGISTER = "user/register";
    String URL_CHECK_EMAIL = "user/forgot-password";
    String URL_CHANGER_PASSWORD = "user/change-password";
    String URL_CHANGER_PASSWORD_FORGOT = "user/forgot-password-change";
    String URL_DELETE_ACCOUNT = "user/{userId}";
    String URL_UPDATE_ACCOUNT = "user/update-account-info";
    String URL_PRODUCT_SALE = "product/sale/{userId}/{limit}";
    String URL_PRODUCT_NEW = "product/latest/{userId}/{limit}";
    String URL_PRODUCT_HIGHESTVIEW = "product/highest-view/{userId}/{limit}";
    String URL_PRODUCT_TYPE = "product/line";
    String URL_PRODUCT_ALL = "product/products/{userId}";
    String URL_PRODUCT_LINEID = "product/products/{userId}/{productLineId}";
    String URL_ADD_VIEW = "product/view/{productId}";
    String URL_ADD_CART = "cart";
    String URL_LIKE = "product/like";
    String URL_LIKE_SHOW = "product/like/{userId}";
    String URL_CART = "cart/{customerId}";
    String URL_CART_DELETE = "cart/{cartId}";
    String URL_CART_UPDATE = "cart/";
    String URL_PROVINCE = "user/provinces";
    String URL_DISTRICT = "user/districts/{code}";
    String URL_WARDS = "user/wards/{code}";
    String URL_LIST_ADDRESS = "user/order-info/{userId}";
    String URL_ADD_ADDRESS = "user/order-info";
    String URL_DELETE_ADDRESS = "user/order-info/{infoId}";
    String URL_SEARCH_BY_IMAGE = "search";
    String URL_SHOW_RATING = "ratting/{productId}";
    String URL_ORDER = "order";
    String URL_SHOW_ORDER = "order/{userId}/{filter}";
}
