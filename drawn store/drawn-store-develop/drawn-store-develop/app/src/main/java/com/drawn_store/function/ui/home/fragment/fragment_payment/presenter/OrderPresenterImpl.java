package com.drawn_store.function.ui.home.fragment.fragment_payment.presenter;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.presenter.InteractorCallback;
import com.drawn_store.function.common.objects.request.OrderReq;
import com.drawn_store.function.ui.home.fragment.fragment_payment.model.OrderIteratorImpl;

public class OrderPresenterImpl implements OrderPresenter {
    private OrderIteratorImpl orderPresenter;

    public OrderPresenterImpl(BaseView<BaseObj, Object> mBaseView) {
        InteractorCallback<BaseObj, Object> mCallback = new InteractorCallback<BaseObj, Object>() {
            @Override
            public void success(BaseObj x) {
                mBaseView.success(x);
            }

            @Override
            public void error(Object o, boolean isShowDialog) {
                mBaseView.error(o, isShowDialog);
            }
        };
        orderPresenter = new OrderIteratorImpl(mCallback);
    }

    @Override
    public void order(OrderReq orderReq) {
        orderPresenter.order(orderReq);
    }

    @Override
    public void showOrder(int userId, String filter) {
        orderPresenter.showOrder(userId, filter);
    }
}
