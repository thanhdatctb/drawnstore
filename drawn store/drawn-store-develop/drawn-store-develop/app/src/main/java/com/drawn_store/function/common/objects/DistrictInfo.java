package com.drawn_store.function.common.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DistrictInfo {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("name_with_type")
    @Expose
    private String nameWithType;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("path_with_type")
    @Expose
    private String pathWithType;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("parent_code")
    @Expose
    private String parentCode;

    public DistrictInfo() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getNameWithType() {
        return nameWithType;
    }

    public void setNameWithType(String nameWithType) {
        this.nameWithType = nameWithType;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPathWithType() {
        return pathWithType;
    }

    public void setPathWithType(String pathWithType) {
        this.pathWithType = pathWithType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }
}
