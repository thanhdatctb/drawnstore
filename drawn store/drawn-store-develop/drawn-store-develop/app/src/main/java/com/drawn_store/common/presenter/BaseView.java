package com.drawn_store.common.presenter;

public interface BaseView<T, E> extends BaseResponse<T, E> {
    /**
     * show progress view.
     */
    void showProgress();

    /**
     * hide progress view.
     */
    void hideProgress();
}
