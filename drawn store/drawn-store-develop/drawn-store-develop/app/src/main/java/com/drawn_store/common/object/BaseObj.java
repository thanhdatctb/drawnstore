package com.drawn_store.common.object;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BaseObj implements Serializable {
    @SerializedName("statusCode")
    private int statusCode;

    @SerializedName("message")
    private String message;

    private String type;

    public static BaseObj createError(String type, String message) {
        BaseObj baseObj = new BaseObj();
        baseObj.setMessage(message);
        baseObj.setType(type);
        return baseObj;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getMessage() {
        return message == null ? "" : message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
