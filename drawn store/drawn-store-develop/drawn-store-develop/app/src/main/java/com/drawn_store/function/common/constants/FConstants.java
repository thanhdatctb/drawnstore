package com.drawn_store.function.common.constants;

public interface FConstants {
    String KEY_PUT_USERINFO = "KEY_PUT_USERINFO";
    String KEY_IS_TITLE = "KEY_IS_TITLE";
    String KEY_PRODUCT_INFO = "KEY_PRODUCT_INFO";
    String KEY_SHOW_BOTTON = "KEY_SHOW_BOTTON";
    String KEY_CHANGER_PASSWORD= "KEY_CHANGER_PASSWORD";
    String KEY_EMAIL = "EMAIL";
    String KEY_POSITION = "KEY_POSITION";
    String KEY_LINE_ID = "KEY_LINE_ID";
    int TAB_RESULT = 0;
    int TAB_PRICE = 1;
    int TAB_SEE = 2;
    int TAB_DATE = 3;
    int SHOW_ALL_BOTTON = 0;
    int SHOW_BOTTON_BUY_NOW = 1;
    int SHOW_BOTTIN_ADD_CART = 2;
    String KEY_PUT_USER = "KEY_PUT_USER";
    String KEY_PUT_CODE = "KEY_PUT_CODE";
    String KEY_JSON_PROVINCE = "KEY_JSON_PROVINCE";
    String KEY_JSON_DISTRICT = "KEY_JSON_DISTRICT";
    String KEY_JSON_WARDS = "KEY_JSON_WARDS";
    String KEY_CHECK_DEFAULT = "KEY_CHECK_DEFAULT";
    String KEY_JSON_ADDRESS = "KEY_JSON_ADDRESS";
    String KEY_LIST_PAYMENT_ORDER = "KEY_LIST_PAYMENT_ORDER";
}
