package com.drawn_store.function.common.api.request;

import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.objects.request.CartRep;
import com.drawn_store.function.common.objects.request.UpdateCartRep;
import com.drawn_store.function.common.objects.response.CartDeleteRes;
import com.drawn_store.function.common.objects.response.CartRes;
import com.drawn_store.function.common.objects.response.CartUpdateRes;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CartRequest {
    @POST(ConstantApi.URL_ADD_CART)
    Call<CartRes> addCart(@Body CartRep cartRep);

    @GET(ConstantApi.URL_CART)
    Call<CartRes> getCart(@Path("customerId") int customerId);

    @HTTP(method = "DELETE", path = ConstantApi.URL_CART_DELETE, hasBody = true)
    Call<CartDeleteRes> deleteCart(@Path("cartId") int cartId);


    @PUT(ConstantApi.URL_CART_UPDATE)
    Call<CartUpdateRes> updateCart(@Body UpdateCartRep updateCartRep);
}
