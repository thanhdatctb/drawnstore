package com.drawn_store.function.ui.home.fragment.fragment_settings;

import android.annotation.SuppressLint;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.drawn_store.R;
import com.drawn_store.common.base.BaseFragment;
import com.drawn_store.common.view.ToastUtils;
import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.objects.UtilityOrder;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_like_of_you.FragmentLikeOffYou;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_order.FragmentOrder;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_set_profile.FragmentSetProfile;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentSettings extends BaseFragment implements UtilityOrderAdapter.ClickOnItem, View.OnClickListener {


    public FragmentSettings newInstance() {
        return this;
    }


    @BindView(R.id.mTitleContainer)
    LinearLayout mTitleContainer;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.mTitle)
    TextView mTitle;
    @BindView(R.id.lnShowToal)
    LinearLayout lnShowToal;
    @BindView(R.id.collapsing)
    CollapsingToolbarLayout collapsing;
    @BindView(R.id.mImage)
    ImageView mImage;
    @BindView(R.id.imgUser)
    ImageView imgUser;
    @BindView(R.id.tvNameUser)
    TextView tvNameUser;
    @BindView(R.id.tvVersion)
    TextView tvVersion;
    @BindView(R.id.tvSeeNow)
    TextView tvSeeNow;
    @BindView(R.id.tvSeeAllProduct)
    TextView tvSeeAllProduct;
    @BindView(R.id.tvLikeOfYou)
    TextView tvLikeOfYou;
    @BindView(R.id.rvUtilitiesOrder)
    RecyclerView rvUtilitiesOrder;
    @BindView(R.id.btnLogout)
    MaterialButton btnLogout;
    @BindView(R.id.rlHistory)
    RelativeLayout rlHistory;
    @BindView(R.id.rlSetProfile)
    RelativeLayout rlSetProfile;
    @BindView(R.id.lnCancelOrder)
    LinearLayout lnCancelOrder;
    private UtilityOrderAdapter utilityOrderAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        collapsing.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsing.setCollapsedTitleTextAppearance(R.style.Collapsingbar);
        setIconToolBar();
        showInfoUser();
        setListener();
        showListUtilityOrder();
        try {
            PackageInfo pInfo = requireActivity().getPackageManager().getPackageInfo(requireActivity().getPackageName(), 0);
            String version = pInfo.versionName;
            tvVersion.setText("#version: " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void setIconToolBar() {
        if (mToolbar != null) {
            appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    if ((collapsing.getHeight() + verticalOffset) < (2 * ViewCompat.getMinimumHeight(collapsing))) {
                        lnShowToal.setVisibility(View.VISIBLE);
                        mToolbar.setBackground(ContextCompat.getDrawable(requireActivity(), R.drawable.bg_button));
                    } else {
                        lnShowToal.setVisibility(View.GONE);
                        setBackgroundToolbar(Color.TRANSPARENT);
                    }
                }
            });
        }
    }

    private void showInfoUser() {
        mTitle.setText(loginRes.getData().getName());
        tvNameUser.setText(loginRes.getData().getName());
        if (loginRes.getData().getAvatar() == null) {
            mImage.setImageResource(R.drawable.avata_de);
            imgUser.setImageResource(R.drawable.avata_de);
        } else {
            if (loginRes.getData().getAvatar().contains("http")) {
                Glide.with(this)
                        .load(loginRes.getData().getAvatar())
                        .apply(RequestOptions.circleCropTransform())
                        .into(mImage);
                Glide.with(this)
                        .load(loginRes.getData().getAvatar())
                        .apply(RequestOptions.circleCropTransform())
                        .into(imgUser);
            } else {
                Glide.with(this)
                        .load(ConstantApi.MAIN_DNS + "/" + loginRes.getData().getAvatar())
                        .apply(RequestOptions.circleCropTransform())
                        .into(mImage);
                Glide.with(this)
                        .load(ConstantApi.MAIN_DNS + "/" + loginRes.getData().getAvatar())
                        .apply(RequestOptions.circleCropTransform())
                        .into(imgUser);
            }
        }
    }

    private void setListener() {
        btnLogout.setOnClickListener(this);
        rlHistory.setOnClickListener(this);
        rlSetProfile.setOnClickListener(this);
        tvSeeNow.setOnClickListener(this);
        tvSeeAllProduct.setOnClickListener(this);
        tvLikeOfYou.setOnClickListener(this);
        lnCancelOrder.setOnClickListener(this);
    }

    private void showListUtilityOrder() {
        List<UtilityOrder> utilityOrders = new ArrayList<>();
        utilityOrders.add(new UtilityOrder(R.drawable.ic_wating_pack, getResources().getString(R.string.wating_pack)));
        utilityOrders.add(new UtilityOrder(R.drawable.ic_pack, getResources().getString(R.string.sent)));
        utilityOrders.add(new UtilityOrder(R.drawable.ic_rating, getResources().getString(R.string.not_yet_evaluate)));
        utilityOrderAdapter = new UtilityOrderAdapter(requireContext());
        rvUtilitiesOrder.setHasFixedSize(true);
        rvUtilitiesOrder.setLayoutManager(new GridLayoutManager(requireContext(), 3));
        rvUtilitiesOrder.setAdapter(utilityOrderAdapter);
        utilityOrderAdapter.swap(utilityOrders);
        utilityOrderAdapter.setOnClickItem(this);
    }


    @Override
    public void clickPositionItem(View view, int position, String name) {
        replaceFragment(new FragmentOrder().newInstance(position, name), true, false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogout:
                logout();
                break;
            case R.id.rlHistory:
                ToastUtils.showToast(requireContext(), "1", Gravity.BOTTOM);
                break;
            case R.id.rlSetProfile:
                replaceFragment(new FragmentSetProfile().newInstance(), true, false);
                break;
            case R.id.tvSeeNow:
                break;
            case R.id.tvSeeAllProduct:
                replaceFragment(new FragmentOrder().newInstance(4, getResources().getString(R.string.see_all_order)), true, false);

                break;
            case R.id.tvLikeOfYou:
                replaceFragment(new FragmentLikeOffYou().newInstance(), true, false);
                break;
            case R.id.lnCancelOrder:
                replaceFragment(new FragmentOrder().newInstance(3, getResources().getString(R.string.cancel_order)), true, false);
                break;
        }
    }

    private void logout() {
        prefManager.remove();
        navigator.screenLogin(getActivity());
    }
}
