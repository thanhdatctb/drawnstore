package com.drawn_store.function.common.objects.response;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.function.common.objects.ProvinceInfo;
import com.drawn_store.function.common.objects.RatingInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RatingRes extends BaseObj {

    @SerializedName("data")
    @Expose
    private List<RatingInfo> data = null;

    public List<RatingInfo> getData() {
        return data;
    }

    public void setData(List<RatingInfo> data) {
        this.data = data;
    }
}
