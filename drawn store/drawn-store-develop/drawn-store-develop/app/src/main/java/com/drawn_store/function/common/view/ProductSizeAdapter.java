package com.drawn_store.function.common.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.drawn_store.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductSizeAdapter extends RecyclerView.Adapter<ProductSizeAdapter.ViewHolder> {
    private Context context;
    private List<String> sizeList;
    private int rowIndex = -1;
    private OnClickOptions onClickOptions;
    private boolean checkClick;
    public ProductSizeAdapter(Context context, boolean checkClick) {
        this.context = context;
        this.checkClick = checkClick;
    }

    public void swap(List<String> sizeList) {
        this.sizeList = sizeList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductSizeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_options, parent, false);
        return new ProductSizeAdapter.ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ProductSizeAdapter.ViewHolder holder, int position) {
        String size = sizeList.get(position);
        holder.tvColor.setText(size);
        if (checkClick) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    rowIndex = position;
                    onClickOptions.setOnClickOptionsSize(view, position, size);
                    notifyDataSetChanged();
                }
            });
        }
        if (rowIndex == position){
            holder.tvColor.setBackgroundResource(R.drawable.bg_color_options);
            holder.tvColor.setTextColor(context.getResources().getColor(R.color.colorGradientStart));
        }else {
            holder.tvColor.setBackgroundResource(R.drawable.bg_color);
            holder.tvColor.setTextColor(context.getResources().getColor(R.color.colorBlack));
        }

    }

    @Override
    public int getItemCount() {
        return sizeList.size();
    }
    public void setOnClickListenerOptions(OnClickOptions onClickListener) {
        this.onClickOptions = onClickListener;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvColor)
        TextView tvColor;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
