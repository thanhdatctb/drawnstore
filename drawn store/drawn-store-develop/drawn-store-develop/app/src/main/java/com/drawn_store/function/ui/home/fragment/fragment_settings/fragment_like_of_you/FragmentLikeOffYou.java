package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_like_of_you;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.drawn_store.R;
import com.drawn_store.common.api.ErrorCode;
import com.drawn_store.common.base.BaseFragment;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.function.common.objects.ProductsInfo;
import com.drawn_store.function.common.objects.response.LikeRes;
import com.drawn_store.function.common.view.OnRecyclerViewItemClickListener;
import com.drawn_store.function.ui.home.fragment.fragment_detail.FragmentDetailProduct;
import com.drawn_store.function.ui.home.fragment.fragment_home.presenter.ProductPresenterImpl;
import com.drawn_store.function.ui.home.fragment.fragment_home.view.adapter.ProductSeeMoreAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentLikeOffYou extends BaseFragment implements OnRecyclerViewItemClickListener {
    public FragmentLikeOffYou newInstance() {
        return this;
    }

    @BindView(R.id.rvListLike)
    RecyclerView rvListLike;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    private ProductSeeMoreAdapter productSeeMoreAdapter;
    private ProductPresenterImpl productPresenter;
    private List<ProductsInfo> productInfos;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_like_off_you, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        isHideButtonBack(false);
        backgroudToolbar(true);
        setTitle("Yêu thích của tôi");
        productInfos = new ArrayList<>();
        productPresenter = new ProductPresenterImpl(this);
        productPresenter.showLike(loginRes.getData().getId());

        loadSwipe();
    }

    @Override
    public void success(Object x) {
        super.success(x);
        if (x instanceof LikeRes) {
            LikeRes likeRes = (LikeRes) x;
            productInfos = likeRes.getData();
            productSeeMoreAdapter = new ProductSeeMoreAdapter(requireContext(), false);
            productSeeMoreAdapter.setOnClickListener(this);
            rvListLike.setLayoutManager(new LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false));
            rvListLike.setItemAnimator(new DefaultItemAnimator());
            rvListLike.setAdapter(productSeeMoreAdapter);
            productSeeMoreAdapter.swap(productInfos);
        }
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj err = (BaseObj) o;
        if (err.getStatusCode() == ErrorCode.BAD_REQUEST) {
            DialogUtil.showDialogMessage(requireActivity(), getResources().getString(R.string.data_null));
        } else {
            DialogUtil.showDialogMessage(requireActivity(), err.getMessage() + " (" + err.getStatusCode() + ")");
        }
        swipeRefresh.setRefreshing(false);
    }

    private void loadSwipe() {
        swipeRefresh.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorRed,
                R.color.colorBlack);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                productPresenter.showLike(loginRes.getData().getId());
            }
        });
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {

    }

    @Override
    public void recyclerViewListClickedData(View v, int position, ProductsInfo productsInfo) {
        productPresenter.addView(productsInfo.getId());
        replaceFragment(new FragmentDetailProduct().newInstance(productsInfo), true, false);
    }
}
