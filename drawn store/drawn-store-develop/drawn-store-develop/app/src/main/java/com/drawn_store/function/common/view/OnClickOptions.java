package com.drawn_store.function.common.view;

import android.view.View;

public interface OnClickOptions {
    void setOnClickOptionsColor(View view, int position, String name);
    void setOnClickOptionsSize(View view,int position, String name);
}
