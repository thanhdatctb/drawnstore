package com.drawn_store.common.other.swipe.lib;

public class SimpleSwipeListener  implements SwipeLayout.SwipeListener {

    @Override
    public void onStartOpen(SwipeLayout layout) {
    }

    @Override
    public void onOpen(SwipeLayout layout) {
    }

    @Override
    public void onStartClose(SwipeLayout layout) {
    }

    @Override
    public void onClose(SwipeLayout layout) {
    }

    @Override
    public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
    }

    @Override
    public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
    }
}
