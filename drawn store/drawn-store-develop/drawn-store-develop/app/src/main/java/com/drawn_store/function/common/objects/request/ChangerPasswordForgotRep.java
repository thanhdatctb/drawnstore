package com.drawn_store.function.common.objects.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangerPasswordForgotRep {
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("newPassword")
    @Expose
    private String newPassword;

    public ChangerPasswordForgotRep() {
    }

    public ChangerPasswordForgotRep(String email, String newPassword) {
        this.email = email;
        this.newPassword = newPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
