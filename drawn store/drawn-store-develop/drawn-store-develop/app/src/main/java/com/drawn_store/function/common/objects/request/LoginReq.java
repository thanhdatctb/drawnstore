
package com.drawn_store.function.common.objects.request;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class LoginReq {

    @SerializedName("password")
    private String mPassword;
    @SerializedName("email")
    private String mEmail;

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }
}
