package com.drawn_store;

import android.app.Application;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDex;


public class Apps extends Application {
    private static Apps sApp;

    public static Apps getInstance() {
        return sApp;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApp = this;
        MultiDex.install(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
}
