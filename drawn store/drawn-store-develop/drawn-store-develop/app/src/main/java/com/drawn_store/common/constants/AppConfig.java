package com.drawn_store.common.constants;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public final class AppConfig {
    public static final int s_morningTime = 10;
    public static final int s_noonTime = 18;
    public static final int s_afternoonTime = 24;

    public enum TimeOfDay {
        MORNING, NOON, AFTERNOON
    }

    //time show splash screen - unit: millisecond
    public static final int TIME_SHOW_SPLASH_SCREEN = 2000;
    public static final int RC_SIGN_IN = 9001;

}
