package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.dialog.presenter;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.presenter.InteractorCallback;
import com.drawn_store.function.common.objects.request.AddressRep;
import com.drawn_store.function.common.objects.request.UpdateAddressRep;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.dialog.model.IAddressIteratorImpl;

public class IAddressPresenterImpl implements IAddressPresenter{

    private IAddressIteratorImpl iAddressIterator;
    public IAddressPresenterImpl(BaseView<BaseObj, Object> mBaseView) {
        InteractorCallback<BaseObj, Object> mCallback = new InteractorCallback<BaseObj, Object>() {
            @Override
            public void success(BaseObj x) {
                mBaseView.success(x);
            }

            @Override
            public void error(Object o, boolean isShowDialog) {
                mBaseView.error(o, isShowDialog);
            }
        };
        iAddressIterator = new IAddressIteratorImpl(mCallback);
    }
    @Override
    public void provinces() {
        iAddressIterator.provinces();
    }

    @Override
    public void district(String code) {
        iAddressIterator.district(code);
    }

    @Override
    public void wards(String code) {
        iAddressIterator.wards(code);
    }

    @Override
    public void listAddres(int userId) {
        iAddressIterator.listAddres(userId);
    }

    @Override
    public void addAddress(AddressRep addressRep) {
        iAddressIterator.addAddress(addressRep);
    }

    @Override
    public void deleteAddress(int infoId) {
        iAddressIterator.deleteAddress(infoId);
    }

    @Override
    public void updateAddress(UpdateAddressRep updateAddressRep) {
        iAddressIterator.updateAddress(updateAddressRep);
    }
}
