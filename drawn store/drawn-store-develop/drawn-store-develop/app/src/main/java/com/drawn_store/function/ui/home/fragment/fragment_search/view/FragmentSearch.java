package com.drawn_store.function.ui.home.fragment.fragment_search.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.drawn_store.R;
import com.drawn_store.common.api.ErrorCode;
import com.drawn_store.common.base.BaseFragment;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.function.common.objects.ProductsInfo;
import com.drawn_store.function.common.objects.response.ProductsAllRes;
import com.drawn_store.function.common.objects.response.SearchResObj;
import com.drawn_store.function.common.view.BackReload;
import com.drawn_store.function.common.view.OnRecyclerViewItemClickListener;
import com.drawn_store.function.ui.home.fragment.fragment_detail.FragmentDetailProduct;
import com.drawn_store.function.ui.home.fragment.fragment_home.presenter.ProductPresenterImpl;
import com.drawn_store.function.ui.home.fragment.fragment_home.view.adapter.ProductsSaleAdapter;
import com.drawn_store.function.ui.home.fragment.fragment_search.SearchTestApdater;
import com.drawn_store.function.ui.home.fragment.fragment_search.presenter.SearchPresenterImpl;
import com.drawn_store.function.ui.home.fragment.fragment_search.support_image_detect.BitmapUtils;
import com.drawn_store.function.ui.home.fragment.fragment_search.support_image_detect.GraphicOverlay;
import com.drawn_store.function.ui.home.fragment.fragment_search.support_image_detect.ObjectDetectorProcessor;
import com.drawn_store.function.ui.home.fragment.fragment_search.support_image_detect.PreferenceUtils;
import com.drawn_store.function.ui.home.fragment.fragment_search.support_image_detect.VisionImageProcessor;
import com.google.mlkit.vision.objects.defaults.ObjectDetectorOptions;
import com.mindorks.editdrawabletext.DrawablePosition;
import com.mindorks.editdrawabletext.EditDrawableText;
import com.mindorks.editdrawabletext.OnDrawableClickListener;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;


public class FragmentSearch extends BaseFragment implements BaseView,
        SearchTestApdater.ClickItemName, OnRecyclerViewItemClickListener, BackReload {

    private static final String TAG = "FragmentSearch";
    private static final int REQUEST_CODE_CHOOSE_IMAGE = 111;
    private static final int REQUEST_CODE_CHOOSE_CAMERA = 222;
    private static final int REQUEST_CODE_CHOOSE_WRITE_EXTERNAL_STORAGE = 333;

    public FragmentSearch newInstance() {
        return this;
    }

    @BindView(R.id.editSearch)
    EditDrawableText editSearch;
    @BindView(R.id.rvSearch)
    RecyclerView rvSearch;
    @BindView(R.id.rvShowSearch)
    RecyclerView rvShowSearch;
    @BindView(R.id.lnSearch)
    LinearLayout lnSearch;
    @BindView(R.id.lnHistory)
    LinearLayout lnHistory;
    @BindView(R.id.lnShowSearch)
    LinearLayout lnShowSearch;
    @BindView(R.id.tvSearch)
    TextView tvSearch;
    @BindView(R.id.imgPreview)
    ImageView imgPreview;
    @BindView(R.id.graphic_overlay)
    GraphicOverlay graphicOverlay;
    @BindView(R.id.root)
    View rootView;
    @BindView(R.id.tvDescription)
    TextView tvDescription;
    @BindView(R.id.rvColors)
    RecyclerView rvColors;
    @BindView(R.id.lnSearchResultFrame)
    LinearLayout lnSearchResultFrame;
    @BindView(R.id.rvSearchResult)
    RecyclerView rvSearchResult;
    private SearchTestApdater searchTestApdater;
    private ProductPresenterImpl productPresenter;
    private List<ProductsInfo> productsInfos;
    private ProductsSaleAdapter productsNewAdapter;
    private Uri imageUri;

    private static final int REQUEST_IMAGE_CAPTURE = 1001;
    private static final int REQUEST_CHOOSE_IMAGE = 1002;

    public static int screenWidth;
    public static int screenHeight;
    private VisionImageProcessor imageProcessor;
    public static Bitmap croppedBitmap = null;

    private SearchPresenterImpl mSearchPresenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isHideButtonBack(false);
        ButterKnife.bind(this, view);
        productPresenter = new ProductPresenterImpl(this);
        productPresenter.productsAll(loginRes.getData().getId());
        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editSearch.getText().toString();
                if (text.isEmpty()) {
                    lnHistory.setVisibility(View.VISIBLE);
                    lnSearch.setVisibility(View.GONE);
                    lnShowSearch.setVisibility(View.GONE);
                } else {
                    lnHistory.setVisibility(View.GONE);
                    lnSearch.setVisibility(View.VISIBLE);
                    lnShowSearch.setVisibility(View.GONE);
                    if (productsInfos != null)
                        searchTestApdater.getFilter().filter(text);
//                        searchTestApdater.setFilter(text);
                }
            }
        });
        editSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    dataShowSearch();
                    return true;
                }
                return false;
            }
        });
        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataShowSearch();
            }
        });
        editSearch.setDrawableClickListener(new OnDrawableClickListener() {
            @Override
            public void onClick(@NotNull DrawablePosition drawablePosition) {
                switch (drawablePosition) {
                    case TOP:
                        break;
                    case LEFT:
                        break;
                    case RIGHT:
                        // Menu for selecting either: a) take new photo b) select from existing

                        PopupMenu popup = new PopupMenu(getActivity(), editSearch);
                        popup.setOnMenuItemClickListener(
                                menuItem -> {
                                    int itemId = menuItem.getItemId();
                                    if (itemId == R.id.select_images_from_local) {
                                        startChooseImageIntentForResult();
                                        return true;
                                    } else if (itemId == R.id.take_photo_using_camera) {
                                        startCameraIntentForResult();
                                        return true;
                                    }
                                    return false;
                                });
                        MenuInflater inflater = popup.getMenuInflater();
                        inflater.inflate(R.menu.camera_button_menu, popup.getMenu());
                        popup.show();
                        break;
                    case BOTTOM:
                        break;
                }
            }
        });

        mSearchPresenter = new SearchPresenterImpl(this);
    }

    @Override
    public void success(Object x) {
        super.success(x);
        if (x instanceof ProductsAllRes) {
            ProductsAllRes productsAllRes = (ProductsAllRes) x;
            productsInfos = productsAllRes.getData();
            showData();
            showDataSearch();
            searchTestApdater.swap(productsInfos);
            productsNewAdapter.swap(productsInfos);
        }
        if (x instanceof SearchResObj) {
            lnSearchResultFrame.setVisibility(View.VISIBLE);
            SearchResObj searchResObj = (SearchResObj) x;
            hideProgress();
            imgPreview.setVisibility(View.GONE);
            graphicOverlay.clear();
            tvDescription.setText(searchResObj.getData().getDetected());
            CustomAdapter adapter = new CustomAdapter(getActivity(), searchResObj.getData().getColors());
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            rvColors.setHasFixedSize(true);
            rvColors.setLayoutManager(layoutManager);
            rvColors.setAdapter(adapter);

            productsNewAdapter = new ProductsSaleAdapter(requireContext());
            productsNewAdapter.swap(searchResObj.getData().getResults());
            rvSearchResult.setHasFixedSize(true);
            rvSearchResult.setLayoutManager(new GridLayoutManager(requireContext(), 2));
            productsNewAdapter.setOnClickListener(this);
            rvSearchResult.setAdapter(productsNewAdapter);
        }
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj err = (BaseObj) o;
        if (err.getStatusCode() == ErrorCode.BAD_REQUEST) {
            DialogUtil.showDialogMessage(requireActivity(), getResources().getString(R.string.data_null));
        } else {
            DialogUtil.showDialogMessage(requireActivity(), err.getMessage() + " (" + err.getStatusCode() + ")");
        }
    }

    private void showData() {
        searchTestApdater = new SearchTestApdater(requireContext());
        rvSearch.setHasFixedSize(true);
        rvSearch.setLayoutManager(new LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false));
        searchTestApdater.setOnClickName(this);
        rvSearch.setAdapter(searchTestApdater);
    }

    private void showDataSearch() {
        productsNewAdapter = new ProductsSaleAdapter(requireContext());
        rvShowSearch.setHasFixedSize(true);
        rvShowSearch.setLayoutManager(new GridLayoutManager(requireContext(), 2));
        productsNewAdapter.setOnClickListener(this);
        rvShowSearch.setAdapter(productsNewAdapter);
    }

    @Override
    public void setName(View view, int position, String name) {
        editSearch.setText(name);
        dataShowSearch();
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {

    }

    @Override
    public void recyclerViewListClickedData(View v, int position, ProductsInfo productsInfo) {
        productPresenter.addView(productsInfo.getId());
        replaceFragment(new FragmentDetailProduct().newInstance(productsInfo).setReloadFragment(this), true, false);
    }

    private void dataShowSearch() {
        editSearch.clearFocus();
        InputMethodManager in = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(editSearch.getWindowToken(), 0);
        lnSearch.setVisibility(View.GONE);
        lnShowSearch.setVisibility(View.VISIBLE);
        if (productsInfos != null)
            productsNewAdapter.getFilter().filter(editSearch.getText().toString());
    }


    private void startChooseImageIntentForResult() {
        if (ContextCompat.checkSelfPermission(
                getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_CODE_CHOOSE_IMAGE
            );
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CHOOSE_IMAGE);
        }


    }

    private void startCameraIntentForResult() {
        // Clean up last time's image
        imageUri = null;
        imgPreview.setImageBitmap(null);
        if (ContextCompat.checkSelfPermission(
                getActivity(), Manifest.permission.CAMERA) !=
                PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CODE_CHOOSE_CAMERA
            );

        } else {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                values.put(MediaStore.Images.Media.DESCRIPTION, "From Camera");
                imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_CHOOSE_IMAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CHOOSE_IMAGE);
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.you_did_not_accept_choose_image), Toast.LENGTH_SHORT).show();
                }
                break;
            case REQUEST_CODE_CHOOSE_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        ContentValues values = new ContentValues();
                        values.put(MediaStore.Images.Media.TITLE, "New Picture");
                        values.put(MediaStore.Images.Media.DESCRIPTION, "From Camera");
                        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.you_did_not_accept_choose_image), Toast.LENGTH_SHORT).show();
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            tryReloadAndDetectInImage();
        } else if (requestCode == REQUEST_CHOOSE_IMAGE && resultCode == RESULT_OK) {
            // In this case, imageUri is returned by the chooser, save it.
            imageUri = data.getData();
            tryReloadAndDetectInImage();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void tryReloadAndDetectInImage() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenWidth = displayMetrics.heightPixels;
        screenHeight = displayMetrics.widthPixels;
        Log.d(TAG, "Try reload and detect image");
        try {
            if (imageUri == null) {
                return;
            }

            Bitmap imageBitmap = BitmapUtils.getBitmapFromContentUri(getActivity().getContentResolver(), imageUri);
            if (imageBitmap == null) {
                return;
            }
            Bitmap bitmap = Bitmap.createScaledBitmap(imageBitmap, screenHeight, screenWidth, false);

            // Clear the overlay first
            graphicOverlay.clear();

            ObjectDetectorOptions objectDetectorOptions =
                    PreferenceUtils.getObjectDetectorOptionsForStillImage(getActivity());
            imageProcessor = new ObjectDetectorProcessor(getActivity(), objectDetectorOptions, bitmap);

            imgPreview.setImageBitmap(bitmap);


            if (imageProcessor != null) {
                lnSearchResultFrame.setVisibility(View.GONE);
                imgPreview.setVisibility(View.VISIBLE);
                graphicOverlay.setImageSourceInfo(
                        imageBitmap.getWidth(), imageBitmap.getHeight(), false);
                imageProcessor.processBitmap(imageBitmap, graphicOverlay);
                editSearch.setEnabled(false);
                editSearch.setFocusable(false);
                tvSearch.setEnabled(false);
            } else {
                Log.e(TAG, "Null imageProcessor, please check adb logs for imageProcessor creation error");
            }


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (croppedBitmap != null) {

                        // Clear the overlay first
                        showProgress();
                        multipartImageUpload();

                    } else {
                        DialogUtil.showDialogMessage(getActivity(), getResources().getString(R.string.cannot_detect_this_image));
                    }

                }
            }, 500);


        } catch (IOException e) {
            Log.e(TAG, "Error retrieving saved image");
            imageUri = null;
        }
    }

    private void multipartImageUpload() {
        croppedBitmap = scaleDown(croppedBitmap, 1500, true);

        try {
            File filesDir = getActivity().getFilesDir();
            File file = new File(filesDir, "image" + ".png");

            OutputStream os;
            try {
                os = new FileOutputStream(file);
                croppedBitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
                os.flush();
                os.close();
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
            }

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            croppedBitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();


            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
            RequestBody customerID = RequestBody.create(MediaType.parse("text/plain"), loginRes.getData().getId().toString());
            mSearchPresenter.search(body, customerID);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    @Override
    public void reloadFragment() {

    }
}
