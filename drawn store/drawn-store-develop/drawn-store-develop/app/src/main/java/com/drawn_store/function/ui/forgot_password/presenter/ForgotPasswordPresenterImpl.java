package com.drawn_store.function.ui.forgot_password.presenter;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.presenter.InteractorCallback;
import com.drawn_store.function.common.objects.request.ChangerPasswordForgotRep;
import com.drawn_store.function.common.objects.request.EmployeeReq;
import com.drawn_store.function.ui.forgot_password.model.ForgotPasswordIteratorImpl;

public class ForgotPasswordPresenterImpl implements IForgotPasswordPresenter {

    private ForgotPasswordIteratorImpl mForgotPasswordIterator;

    public ForgotPasswordPresenterImpl(BaseView<BaseObj, Object> mBaseView) {
        InteractorCallback<BaseObj, Object> mCallback = new InteractorCallback<BaseObj, Object>() {
            @Override
            public void success(BaseObj x) {
                mBaseView.success(x);
            }

            @Override
            public void error(Object o, boolean isShowDialog) {
                mBaseView.error(o, isShowDialog);
            }
        };
        mForgotPasswordIterator = new ForgotPasswordIteratorImpl(mCallback);
    }

    @Override
    public void updatePassword(ChangerPasswordForgotRep changerPasswordForgotRep) {
        mForgotPasswordIterator.updatePassword(changerPasswordForgotRep);
    }
}
