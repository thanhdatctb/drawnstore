/*
 * Copyright 2020 Google LLC. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drawn_store.function.ui.home.fragment.fragment_search.support_image_detect;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;

import com.drawn_store.function.ui.home.fragment.fragment_search.view.FragmentSearch;
import com.google.mlkit.vision.objects.DetectedObject;
import com.google.mlkit.vision.objects.DetectedObject.Label;

import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;
import java.util.Stack;

/**
 * Draw the detected object info in preview.
 */
public class ObjectGraphic extends GraphicOverlay.Graphic {
    private static final float STROKE_WIDTH = 10.0f;
    private static final int NUM_COLORS = 10;
    private final DetectedObject object;
    private final Paint boxPaints;
    private Bitmap imageBitmap;
    private Stack<Integer> recycle, colors;

    ObjectGraphic(GraphicOverlay overlay, DetectedObject object, Bitmap bitmapSource) {
        super(overlay);

        this.object = object;

        colors = new Stack<>();
        recycle = new Stack<>();
        recycle.addAll(Arrays.asList(
                0xfff44336, 0xffe91e63, 0xff9c27b0, 0xff673ab7,
                0xff3f51b5, 0xff2196f3, 0xff03a9f4, 0xff00bcd4,
                0xff009688, 0xff4caf50, 0xff8bc34a, 0xffcddc39,
                0xffffeb3b, 0xffffc107, 0xffff9800, 0xffff5722,
                0xff795548, 0xff9e9e9e, 0xff607d8b, 0xff333333
                )
        );

        imageBitmap = bitmapSource;

        boxPaints = new Paint();
        boxPaints.setColor(getColor());
        boxPaints.setStyle(Paint.Style.STROKE);
        boxPaints.setStrokeWidth(STROKE_WIDTH);
    }

    @Override
    public void draw(Canvas canvas) {


        // Draws the bounding box.
        RectF rect = new RectF(object.getBoundingBox());
        // If the image is flipped, the left will be translated to right, and the right to left.
        float x0 = translateX(rect.left);
        float x1 = translateX(rect.right);
        rect.left = Math.min(x0, x1);
        rect.right = Math.max(x0, x1);
        rect.top = translateY(rect.top);
        rect.bottom = translateY(rect.bottom);
        canvas.drawRect(rect, boxPaints);

        int x = (int) (rect.left < 0 ? 0 : rect.left);
        int y = (int) (rect.top < 0 ? 0 : rect.top);

        try {
            FragmentSearch.croppedBitmap = Bitmap.createBitmap(
                    imageBitmap,
                    x,
                    y,
                    (int) (rect.width() > imageBitmap.getWidth() ? imageBitmap.getWidth() - x : rect.width()),
                    (int) (rect.height() > imageBitmap.getHeight() ? imageBitmap.getHeight() - y : rect.height())
            );
        } catch (Exception e) {
            Log.e("SSSSSSSSSSSSSSS", e.getMessage());
        }

    }

    public int getColor() {
        if (colors.size() == 0) {
            while (!recycle.isEmpty())
                colors.push(recycle.pop());
            Collections.shuffle(colors);
        }
        Integer c = colors.pop();
        recycle.push(c);
        return c;
    }
}
