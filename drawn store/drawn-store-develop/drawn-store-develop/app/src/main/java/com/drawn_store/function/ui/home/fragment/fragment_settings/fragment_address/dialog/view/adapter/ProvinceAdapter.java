package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.dialog.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.drawn_store.R;
import com.drawn_store.common.pref.PrefManager;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.function.common.constants.FConstants;
import com.drawn_store.function.common.objects.ProvinceInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProvinceAdapter  extends RecyclerView.Adapter<ProvinceAdapter.ViewHodel> {

    private Context context;
    private List<ProvinceInfo> provinceInfos;
    private ClickItemName clickItemName;
    private String nameProvince;
    public ProvinceAdapter(Context context,String nameProvince) {
        this.context = context;
        this.nameProvince = nameProvince;
    }

    public void swap(List<ProvinceInfo> provinceInfos) {
        this.provinceInfos = provinceInfos;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProvinceAdapter.ViewHodel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_text_address, parent, false);
        return new ViewHodel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProvinceAdapter.ViewHodel holder, int position) {
        ProvinceInfo provinceInfo = provinceInfos.get(position);
        PrefManager prefManager = new PrefManager(context);
        holder.tvTextAddress.setText(provinceInfo.getName());
        String personJsonString = Utils.getGsonParser().toJson(provinceInfo);
        if (provinceInfo.getName().equals(nameProvince)){
            holder.imgCheck.setVisibility(View.VISIBLE);
        }else {
            holder.imgCheck.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.imgCheck.setVisibility(View.VISIBLE);
                clickItemName.setName(view,position,provinceInfo.getName());
                prefManager.saveSetting(FConstants.KEY_JSON_PROVINCE,personJsonString);
            }
        });
    }
    public void setOnClickName(ClickItemName clickName){
        this.clickItemName = clickName;
    }


    @Override
    public int getItemCount() {
        return provinceInfos.size();
    }

    public class ViewHodel extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTextAddress)
        TextView tvTextAddress;

        @BindView(R.id.imgCheck)
        ImageView imgCheck;

        public ViewHodel(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imgCheck.setVisibility(View.GONE);
        }
    }

    public interface ClickItemName{
        void setName(View view, int position, String name);
    }
}
