package com.drawn_store.function.ui.home.fragment.fragment_home.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.drawn_store.R;
import com.drawn_store.common.api.ErrorCode;
import com.drawn_store.common.base.BaseFragment;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.function.common.constants.FConstants;
import com.drawn_store.function.common.objects.ProductsInfo;
import com.drawn_store.function.common.objects.response.ProductsHighestViewRes;
import com.drawn_store.function.common.objects.response.ProductsNewRes;
import com.drawn_store.function.common.objects.response.ProductsSaleRes;
import com.drawn_store.function.common.view.BackReload;
import com.drawn_store.function.common.view.OnRecyclerViewItemClickListener;
import com.drawn_store.function.ui.home.fragment.fragment_cart.view.FragmentCart;
import com.drawn_store.function.ui.home.fragment.fragment_detail.FragmentDetailProduct;
import com.drawn_store.function.ui.home.fragment.fragment_home.presenter.ProductPresenterImpl;
import com.drawn_store.function.ui.home.fragment.fragment_home.view.adapter.ProductSeeMoreAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentSeeMore extends BaseFragment implements BaseView, OnRecyclerViewItemClickListener,
        BackReload {

    public FragmentSeeMore newInstance(String title, int positon) {
        Bundle args = new Bundle();
        args.putString(FConstants.KEY_IS_TITLE, title);
        args.putInt(FConstants.KEY_POSITION, positon);
        FragmentSeeMore fragment = new FragmentSeeMore();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.rvListProductSeeMore)
    RecyclerView rvListProductSeeMore;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;
    private ProductPresenterImpl productPresenter;
    private Bundle args;
    private ProductSeeMoreAdapter productSeeMoreAdapter;
    private List<ProductsInfo> productsInfoList;
    ProductsHighestViewRes productsHighestViewRes;
    private int limit = 6, position;
    private LinearLayoutManager linearLayoutManager;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_see_more, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        isHideButtonBack(false);
        backgroudToolbar(false);
        setIconToolBar();
        args = getArguments();
        setTitle(args.getString(FConstants.KEY_IS_TITLE));
        position = args.getInt(FConstants.KEY_POSITION);
        productPresenter = new ProductPresenterImpl(this);
        setPositionProduct(limit);
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                    limit++;
                    progressBar.setVisibility(View.VISIBLE);
                    setPositionProduct(limit);
                }
            }
        });
        loadSwipe();
//        showProgress();
    }

    private void setIconToolBar() {
        if (mToolbar != null) {
            mToolbar.getMenu().clear();
            mToolbar.inflateMenu(R.menu.menu_cart_product);
            mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.itemMenuCart:
                            replaceFragment(new FragmentCart().newInstance(), true, false);
                            break;
                    }
                    return true;
                }
            });
        }
    }

    private void setPositionProduct(int limit) {
        switch (position) {
            //sale
            case 1:
                productPresenter.productsSale(loginRes.getData().getId(), String.valueOf(limit));
                break;
            case 2:
                productPresenter.productsNew(loginRes.getData().getId(), String.valueOf(limit));
                break;
            case 3:
                productPresenter.productsHighestView(loginRes.getData().getId(), String.valueOf(limit));
                break;
        }
    }

    @Override
    public void success(Object x) {
        super.success(x);
        hideProgress();
        boolean checkNew = false;
        if (x instanceof ProductsSaleRes) {
            ProductsSaleRes productsSaleRes = (ProductsSaleRes) x;
            productsInfoList = new ArrayList<>();
            productsInfoList = productsSaleRes.getData();
        } else if (x instanceof ProductsNewRes) {
            ProductsNewRes productsNewRes = (ProductsNewRes) x;
            productsInfoList = new ArrayList<>();
            productsInfoList = productsNewRes.getData();
            checkNew = true;
        } else if (x instanceof ProductsHighestViewRes) {
            productsHighestViewRes = (ProductsHighestViewRes) x;
            productsInfoList = new ArrayList<>();
            productsInfoList = productsHighestViewRes.getData();
        }
        swipeRefresh.setRefreshing(false);
        getDataProductLoad(checkNew);
        productSeeMoreAdapter.swap(productsInfoList);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj err = (BaseObj) o;
        if (err.getStatusCode() == ErrorCode.BAD_REQUEST) {
            DialogUtil.showDialogMessage(requireActivity(), getResources().getString(R.string.data_null));
        } else {
            DialogUtil.showDialogMessage(requireActivity(), err.getMessage() + " (" + err.getStatusCode() + ")");
        }
        swipeRefresh.setRefreshing(false);
        progressBar.setVisibility(View.GONE);
    }

    private void loadSwipe() {
        swipeRefresh.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorRed,
                R.color.colorBlack);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                setPositionProduct(limit);
            }
        });
    }

    private void getDataProductLoad(boolean checkNew) {
        productSeeMoreAdapter = new ProductSeeMoreAdapter(requireContext(), checkNew);
        productSeeMoreAdapter.setOnClickListener(this);
        linearLayoutManager = new LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false);
        rvListProductSeeMore.setLayoutManager(linearLayoutManager);
        rvListProductSeeMore.setItemAnimator(new DefaultItemAnimator());
        rvListProductSeeMore.setAdapter(productSeeMoreAdapter);
    }


    @Override
    public void recyclerViewListClicked(View v, int position) {
        //
    }

    @Override
    public void recyclerViewListClickedData(View v, int position, ProductsInfo productsInfo) {
        productPresenter.addView(productsInfo.getId());
        replaceFragment(new FragmentDetailProduct().newInstance(productsInfo).setReloadFragment(this), true, false);
    }

    @Override
    public void reloadFragment() {
        setPositionProduct(limit);
    }
}
