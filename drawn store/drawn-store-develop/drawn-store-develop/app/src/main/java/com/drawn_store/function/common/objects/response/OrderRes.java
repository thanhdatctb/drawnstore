package com.drawn_store.function.common.objects.response;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.function.common.objects.OrderInfo;
import com.drawn_store.function.common.objects.UserCodeInfo;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderRes extends BaseObj {
    @SerializedName("data")
    private List<OrderInfo> mData;

    public List<OrderInfo> getData() {
        return mData;
    }

    public void setData(List<OrderInfo> data) {
        mData = data;
    }
}
