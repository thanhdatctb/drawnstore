package com.drawn_store.function.ui.home.fragment.fragment_home.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.drawn_store.R;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.objects.ProductsInfo;
import com.drawn_store.function.common.view.OnRecyclerViewItemClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductSeeMoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private Context context;
    private List<ProductsInfo> productsInfoList;
    private boolean isNew = false;
    private OnRecyclerViewItemClickListener itemClickListener;

    public ProductSeeMoreAdapter(Context context, boolean isNew) {
        this.context = context;
        this.isNew = isNew;
    }

    public void swap(List<ProductsInfo> productsInfoList) {
        this.productsInfoList = productsInfoList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_see_more, parent, false);
            return new ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            populateItemRows((ItemViewHolder) holder, position);
        } else if (holder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return productsInfoList == null ? 0 : productsInfoList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return productsInfoList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public void setOnClickListener(OnRecyclerViewItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvNameProducts)
        TextView tvNameProducts;
        @BindView(R.id.tvDiscountPric)
        TextView tvDiscountPric;
        @BindView(R.id.tvPric)
        TextView tvPric;
        @BindView(R.id.tvDiscountPercent)
        TextView tvDiscountPercent;
        @BindView(R.id.tvViewRating)
        TextView tvViewRating;
        @BindView(R.id.tvtotalView)
        TextView tvtotalView;
        @BindView(R.id.tvDateTime)
        TextView tvDateTime;
        @BindView(R.id.imgAvatarProduct)
        ImageView imgAvatarProduct;
        @BindView(R.id.imgNew)
        ImageView imgNew;
        @BindView(R.id.imgLike)
        ImageView imgLike;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvPric.setPaintFlags(tvPric.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.progressBar)
        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    @SuppressLint("SetTextI18n")
    private void populateItemRows(ItemViewHolder viewHolder, int position) {
        ProductsInfo item = productsInfoList.get(position);
        viewHolder.tvNameProducts.setText(item.getName());
        viewHolder.tvViewRating.setText("10 đánh giá");
        viewHolder.tvtotalView.setText("Lượt xem " + item.getTotalView());
        viewHolder.tvDateTime.setText(Utils.convertDate(item.getCreatedAt()));
        if (isNew) {
            viewHolder.imgNew.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imgNew.setVisibility(View.GONE);
        }
        if (item.getDiscountPercent().equals("0%")) {
            viewHolder.tvDiscountPercent.setVisibility(View.GONE);
            viewHolder.tvPric.setVisibility(View.GONE);
            viewHolder.tvDiscountPric.setText(Utils.formatAmount(Double.parseDouble(item.getPrice())));
        } else {
            viewHolder.tvDiscountPercent.setVisibility(View.VISIBLE);
            viewHolder.tvPric.setVisibility(View.VISIBLE);
            viewHolder.tvDiscountPric.setText(Utils.formatAmount(Double.parseDouble(item.getDiscountPrice())));
            viewHolder.tvPric.setText(Utils.formatAmount(Double.parseDouble(item.getPrice())));
            viewHolder.tvDiscountPercent.setText("- " + item.getDiscountPercent());
        }
        if (item.getImages().size() == 0) {
            viewHolder.imgAvatarProduct.setImageResource(R.drawable.logo);
        } else {
            Glide.with(context)
                    .load(ConstantApi.MAIN_DNS + "/" + item.getImages().get(0))
                    .transform(new CenterCrop(), new RoundedCorners(8))
                    .error(R.drawable.logo)
                    .into(viewHolder.imgAvatarProduct);
        }
        if (item.getIsLiked()) {
            viewHolder.imgLike.setImageResource(R.drawable.ic_like);
//            checkLike = false;
        } else {
            viewHolder.imgLike.setImageResource(R.drawable.ic_no_like);
//            checkLike = true;
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.recyclerViewListClickedData(view, position, item);
            }
        });
    }

}
