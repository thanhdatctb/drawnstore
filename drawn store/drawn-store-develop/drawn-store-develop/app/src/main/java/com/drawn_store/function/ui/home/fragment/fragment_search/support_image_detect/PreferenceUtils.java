/*
 * Copyright 2020 Google LLC. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drawn_store.function.ui.home.fragment.fragment_search.support_image_detect;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import androidx.annotation.StringRes;

import com.drawn_store.R;
import com.google.mlkit.vision.objects.ObjectDetectorOptionsBase.DetectorMode;
import com.google.mlkit.vision.objects.defaults.ObjectDetectorOptions;

/**
 * Utility class to retrieve shared preferences.
 */
public class PreferenceUtils {


    public static ObjectDetectorOptions getObjectDetectorOptionsForStillImage(Context context) {
        return getObjectDetectorOptions(
                context,
                R.string.pref_key_still_image_object_detector_enable_multiple_objects,
                R.string.pref_key_still_image_object_detector_enable_classification,
                ObjectDetectorOptions.SINGLE_IMAGE_MODE);
    }


    private static ObjectDetectorOptions getObjectDetectorOptions(
            Context context,
            @StringRes int prefKeyForMultipleObjects,
            @StringRes int prefKeyForClassification,
            @DetectorMode int mode) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        boolean enableMultipleObjects =
                sharedPreferences.getBoolean(context.getString(prefKeyForMultipleObjects), false);
        boolean enableClassification =
                sharedPreferences.getBoolean(context.getString(prefKeyForClassification), true);

        ObjectDetectorOptions.Builder builder =
                new ObjectDetectorOptions.Builder().setDetectorMode(mode);
        if (enableMultipleObjects) {
            builder.enableMultipleObjects();
        }
        if (enableClassification) {
            builder.enableClassification();
        }
        return builder.build();
    }

    public static boolean isCameraLiveViewportEnabled(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String prefKey = context.getString(R.string.pref_key_camera_live_viewport);
        return sharedPreferences.getBoolean(prefKey, false);
    }

    private PreferenceUtils() {
    }
}
