package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.dialog.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.drawn_store.R;
import com.drawn_store.common.pref.PrefManager;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.function.common.constants.FConstants;
import com.drawn_store.function.common.objects.DistrictInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DistrictAdapter extends RecyclerView.Adapter<DistrictAdapter.ViewHodel> {

    private Context context;
    private List<DistrictInfo> districtInfos;
    private ClickItemNameDistrict clickItemName;
    private String nameDistrict;
    public DistrictAdapter(Context context, String nameDistrict) {
        this.context = context;
        this.nameDistrict = nameDistrict;
    }

    public void swap(List<DistrictInfo> districtInfos) {
        this.districtInfos = districtInfos;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DistrictAdapter.ViewHodel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_text_address, parent, false);
        return new ViewHodel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DistrictAdapter.ViewHodel holder, int position) {
        DistrictInfo districtInfo = districtInfos.get(position);
        PrefManager prefManager = new PrefManager(context);
        holder.tvTextAddress.setText(districtInfo.getName());
        String personJsonString = Utils.getGsonParser().toJson(districtInfo);
        if (districtInfo.getName().equals(nameDistrict)){
            holder.imgCheck.setVisibility(View.VISIBLE);
        }else {
            holder.imgCheck.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.imgCheck.setVisibility(View.VISIBLE);
                clickItemName.setNameDistrict(view,position,districtInfo.getName());
                prefManager.saveSetting(FConstants.KEY_JSON_DISTRICT,personJsonString);
            }
        });
    }
    public void setOnClickName(ClickItemNameDistrict clickName){
        this.clickItemName = clickName;
    }


    @Override
    public int getItemCount() {
        return districtInfos.size();
    }

    public class ViewHodel extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTextAddress)
        TextView tvTextAddress;

        @BindView(R.id.imgCheck)
        ImageView imgCheck;

        public ViewHodel(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imgCheck.setVisibility(View.GONE);
        }
    }

    public interface ClickItemNameDistrict{
        void setNameDistrict(View view, int position, String name);
    }
}
