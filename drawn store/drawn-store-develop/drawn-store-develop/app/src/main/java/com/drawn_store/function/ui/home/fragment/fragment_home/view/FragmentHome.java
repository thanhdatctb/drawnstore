package com.drawn_store.function.ui.home.fragment.fragment_home.view;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.drawn_store.R;
import com.drawn_store.common.api.ErrorCode;
import com.drawn_store.common.base.BaseFragment;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.common.view.ToastUtils;
import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.objects.MenuHome;
import com.drawn_store.function.common.objects.ProductsInfo;
import com.drawn_store.function.common.objects.response.ProductsHighestViewRes;
import com.drawn_store.function.common.objects.response.ProductsNewRes;
import com.drawn_store.function.common.objects.response.ProductsSaleRes;
import com.drawn_store.function.common.objects.response.ProductsTypeRes;
import com.drawn_store.function.common.view.BackReload;
import com.drawn_store.function.common.view.MenuHomeAdapter;
import com.drawn_store.function.common.view.OnRecyclerViewItemClickListener;
import com.drawn_store.function.ui.home.fragment.fragment_cart.view.FragmentCart;
import com.drawn_store.function.ui.home.fragment.fragment_detail.FragmentDetailProduct;
import com.drawn_store.function.ui.home.fragment.fragment_home.presenter.ProductPresenterImpl;
import com.drawn_store.function.ui.home.fragment.fragment_home.view.adapter.ProductsNewAdapter;
import com.drawn_store.function.ui.home.fragment.fragment_home.view.adapter.ProductsSaleAdapter;
import com.drawn_store.function.ui.home.fragment.fragment_search.view.FragmentSearch;
import com.drawn_store.function.ui.home.fragment.fragment_settings.FragmentSettings;
import com.google.android.material.navigation.NavigationView;
import com.mindorks.editdrawabletext.DrawablePosition;
import com.mindorks.editdrawabletext.EditDrawableText;
import com.mindorks.editdrawabletext.OnDrawableClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import softpro.naseemali.ShapedNavigationView;

public class FragmentHome extends BaseFragment implements NavigationView.OnNavigationItemSelectedListener,
        BaseView, View.OnClickListener, OnRecyclerViewItemClickListener, MenuHomeAdapter.ItemMenuClickListener,
        BackReload {

    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.nav_view)
    ShapedNavigationView navigationView;
    @BindView(R.id.rvLisSale)
    RecyclerView rvLisSale;
    @BindView(R.id.rvLisNew)
    RecyclerView rvLisNew;
    @BindView(R.id.rvListHighestView)
    RecyclerView rvListHighestView;
    @BindView(R.id.tvSeeMoreSale)
    TextView tvSeeMoreSale;
    @BindView(R.id.tvSeeHighestView)
    TextView tvSeeHighestView;
    @BindView(R.id.tvSeeMoreNew)
    TextView tvSeeMoreNew;
    @BindView(R.id.editSearch)
    EditDrawableText editSearch;
    @BindView(R.id.logoNavigation)
    ImageView logoNavigation;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.rvMenu)
    RecyclerView rvMenu;
    private ProductPresenterImpl productPresenter;
    private ProductsSaleAdapter productsSaleAdapter;
    private ProductsNewAdapter productsNewAdapter;
    private List<ProductsInfo> productsInfoList;
    private List<MenuHome> dataSection;
    private ProductsTypeRes productsTypeRes;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        isHideButtonBack(true);
        backgroudToolbar(true);
        setTitle(R.string.app_name);
        setUpMenuCart();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                requireActivity(), drawerLayout, mToolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        getDataNavigation();
        productPresenter = new ProductPresenterImpl(this);
        productPresenter.productsSale(loginRes.getData().getId(), "6");
        productPresenter.productsNew(loginRes.getData().getId(), "9");
        productPresenter.productsHighestView(loginRes.getData().getId(), "9");
        productPresenter.productsType();
        showProgress();
        setOnClick();
    }

    @Override
    public void success(Object x) {
        super.success(x);
        hideProgress();
        if (x instanceof ProductsSaleRes) {
            ProductsSaleRes productsSaleRes = (ProductsSaleRes) x;
            productsInfoList = new ArrayList<>();
            productsInfoList = productsSaleRes.getData();
            getDataProductSale();
            productsSaleAdapter.swap(productsInfoList);
        } else if (x instanceof ProductsNewRes) {
            ProductsNewRes productsNewRes = (ProductsNewRes) x;
            productsInfoList = new ArrayList<>();
            productsInfoList = productsNewRes.getData();
            getDataProductNew();
            productsNewAdapter.swap(productsInfoList);
        } else if (x instanceof ProductsHighestViewRes) {
            ProductsHighestViewRes productsHighestViewRes = (ProductsHighestViewRes) x;
            productsInfoList = new ArrayList<>();
            productsInfoList = productsHighestViewRes.getData();
            getDataProductHighestView();
            productsNewAdapter.swap(productsInfoList);
        } else if (x instanceof ProductsTypeRes) {
            productsTypeRes = (ProductsTypeRes) x;
            getDataMenu(productsTypeRes);
        }
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj err = (BaseObj) o;
        if (err.getStatusCode() == ErrorCode.BAD_REQUEST) {
            DialogUtil.showDialogMessage(requireActivity(), getResources().getString(R.string.data_null));
        } else {
            DialogUtil.showDialogMessage(requireActivity(), err.getMessage() + " (" + err.getStatusCode() + ")");
        }
    }

    private void setOnClick() {
        tvSeeMoreSale.setOnClickListener(this);
        tvSeeHighestView.setOnClickListener(this);
        tvSeeMoreNew.setOnClickListener(this);
        editSearch.setOnClickListener(this);
        editSearch.setDrawableClickListener(new OnDrawableClickListener() {
            @Override
            public void onClick(@NotNull DrawablePosition drawablePosition) {
                switch (drawablePosition) {
                    case TOP:
                        break;
                    case LEFT:
                        replaceFragment(new FragmentSearch().newInstance(), true, false);
                        break;
                    case RIGHT:
                        ToastUtils.showToast(requireContext(), "Đang phát  triển", Gravity.BOTTOM);
                        break;
                    case BOTTOM:
                        break;
                }
            }
        });
    }

    private void getDataNavigation() {
        tvName.setText(loginRes.getData().getName());
        if (loginRes.getData().getAvatar() == null) {
            logoNavigation.setImageResource(R.drawable.avata_de);
        } else {
            if (loginRes.getData().getAvatar().contains("http")) {
                Glide.with(this)
                        .load(loginRes.getData().getAvatar())
                        .apply(RequestOptions.circleCropTransform())
                        .into(logoNavigation);
            } else {
                Glide.with(this)
                        .load(ConstantApi.MAIN_DNS + "/" + loginRes.getData().getAvatar())
                        .apply(RequestOptions.circleCropTransform())
                        .into(logoNavigation);
            }
        }
    }

    private void getDataMenu(ProductsTypeRes productsTypeRes) {
        dataSection = new ArrayList<>();
        for (int i = 0; i < productsTypeRes.getData().size(); i++) {
            dataSection.add(new MenuHome(productsTypeRes.getData().get(i).getId(),
                    productsTypeRes.getData().get(i).getName(),
                    setIcon(productsTypeRes.getData().get(i).getId())));
        }
        MenuHomeAdapter menuHomeAdapter = new MenuHomeAdapter(requireContext());
        rvMenu.setLayoutManager(new LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false));
//        rvMenu.setItemAnimator(new DefaultItemAnimator());
        rvMenu.setHasFixedSize(true);
        rvMenu.setAdapter(menuHomeAdapter);
        menuHomeAdapter.swap(dataSection);
        menuHomeAdapter.setOnClickListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawerLayout.closeDrawer(GravityCompat.START);
        int id = item.getItemId();
        return true;
    }

    private void getDataProductSale() {
        productsSaleAdapter = new ProductsSaleAdapter(requireContext());
        productsSaleAdapter.setOnClickListener(this);
        rvLisSale.setLayoutManager(new GridLayoutManager(requireContext(), 1, GridLayoutManager.HORIZONTAL, false));
        rvLisSale.setAdapter(productsSaleAdapter);
    }

    private void getDataProductNew() {
        productsNewAdapter = new ProductsNewAdapter(requireContext(), true);
        productsNewAdapter.setOnClickListener(this);
        rvLisNew.setLayoutManager(new GridLayoutManager(requireContext(), 1, GridLayoutManager.HORIZONTAL, false));
        rvLisNew.setHasFixedSize(true);
        rvLisNew.setAdapter(productsNewAdapter);
    }

    private void getDataProductHighestView() {
        productsNewAdapter = new ProductsNewAdapter(requireContext(), false);
        productsNewAdapter.setOnClickListener(this);
        rvListHighestView.setHasFixedSize(true);
        rvListHighestView.setLayoutManager(new LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false));
        rvListHighestView.setAdapter(productsNewAdapter);
    }


    private void setUpMenuCart() {
        int positon = 0;
        if (mToolbar != null) {
            mToolbar.getMenu().clear();
            if (positon == 0) {
                mToolbar.inflateMenu(R.menu.menu_cart);
            } else {
                mToolbar.inflateMenu(R.menu.menu_cart_available);
            }
            mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.itemMenuCardAvailable:
                            replaceFragment(new FragmentCart().newInstance(), true, false);
                            break;
                        case R.id.itemMenuCart:
                            replaceFragment(new FragmentCart().newInstance(), true, false);
                            break;
                        case R.id.itemSettingCart:
                            replaceFragment(new FragmentSettings().newInstance(), true, false);
                    }
                    return true;
                }
            });

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvSeeMoreSale:
                replaceFragment(new FragmentSeeMore().newInstance(getResources().getString(R.string.product_sale), 1), true, true);
                break;
            case R.id.tvSeeMoreNew:
                replaceFragment(new FragmentSeeMore().newInstance(getResources().getString(R.string.product_new), 2), true, true);
                break;
            case R.id.tvSeeHighestView:
                replaceFragment(new FragmentSeeMore().newInstance(getResources().getString(R.string.highest_view), 3), true, true);
                break;
            case R.id.editSearch:
                replaceFragment(new FragmentSearch().newInstance(), true, false);
                break;
        }
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {

    }

    @Override
    public void recyclerViewListClickedData(View v, int position, ProductsInfo productsInfo) {
        productPresenter.addView(productsInfo.getId());
        replaceFragment(new FragmentDetailProduct().newInstance(productsInfo).setReloadFragment(this), true, false);
    }

    @Override
    public void onClickItem(View v, int position) {
        switch (position) {
            case 0:
            case 1:
            case 2:
                replaceFragment(new FragmentProduct().newInstance(dataSection.get(position).getName(),
                        productsTypeRes.getData().get(position).getId()), true, true);
                break;
        }
    }

    private int setIcon(int id) {
        if (id == 1)
            return R.drawable.ic_fashion_men;
        else if (id == 2)
            return R.drawable.ic_faction_fmale;
        else
            return R.drawable.ic_fashion_baby;
    }


    @Override
    public void reloadFragment() {
//        showProgress();
        productPresenter.productsSale(loginRes.getData().getId(), "6");
        productPresenter.productsNew(loginRes.getData().getId(), "9");
        productPresenter.productsHighestView(loginRes.getData().getId(), "9");
    }
}
