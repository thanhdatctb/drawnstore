
package com.drawn_store.function.common.objects.response;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.function.common.objects.UserInfo;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class UserRes extends BaseObj {

    @SerializedName("data")
    private UserInfo mData;

    public UserInfo getData() {
        return mData;
    }

    public void setData(UserInfo data) {
        mData = data;
    }
}
