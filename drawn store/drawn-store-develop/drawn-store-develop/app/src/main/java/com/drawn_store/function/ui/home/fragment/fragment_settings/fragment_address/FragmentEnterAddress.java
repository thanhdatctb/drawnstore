package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.drawn_store.R;
import com.drawn_store.common.api.ErrorCode;
import com.drawn_store.common.base.BaseActivity;
import com.drawn_store.common.base.BaseFragment;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.common.view.DialogMessage;
import com.drawn_store.function.common.constants.FConstants;
import com.drawn_store.function.common.objects.DistrictInfo;
import com.drawn_store.function.common.objects.ListAddressInfo;
import com.drawn_store.function.common.objects.ProvinceInfo;
import com.drawn_store.function.common.objects.WardsInfo;
import com.drawn_store.function.common.objects.request.AddressRep;
import com.drawn_store.function.common.objects.request.UpdateAddressRep;
import com.drawn_store.function.common.objects.response.AddAddressRes;
import com.drawn_store.function.common.objects.response.DeleteAddressRes;
import com.drawn_store.function.common.objects.response.UpdateAddressRes;
import com.drawn_store.function.common.view.BackReload;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.dialog.presenter.IAddressPresenterImpl;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.dialog.view.DialogChooseAddress;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.switchmaterial.SwitchMaterial;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentEnterAddress extends BaseFragment implements View.OnClickListener, DialogChooseAddress.SetNameEditext {
    public FragmentEnterAddress newInstance(boolean checkDefault) {
        Bundle args = new Bundle();
        args.putBoolean(FConstants.KEY_CHECK_DEFAULT, checkDefault);
        FragmentEnterAddress fragment = new FragmentEnterAddress();
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentEnterAddress newInstanceUpdate(ListAddressInfo addressInfo) {
        Bundle args = new Bundle();
        String personJsonAddress = Utils.getGsonParser().toJson(addressInfo);
        args.putString(FConstants.KEY_JSON_ADDRESS, personJsonAddress);
        FragmentEnterAddress fragment = new FragmentEnterAddress();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.editFullName)
    EditText editFullName;
    @BindView(R.id.editPhone)
    EditText editPhone;
    @BindView(R.id.edtWards)
    EditText edtWards;
    @BindView(R.id.edtProvince)
    EditText edtProvince;
    @BindView(R.id.edtDistrict)
    EditText edtDistrict;
    @BindView(R.id.editStreet)
    EditText editStreet;
    @BindView(R.id.bntSave)
    MaterialButton bntSave;
    @BindView(R.id.swCheck)
    SwitchMaterial swCheck;
    @BindView(R.id.rlDeleteAddress)
    RelativeLayout rlDeleteAddress;
    private DialogChooseAddress dialogWards;
    private ProvinceInfo provinceInfo;
    private DistrictInfo districtInfo;
    private WardsInfo wardsInfo;
    private ListAddressInfo listAddressInfo;
    private boolean checkAdd = true;
    String personJsonProvince, personJsonDistrict, personJsonWards;
    String code, codeDistrict;
    private boolean checkDefault = true;
    private Bundle args;
    private String personJsonAddress;
    private IAddressPresenterImpl iAddressPresenterImpl;
    private String nameProvince = " ", nameDistrict = " ", nameWards = " ";
    private BackReload backReload;
    public FragmentEnterAddress checkAdd(boolean checkAdd) {
        this.checkAdd = checkAdd;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_enter_address, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        isHideButtonBack(false);
        backgroudToolbar(true);
        iAddressPresenterImpl = new IAddressPresenterImpl(this);
        args = getArguments();
        showUI();
        setListener();
        getTextAddress();
    }
    public FragmentEnterAddress setReloadFragment(BackReload backReload) {
        this.backReload = backReload;
        return this;
    }
    private void showUI() {
        if (checkAdd) {
            setTitle(R.string.add_address);
            checkDefault = args.getBoolean(FConstants.KEY_CHECK_DEFAULT);
            rlDeleteAddress.setVisibility(View.GONE);
        } else {
            setTitle(R.string.update_address);
            personJsonAddress = args.getString(FConstants.KEY_JSON_ADDRESS);
            listAddressInfo = Utils.getGsonParser().fromJson(personJsonAddress, ListAddressInfo.class);
            rlDeleteAddress.setVisibility(View.VISIBLE);
            editFullName.setText(listAddressInfo.getName());
            editPhone.setText(listAddressInfo.getPhoneNumber());
            edtProvince.setText(Utils.splipStringProvince(listAddressInfo.getAddress()));
            edtDistrict.setText(Utils.splipStringDistrict(listAddressInfo.getAddress()));
            edtWards.setText(Utils.splipStringWards(listAddressInfo.getAddress()));
            editStreet.setText(Utils.splipStringStart(listAddressInfo.getAddress()));
            swCheck.setChecked(listAddressInfo.getIsDefault());
            checkDefault = listAddressInfo.getIsDefault();
            code = listAddressInfo.getProvinceCode();
            codeDistrict = listAddressInfo.getDistrictCode();
            nameProvince = Utils.splipStringProvince(listAddressInfo.getAddress());
            nameDistrict = Utils.splipStringDistrict(listAddressInfo.getAddress());
            nameWards = Utils.splipStringWards(listAddressInfo.getAddress());
        }
    }

    private void setListener() {
        edtWards.setOnClickListener(this);
        edtProvince.setOnClickListener(this);
        edtDistrict.setOnClickListener(this);
        bntSave.setOnClickListener(this);
        rlDeleteAddress.setOnClickListener(this);
        swCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    checkDefault = true;
                } else {
                    checkDefault = false;
                }
            }
        });
    }

    @Override
    public void success(Object x) {
        super.success(x);
        hideProgress();
        if (x instanceof AddAddressRes) {
            DialogUtil.showDialogMessage(requireContext(), getResources().getString(R.string.announce), R.string.add_success, new DialogMessage.OnDialogMessageListener() {
                @Override
                public void yesClick() {
                    ((BaseActivity) getActivity()).onBackPressed();
                    iAddressPresenterImpl.listAddres(loginRes.getData().getId());
                }
            });
        } else if (x instanceof DeleteAddressRes) {
            DialogUtil.showDialogMessage(requireContext(), getResources().getString(R.string.note_delete), R.string.ok, new DialogMessage.OnDialogMessageListener() {
                @Override
                public void yesClick() {
                    ((BaseActivity) getActivity()).onBackPressed();
                    iAddressPresenterImpl.listAddres(loginRes.getData().getId());
                }
            });
        }else if (x instanceof UpdateAddressRes){
            DialogUtil.showDialogMessage(requireContext(), getResources().getString(R.string.update_success), R.string.ok, new DialogMessage.OnDialogMessageListener() {
                @Override
                public void yesClick() {
                    ((BaseActivity) getActivity()).onBackPressed();
                    iAddressPresenterImpl.listAddres(loginRes.getData().getId());
                }
            });
        }
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj err = (BaseObj) o;
        if (err.getStatusCode() == ErrorCode.CODE_NOT_FOUND) {
            DialogUtil.showDialogMessage(requireActivity(), requireActivity().getResources().getString(R.string.data_null));
        } else {
            DialogUtil.showDialogMessage(requireActivity(), err.getMessage() + " (" + err.getStatusCode() + ")");
            Log.d("ABC", err.getMessage());
        }
    }

    private void showDialogWards(String title, String code, String codeDistrict, int typeAddress) {
        dialogWards = new DialogChooseAddress(requireContext()).newInstance();
        dialogWards.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getTextAddress();
            }
        });
        dialogWards.setNameEditext(this);
        dialogWards.setTitle(title);
        dialogWards.setCode(code);
        dialogWards.setCodeDistrict(codeDistrict);
        dialogWards.setTypeAddress(typeAddress);
        dialogWards.setNameProvince(nameProvince);
        dialogWards.setNameDistrict(nameDistrict);
        dialogWards.setNameWards(nameWards);
        dialogWards.show();
    }

    private void getTextAddress() {
        personJsonProvince = prefManager.getString(FConstants.KEY_JSON_PROVINCE);
        personJsonDistrict = prefManager.getString(FConstants.KEY_JSON_DISTRICT);
        personJsonWards = prefManager.getString(FConstants.KEY_JSON_WARDS);
        if (personJsonProvince != null) {
            provinceInfo = Utils.getGsonParser().fromJson(personJsonProvince, ProvinceInfo.class);
            code = provinceInfo.getCode();
            nameProvince = provinceInfo.getName();
        }
        if (personJsonDistrict != null) {
            districtInfo = Utils.getGsonParser().fromJson(personJsonDistrict, DistrictInfo.class);
            codeDistrict = districtInfo.getCode();
            nameDistrict = districtInfo.getName();
        }
        if (personJsonWards != null) {
            wardsInfo = Utils.getGsonParser().fromJson(personJsonWards, WardsInfo.class);
            nameWards = wardsInfo.getName();
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edtWards:
                if (!nameDistrict.equals(" ")) {
                    showDialogWards(getResources().getString(R.string.wards), "3", codeDistrict, 2);
                } else {
                    DialogUtil.showDialogMessage(requireContext(), "Vui lòng chọn Quận/Huyện trước");
                }
                break;
            case R.id.edtDistrict:
                if (!nameProvince.equals(" ")) {
                    showDialogWards(getResources().getString(R.string.district), code, "0", 1);
                } else {
                    DialogUtil.showDialogMessage(requireContext(), "Vui lòng chọn Tỉnh/Thành Phố trước");
                }
                break;
            case R.id.edtProvince:
                showDialogWards(getResources().getString(R.string.province_ity), "0", "0", 0);
                break;
            case R.id.bntSave:
                saveAddress();
                break;
            case R.id.rlDeleteAddress:
                if (listAddressInfo.getIsDefault()) {
                    DialogUtil.showDialogMessage(requireContext(), "Không thể xóa địa chỉ mặt định");
                } else {
                    showProgress();
                    iAddressPresenterImpl.deleteAddress(listAddressInfo.getId());
                }
                break;

        }
    }

    private void saveAddress() {
        String fullName = editFullName.getText().toString();
        String phone = editPhone.getText().toString();
        String province = edtProvince.getText().toString();
        String district = edtDistrict.getText().toString();
        String wards = edtWards.getText().toString();
        String street = editStreet.getText().toString();
        if (fullName.equals("") || phone.equals("") || province.equals("") || district.equals("")
                || wards.equals("") || street.equals("")) {
            DialogUtil.showDialogMessage(requireContext(), getResources().getString(R.string.fill_full_info));
        } else {
            showProgress();
            String address = street + ", " + wards + ", " + district + ", " + province;
            if (checkAdd) {
                iAddressPresenterImpl.addAddress(new AddressRep(loginRes.getData().getId(),
                        fullName, phone, address, checkDefault, code, codeDistrict));
            } else {
                iAddressPresenterImpl.updateAddress(new UpdateAddressRep(listAddressInfo.getId(),loginRes.getData().getId(),
                        fullName, phone, address, checkDefault, code, codeDistrict));
            }
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        prefManager.remove(FConstants.KEY_JSON_PROVINCE);
        prefManager.remove(FConstants.KEY_JSON_DISTRICT);
        prefManager.remove(FConstants.KEY_JSON_WARDS);
        backReload.reloadFragment();
    }

    @Override
    public void setNameProvince(String name) {
        edtProvince.setText(name);
    }

    @Override
    public void setNameDistrict(String name) {
        edtDistrict.setText(name);
    }

    @Override
    public void setNameWards(String name) {
        edtWards.setText(name);
    }

}
