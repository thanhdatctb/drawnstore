package com.drawn_store.common.presenter;

import com.drawn_store.common.ultility.Logger;

public abstract class InteractorCallback<T, E> implements BaseResponse<T, E> {
    public void response(T obj){
        Logger.d("Gotcha response", "Skip processing response from...");
    }
}