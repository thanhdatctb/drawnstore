package com.drawn_store.function.common.api;

import com.drawn_store.common.api.RetrofitClient;
import com.drawn_store.function.common.api.request.AddressRequest;
import com.drawn_store.function.common.api.request.CartRequest;
import com.drawn_store.function.common.api.request.LoginRequest;
import com.drawn_store.function.common.api.request.OrderRequest;
import com.drawn_store.function.common.api.request.ProductsRequest;
import com.drawn_store.function.common.api.request.SearchRequest;
import com.drawn_store.function.common.api.request.UserRequest;

public class ApiUtils {
    private ApiUtils() {
    }

    public static LoginRequest getLoginRequest() {
        return RetrofitClient.getInstance().create(LoginRequest.class);
    }


    public static UserRequest getUserRequest() {
        return RetrofitClient.getInstance().create(UserRequest.class);
    }


    public static ProductsRequest getProductsRequest() {
        return RetrofitClient.getInstance().create(ProductsRequest.class);
    }

    public static CartRequest getCartRequest() {
        return RetrofitClient.getInstance().create(CartRequest.class);
    }

    public static AddressRequest getAddressRequest() {
        return RetrofitClient.getInstance().create(AddressRequest.class);
    }

    public static SearchRequest getSearchRequest() {
        return RetrofitClient.getInstance().create(SearchRequest.class);
    }
    public static OrderRequest getOrderRequest() {
        return RetrofitClient.getInstance().create(OrderRequest.class);
    }
}
