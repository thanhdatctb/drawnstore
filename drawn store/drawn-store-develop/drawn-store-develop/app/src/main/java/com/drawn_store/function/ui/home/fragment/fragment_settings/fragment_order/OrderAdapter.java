package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_order;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.drawn_store.R;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.objects.OrderInfo;
import com.drawn_store.function.common.objects.PaymentOrderInfo;
import com.drawn_store.function.common.objects.ProductsInfo;
import com.drawn_store.function.common.objects.ProductsOrderInfo;
import com.drawn_store.function.ui.home.fragment.fragment_payment.view.PaymentOrderAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHodel> {

    private Context context;
    private List<ProductsOrderInfo> productsInfos;
    private SetDataOrder setDataOrder;

    public OrderAdapter(Context context) {
        this.context = context;
    }

    public void swap(List<ProductsOrderInfo> productsInfos) {
        this.productsInfos = productsInfos;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OrderAdapter.ViewHodel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_order, parent, false);
        return new ViewHodel(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull OrderAdapter.ViewHodel holder, int position) {
        ProductsOrderInfo orderInfo = productsInfos.get(position);
//        holder.tvSize.setText(paymentOrderInfo.getColor() + paymentOrderInfo.getSize());
//        holder.tvAmount.setText("SL: " + paymentOrderInfo.getAmount());
        double saving;
        int amount = 0;
        holder.tvNameProducts.setText(orderInfo.getName());
        if (orderInfo.getDiscountPercent().equals("0%")) {
            holder.tvPric.setVisibility(View.GONE);
            holder.tvDiscountPercent.setVisibility(View.GONE);
            saving = 0;
            holder.tvDiscountPric.setText(Utils.formatAmount(Double.parseDouble(orderInfo.getPrice())));
        } else {
            holder.tvPric.setVisibility(View.VISIBLE);
            holder.tvDiscountPercent.setVisibility(View.VISIBLE);
            holder.tvDiscountPric.setText(Utils.formatAmount(Double.parseDouble(orderInfo.getDiscountPrice())));
            holder.tvPric.setText(Utils.formatAmount(Double.parseDouble(orderInfo.getPrice())));
            holder.tvDiscountPercent.setText("-" + orderInfo.getDiscountPercent());
            saving = (Double.parseDouble(orderInfo.getPrice()) * orderInfo.getAmount()) - Double.parseDouble(orderInfo.getDiscountPrice()) * orderInfo.getAmount();
        }
        amount = orderInfo.getAmount();
        amount++;
        setDataOrder.dataOrder(saving,amount);
        holder.tvAmount.setText("SL: " + orderInfo.getAmount());
        Glide.with(context)
                .load(ConstantApi.MAIN_DNS + "/" + orderInfo.getImages().get(0))
                .transform(new CenterCrop(), new RoundedCorners(8))
                .error(R.drawable.logo)
                .into(holder.imgProducts);
        holder.tvSize.setText(orderInfo.getColor() + orderInfo.getSize());
    }


    @Override
    public int getItemCount() {
        return productsInfos.size();
    }

    public void setOnDataOrder(SetDataOrder setDataOrder) {
        this.setDataOrder = setDataOrder;
    }
    public class ViewHodel extends RecyclerView.ViewHolder {
        @BindView(R.id.imgProducts)
        ImageView imgProducts;
        @BindView(R.id.tvNameProducts)
        TextView tvNameProducts;
        @BindView(R.id.tvDiscountPric)
        TextView tvDiscountPric;
        @BindView(R.id.tvPric)
        TextView tvPric;
        @BindView(R.id.tvSize)
        TextView tvSize;
        @BindView(R.id.tvDiscountPercent)
        TextView tvDiscountPercent;
        @BindView(R.id.tvAmount)
        TextView tvAmount;;

        public ViewHodel(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvPric.setPaintFlags(tvPric.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }

    public interface SetDataOrder {
        void dataOrder(double countSale,int amount);
    }
}
