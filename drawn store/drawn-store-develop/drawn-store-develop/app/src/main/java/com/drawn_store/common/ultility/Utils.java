package com.drawn_store.common.ultility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class Utils {
    public static int orientation;
    public static boolean sIsFaceDetect = false;
    public static int tryingDetectTime = 0;

    public static void hideSoftKeyboard(Activity activity) {
        if (activity != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            //Find the currently focused view, so we can grab the correct window token from it.
            View view = activity.getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(activity);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void hideSoftKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (view.isFocused())
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

    }


    /**
     * show password of edittext when click to icon show
     *
     * @param editText
     * @param event
     */
    public static void showPassword(EditText editText, MotionEvent event) {
        if (editText.getText().length() > 0) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    editText.setInputType(InputType.TYPE_CLASS_TEXT);
                    editText.setSelection(editText.getText().length());
                    break;
                case MotionEvent.ACTION_UP:
                    editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    editText.setSelection(editText.getText().length());
                    break;
                default:
                    break;
            }
        }
    }

    public static void hideNavigaitonBar(final DialogFragment dialogFragment) {
        dialogFragment.getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        dialogFragment.getDialog().getWindow().getDecorView().setSystemUiVisibility(
                dialogFragment.getActivity().getWindow().getDecorView().getSystemUiVisibility());

        dialogFragment.getDialog().setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                if (dialogFragment != null
                        && dialogFragment.getDialog() != null
                        && dialogFragment.getActivity() != null
                ) {
                    //Clear the not focusable flag from the window
                    dialogFragment.getDialog().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);

                    //Update the WindowManager with the new attributes (no nicer way I know of to do this)..
                    WindowManager wm = (WindowManager) dialogFragment.getActivity().getSystemService(Context.WINDOW_SERVICE);
                    wm.updateViewLayout(dialogFragment.getDialog().getWindow().getDecorView(),
                            dialogFragment.getDialog().getWindow().getAttributes());
                }
            }
        });
    }

    @SuppressLint({"MissingPermission",})
    public static String getDeviceImel(Activity activity) {
        String deviceUniqueIdentifier = null;
        TelephonyManager tm = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        if (null != tm) {
            deviceUniqueIdentifier = tm.getDeviceId();
        }
        if (null == deviceUniqueIdentifier || 0 == deviceUniqueIdentifier.length()) {
            deviceUniqueIdentifier = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return deviceUniqueIdentifier;
    }

    public static void lockTask(Activity activity) {
        // start lock task mode if it's not already active
        ActivityManager am = (ActivityManager) activity.getSystemService(
                Context.ACTIVITY_SERVICE);
        // ActivityManager.getLockTaskModeState api is not available in pre-M.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (!am.isInLockTaskMode()) {
                    activity.startLockTask();
                }
            }
        } else {
            if (am.getLockTaskModeState() ==
                    ActivityManager.LOCK_TASK_MODE_NONE) {
                activity.startLockTask();
            }
        }
    }

    public static boolean isSafeFragment(Fragment frag) {
        return !(frag.isRemoving() || frag.getActivity() == null || frag.isDetached() || !frag.isAdded() || frag.getView() == null);
    }

    public static void setIsFaceDetect(boolean isFaceDetect) {
        sIsFaceDetect = isFaceDetect;
        tryingDetectTime = 0;
    }

    public static Gson getGsonParser() {
        Gson gson = null;
        if (null == gson) {
            GsonBuilder builder = new GsonBuilder();
            gson = builder.create();
        }
        return gson;
    }

    public static String formatAmount(double amount) {
        Locale lc = new Locale("vi", "VN");
        NumberFormat nf = NumberFormat.getCurrencyInstance(lc);
        return nf.format(amount);
    }

    public static String splipStringStart(String address) {
        String[] separated = address.split(",");
        return separated[0];
    }

    public static String splipStringEnd(String address) {
        String[] separated = address.split(",\\s");
        return separated[1] + ", " + separated[2] + ", " + separated[3];
    }

    public static String splipStringProvince(String address) {
        String[] separated = address.split(",\\s");
        return separated[3];
    }

    public static String splipStringDistrict(String address) {
        String[] separated = address.split(",\\s");
        return separated[2];
    }

    public static String splipStringWards(String address) {
        String[] separated = address.split(",\\s");
        return separated[1];
    }

    public static String convertDate(String dateTime) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", new Locale("vi", "VN"));
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy", new Locale("vi", "VN"));
        Date date = null;
        try {
            date = inputFormat.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputFormat.format(date);
    }

}