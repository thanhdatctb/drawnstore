package com.drawn_store.function.common.api.request;

import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.objects.request.ChangerPasswordForgotRep;
import com.drawn_store.function.common.objects.request.ChangerPasswordRep;
import com.drawn_store.function.common.objects.request.CheckEmailRep;
import com.drawn_store.function.common.objects.request.RegisterReq;
import com.drawn_store.function.common.objects.response.RegisterRes;
import com.drawn_store.function.common.objects.response.UpdateAccountRes;
import com.drawn_store.function.common.objects.response.UserCodeRes;
import com.drawn_store.function.common.objects.response.UserRes;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface UserRequest {

    @PUT(ConstantApi.URL_EMPLOYEE_REGISTER)
    Call<RegisterRes> register(@Body RegisterReq registerReq);


    @POST(ConstantApi.URL_CHECK_EMAIL)
    Call<UserCodeRes> checkEmail(@Body CheckEmailRep checkEmailRep);

    @POST(ConstantApi.URL_CHANGER_PASSWORD)
    Call<UserRes> changerPassword(@Body ChangerPasswordRep changerPasswordRep);

    @POST(ConstantApi.URL_CHANGER_PASSWORD_FORGOT)
    Call<UserRes> changerPasswordForGot(@Body ChangerPasswordForgotRep changerPasswordForgotRep);

    @Multipart
    @POST(ConstantApi.URL_UPDATE_ACCOUNT)
    Call<UpdateAccountRes> updateAccount(@Part("email") RequestBody email, @Part("name") RequestBody name, @Part MultipartBody.Part avatar);

    @HTTP(method = "DELETE", path = ConstantApi.URL_DELETE_ACCOUNT, hasBody = true)
    Call<UserRes> deleteAccount(@Path("userId") int cartId);
}
