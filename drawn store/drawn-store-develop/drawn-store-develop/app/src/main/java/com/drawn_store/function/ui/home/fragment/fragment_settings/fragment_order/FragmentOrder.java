package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_order;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.drawn_store.R;
import com.drawn_store.common.api.ErrorCode;
import com.drawn_store.common.base.BaseFragment;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.function.common.constants.FConstants;
import com.drawn_store.function.common.objects.OrderInfo;
import com.drawn_store.function.common.objects.response.OrderRes;
import com.drawn_store.function.ui.home.fragment.fragment_payment.presenter.OrderPresenterImpl;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentOrder extends BaseFragment {
    public FragmentOrder newInstance(int position, String name) {
        Bundle args = new Bundle();
        args.putString(FConstants.KEY_IS_TITLE, name);
        args.putInt(FConstants.KEY_POSITION, position);
        FragmentOrder fragment = new FragmentOrder();
        fragment.setArguments(args);
        return fragment;
    }

    private Bundle args;
    private OrderPresenterImpl orderPresenter;
    @BindView(R.id.rvOrder)
    RecyclerView rvOrder;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.tvNoteData)
    TextView tvNoteData;
    private OrderListAdapter orderAdapter;
    private List<OrderInfo> orderInfoList;
    private int positon;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        isHideButtonBack(false);
        backgroudToolbar(true);
        args = getArguments();
        positon = args.getInt(FConstants.KEY_POSITION);
        setTitle(args.getString(FConstants.KEY_IS_TITLE));
        orderPresenter = new OrderPresenterImpl(this);
        showProgress();
        checkPositon();
        loadSwipe();
    }

    @Override
    public void success(Object x) {
        super.success(x);
        hideProgress();
        if (x instanceof OrderRes) {
            OrderRes orderRes = (OrderRes) x;
            orderInfoList = orderRes.getData();
            if (orderInfoList.size() !=0) {
                tvNoteData.setVisibility(View.GONE);
                rvOrder.setVisibility(View.VISIBLE);
                orderAdapter = new OrderListAdapter(requireContext());
                rvOrder.setHasFixedSize(true);
                rvOrder.setLayoutManager(new LinearLayoutManager(requireContext()));
                rvOrder.setAdapter(orderAdapter);
                orderAdapter.swap(orderInfoList);
            }else {
                tvNoteData.setVisibility(View.VISIBLE);
                rvOrder.setVisibility(View.GONE);
            }
        }
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj err = (BaseObj) o;
        if (err.getStatusCode() == ErrorCode.BAD_REQUEST) {
            DialogUtil.showDialogMessage(requireActivity(), getResources().getString(R.string.data_null));
        } else {
            DialogUtil.showDialogMessage(requireActivity(), err.getMessage() + " (" + err.getStatusCode() + ")");
        }
        swipeRefresh.setRefreshing(false);
    }
    private void checkPositon(){
        switch (positon){
            case 0:
                orderPresenter.showOrder(loginRes.getData().getId(), "waiting");
                break;
            case 1:
                orderPresenter.showOrder(loginRes.getData().getId(), "sent");
                break;
            case 2:
                break;
            case 3:
                orderPresenter.showOrder(loginRes.getData().getId(), "canceled");
                break;
            case 4:
                orderPresenter.showOrder(loginRes.getData().getId(), "all");
                break;
        }
    }
    private void loadSwipe() {
        swipeRefresh.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorRed,
                R.color.colorBlack);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
               checkPositon();
            }
        });
    }
}
