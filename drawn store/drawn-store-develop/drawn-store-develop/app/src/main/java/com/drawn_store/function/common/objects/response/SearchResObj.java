package com.drawn_store.function.common.objects.response;

import com.drawn_store.common.object.BaseObj;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchResObj extends BaseObj {

    @SerializedName("data")
    @Expose
    private SearchByImageRes data;

    public SearchByImageRes getData() {
        return data;
    }

    public void setData(SearchByImageRes data) {
        this.data = data;
    }

}