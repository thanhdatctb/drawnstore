package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_introduce;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.FileUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.drawn_store.R;
import com.drawn_store.common.base.BaseFragment;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.common.view.DialogMessage;
import com.drawn_store.common.view.ToastUtils;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentIntroduce extends BaseFragment implements View.OnClickListener {

    public FragmentIntroduce newInstance() {
        return this;
    }

    @BindView(R.id.lnDeleteCache)
    LinearLayout lnDeleteCache;
    @BindView(R.id.tvVersion)
    TextView tvVersion;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_introduce, container, false);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        isHideButtonBack(false);
        backgroudToolbar(true);
        setTitle(R.string.introduce);
        setListener();
        try {
            PackageInfo pInfo = requireActivity().getPackageManager().getPackageInfo(requireActivity().getPackageName(), 0);
            String version = pInfo.versionName;
            String versionCode = String.valueOf(pInfo.versionCode);
            tvVersion.setText("v" + version + versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void setListener() {
        lnDeleteCache.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lnDeleteCache:
                deleteCache();
                break;
        }
    }

    private void deleteCache() {
        DialogUtil.showDialogMessage(requireContext(), R.string.title_clear_cache, R.string.clear_cache,
                R.string.no,
                R.string.yes, new DialogMessage.OnDialogMessageListener() {
                    @Override
                    public void yesClick() {
                        try {
                            File dir = requireContext().getCacheDir();
                            if (deleteDir(dir)){
                                ToastUtils.showToast(requireContext(),"Xóa thành công",
                                        Gravity.BOTTOM);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
}
