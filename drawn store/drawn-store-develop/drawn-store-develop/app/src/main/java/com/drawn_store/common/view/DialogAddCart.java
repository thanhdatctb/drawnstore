package com.drawn_store.common.view;

import android.annotation.SuppressLint;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.drawn_store.R;
import com.drawn_store.common.base.BaseBottomSheetDialogFragment;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.constants.FConstants;
import com.drawn_store.function.common.objects.PaymentOrderInfo;
import com.drawn_store.function.common.objects.ProductsInfo;
import com.drawn_store.function.common.objects.request.CartRep;
import com.drawn_store.function.common.objects.response.CartRes;
import com.drawn_store.function.common.view.OnClickOptions;
import com.drawn_store.function.common.view.ProductColorAdapter;
import com.drawn_store.function.common.view.ProductSizeAdapter;
import com.drawn_store.function.ui.home.fragment.fragment_cart.presenter.OrdelPresenterImpl;
import com.drawn_store.function.ui.home.fragment.fragment_payment.view.FragmentPayment;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogAddCart extends BaseBottomSheetDialogFragment implements View.OnClickListener, OnClickOptions {

    public DialogAddCart newInstance(int showBotton, ProductsInfo productsInfo) {
        Bundle args = new Bundle();
        args.putInt(FConstants.KEY_SHOW_BOTTON, showBotton);
        args.putSerializable(FConstants.KEY_PRODUCT_INFO, productsInfo);
        DialogAddCart fragment = new DialogAddCart();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.tvDiscountPric)
    TextView tvDiscountPric;
    @BindView(R.id.tvPric)
    TextView tvPric;
    @BindView(R.id.tvDiscountPercent)
    TextView tvDiscountPercent;
    @BindView(R.id.tvSize)
    TextView tvSize;
    @BindView(R.id.tvColor)
    TextView tvColor;
    @BindView(R.id.imgAvatarProduct)
    ImageView imgAvatarProduct;
    @BindView(R.id.rvOptionsColor)
    RecyclerView rvOptionsColor;
    @BindView(R.id.rvOptionsSize)
    RecyclerView rvOptionsSize;
    @BindView(R.id.imgClosed)
    ImageView imgClosed;
    @BindView(R.id.lnShowAll)
    LinearLayout lnShowAll;
    @BindView(R.id.lnOneBotton)
    LinearLayout lnOneBotton;
    @BindView(R.id.btnOneBotton)
    MaterialButton btnOneBotton;
    @BindView(R.id.btnBuyNow)
    MaterialButton btnBuyNow;
    @BindView(R.id.btnAddCart)
    MaterialButton btnAddCart;
    @BindView(R.id.edCount)
    EditText edCount;
    @BindView(R.id.imgMinus)
    ImageView imgMinus;
    @BindView(R.id.imgPlus)
    ImageView imgPlus;
    private ProductColorAdapter productColorAdapter;
    private ProductSizeAdapter productSizeAdapter;
    private int showBotton;
    private boolean checkAddCart;
    private OrdelPresenterImpl ordelPresenter;
    private ProductsInfo productsInfo;
    private List<PaymentOrderInfo> paymentOrderInfoList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_add_cart, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        showListener();
        getDataProduct();
        paymentOrderInfoList = new ArrayList<>();
        showBotton = getArguments().getInt(FConstants.KEY_SHOW_BOTTON);
        setShowBotton(showBotton);
        ordelPresenter = new OrdelPresenterImpl(this);
    }


    private void showListener() {
        imgClosed.setOnClickListener(this);
        tvColor.addTextChangedListener(textWatcher);
        tvSize.addTextChangedListener(textWatcher);
        btnOneBotton.setOnClickListener(this);
        btnAddCart.setOnClickListener(this);
        imgMinus.setOnClickListener(this);
        imgPlus.setOnClickListener(this);
        btnBuyNow.setOnClickListener(this);
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            String color = tvColor.getText().toString();
            String size = tvSize.getText().toString();
            if (color.isEmpty() || size.isEmpty()) {
                btnAddCart.setEnabled(false);
                btnBuyNow.setEnabled(false);
                btnOneBotton.setEnabled(false);
            } else {
                btnAddCart.setEnabled(true);
                btnBuyNow.setEnabled(true);
                btnOneBotton.setEnabled(true);
            }
        }
    };

    private void setShowBotton(int showBotton) {
        switch (showBotton) {
            case FConstants.SHOW_ALL_BOTTON:
                lnShowAll.setVisibility(View.VISIBLE);
                lnOneBotton.setVisibility(View.GONE);
                break;
            case FConstants.SHOW_BOTTON_BUY_NOW:
                lnShowAll.setVisibility(View.GONE);
                lnOneBotton.setVisibility(View.VISIBLE);
                btnOneBotton.setText(getResources().getString(R.string.buy_now));
                checkAddCart = false;
                break;
            case FConstants.SHOW_BOTTIN_ADD_CART:
                lnShowAll.setVisibility(View.GONE);
                lnOneBotton.setVisibility(View.VISIBLE);
                btnOneBotton.setText(getResources().getString(R.string.add_cart));
                checkAddCart = true;
                break;
        }
    }

    @Override
    public void success(Object x) {
        super.success(x);
        showProgress();
        if (x instanceof CartRes) {
            DialogUtil.showDialogMessage(requireContext(), getResources().getString(R.string.addcart_success), R.string.ok, new DialogMessage.OnDialogMessageListener() {
                @Override
                public void yesClick() {

                }
            });
        }
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj err = (BaseObj) o;
        DialogUtil.showDialogMessage(requireContext(), err.getMessage());
    }

    private void getDataProduct() {
        productsInfo = (ProductsInfo) getArguments().getSerializable(FConstants.KEY_PRODUCT_INFO);
        tvPric.setPaintFlags(tvPric.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        showListColor(productsInfo.getColors());
        showListSize(productsInfo.getSizes());
        if (productsInfo.getDiscountPercent().equals("0%")) {
            tvDiscountPercent.setVisibility(View.GONE);
            tvPric.setVisibility(View.GONE);
            tvDiscountPric.setText(Utils.formatAmount(Double.parseDouble(productsInfo.getPrice())));
        } else {
            tvDiscountPercent.setVisibility(View.VISIBLE);
            tvPric.setVisibility(View.VISIBLE);
            tvDiscountPric.setText(Utils.formatAmount(Double.parseDouble(productsInfo.getDiscountPrice())));
            tvPric.setText(Utils.formatAmount(Double.parseDouble(productsInfo.getPrice())));
            tvDiscountPercent.setText("- " + productsInfo.getDiscountPercent());
        }
        if (productsInfo.getImages().size() == 0) {
            imgAvatarProduct.setImageResource(R.drawable.logo);
        } else {
            Glide.with(requireContext())
                    .load(ConstantApi.MAIN_DNS + "/" + productsInfo.getImages().get(0))
                    .transform(new CenterCrop(), new RoundedCorners(8))
                    .error(R.drawable.logo)
                    .into(imgAvatarProduct);
        }

    }

    private void showListColor(List<String> colors) {
        productColorAdapter = new ProductColorAdapter(requireContext(), true);
        rvOptionsColor.setHasFixedSize(true);
        rvOptionsColor.setLayoutManager(new GridLayoutManager(requireContext(), 4));
        productColorAdapter.setOnClickListenerOptions(this);
        rvOptionsColor.setAdapter(productColorAdapter);
        productColorAdapter.swap(colors);
    }

    private void showListSize(List<String> size) {
        productSizeAdapter = new ProductSizeAdapter(requireContext(), true);
        rvOptionsSize.setHasFixedSize(true);
        rvOptionsSize.setLayoutManager(new GridLayoutManager(requireContext(), 4));
        productSizeAdapter.setOnClickListenerOptions(this);
        rvOptionsSize.setAdapter(productSizeAdapter);
        productSizeAdapter.swap(size);
    }

    @Override
    public void onClick(View view) {
        int amount = Integer.parseInt(edCount.getText().toString());
        switch (view.getId()) {
            case R.id.imgClosed:
                dismiss();
                break;
            case R.id.btnOneBotton:
                if (checkAddCart) {
                    addDataCart(amount);
                } else {
                    replaceFragment(new FragmentPayment().newInstance(buyNow(amount)), true, false);
                    dismiss();
                }
                break;
            case R.id.btnAddCart:
                addDataCart(amount);
                break;
            case R.id.imgPlus:
                amount += 1;
                edCount.setText(String.valueOf(amount));
                break;
            case R.id.imgMinus:
                if (amount == 1) {
                    amount = 1;
                } else {
                    amount -= 1;
                }
                edCount.setText(String.valueOf(amount));
                break;
            case R.id.btnBuyNow:
                replaceFragment(new FragmentPayment().newInstance(buyNow(amount)), true, false);
                dismiss();
                break;
        }
    }

    private void addDataCart(int amount) {
        ordelPresenter.addCart(new CartRep(loginRes.getData().getId(), productsInfo.getId(),
                amount,
                tvColor.getText().toString(), tvSize.getText().toString()));
    }

    private List<PaymentOrderInfo> buyNow(int amount) {
        paymentOrderInfoList.add(new PaymentOrderInfo(amount, tvSize.getText().toString(),
                tvColor.getText().toString(), productsInfo.getId(), productsInfo.getName()
                , productsInfo.getPrice(), productsInfo.getDiscountPrice(), productsInfo.getImages().get(0),
                productsInfo.getDiscountPercent()));
        return paymentOrderInfoList;
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void setOnClickOptionsColor(View view, int position, String name) {
        tvColor.setText(name + ", ");
    }

    @Override
    public void setOnClickOptionsSize(View view, int position, String name) {
        tvSize.setText(name);
    }

}
