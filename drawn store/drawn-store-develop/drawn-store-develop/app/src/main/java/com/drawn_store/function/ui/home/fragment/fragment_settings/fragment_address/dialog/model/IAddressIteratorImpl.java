package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.dialog.model;

import com.drawn_store.common.api.callback.MyCallback;
import com.drawn_store.common.base.BaseInteractorImpl;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.InteractorCallback;
import com.drawn_store.function.common.api.ApiUtils;
import com.drawn_store.function.common.api.request.AddressRequest;
import com.drawn_store.function.common.objects.request.AddressRep;
import com.drawn_store.function.common.objects.request.UpdateAddressRep;
import com.drawn_store.function.common.objects.response.AddAddressRes;
import com.drawn_store.function.common.objects.response.DeleteAddressRes;
import com.drawn_store.function.common.objects.response.DistrictRes;
import com.drawn_store.function.common.objects.response.ListAddressRes;
import com.drawn_store.function.common.objects.response.ProvinceRes;
import com.drawn_store.function.common.objects.response.UpdateAddressRes;
import com.drawn_store.function.common.objects.response.WardsRes;

import retrofit2.Call;
import retrofit2.Response;

public class IAddressIteratorImpl extends BaseInteractorImpl<BaseObj, Object> implements IAddressIterator {

    private AddressRequest addressRequest;

    public IAddressIteratorImpl(InteractorCallback callback) {
        super(callback);
        addressRequest = ApiUtils.getAddressRequest();
    }

    @Override
    public void provinces() {
        addressRequest.getProvince().enqueue(new MyCallback<ProvinceRes>() {
            @Override
            public void onSuccess(Call<ProvinceRes> call, Response<ProvinceRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<ProvinceRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void district(String code) {
        addressRequest.getDistrict(code).enqueue(new MyCallback<DistrictRes>() {
            @Override
            public void onSuccess(Call<DistrictRes> call, Response<DistrictRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<DistrictRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void wards(String code) {
        addressRequest.getWards(code).enqueue(new MyCallback<WardsRes>() {
            @Override
            public void onSuccess(Call<WardsRes> call, Response<WardsRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<WardsRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void listAddres(int userId) {
        addressRequest.getListAddress(userId).enqueue(new MyCallback<ListAddressRes>() {
            @Override
            public void onSuccess(Call<ListAddressRes> call, Response<ListAddressRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<ListAddressRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void addAddress(AddressRep addressRep) {
        addressRequest.addAddress(addressRep).enqueue(new MyCallback<AddAddressRes>() {
            @Override
            public void onSuccess(Call<AddAddressRes> call, Response<AddAddressRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<AddAddressRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void deleteAddress(int infoId) {
        addressRequest.deleteAddress(infoId).enqueue(new MyCallback<DeleteAddressRes>() {
            @Override
            public void onSuccess(Call<DeleteAddressRes> call, Response<DeleteAddressRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<DeleteAddressRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void updateAddress(UpdateAddressRep updateAddressRep) {
        addressRequest.updateAddress(updateAddressRep).enqueue(new MyCallback<UpdateAddressRes>() {
            @Override
            public void onSuccess(Call<UpdateAddressRes> call, Response<UpdateAddressRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<UpdateAddressRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }
}
