package com.drawn_store.common.view;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.drawn_store.R;

public class MyToast extends Toast {
    private Context mContext;


    public MyToast(Context context) {
        super(context);
        this.mContext = context;
    }

    public MyToast(Context context, String message) {
        super(context);
        this.mContext = context;
        initView(-1, message,0, false);
    }


    public MyToast(Context context, int drawableResource, String message, int gravity) {
        super(context);
        this.mContext = context;
        initView(drawableResource, message,gravity, false);
    }

    public MyToast(Context context, int drawableResource, String message, int gravity, boolean isCorrectMess) {
        super(context);
        this.mContext = context;
        initView(drawableResource, message,gravity, isCorrectMess);
    }

    public MyToast(Context context, int drawableResource, int message) {
        super(context);
        this.mContext = context;
        String s = context.getString(message);
        initView(drawableResource, s,0, false);
    }

    /**
     * @param drawableResource
     * @param message
     */
    public void initView(int drawableResource, String message, int gravity, boolean isCorrectMess) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.toast, null);
        if(isCorrectMess){
            view.setBackground(mContext.getResources().getDrawable(R.drawable.bg_toast_correct));
        }else {
            view.setBackground(mContext.getResources().getDrawable(R.drawable.bg_toast));
        }
        TextView tvMessage = view.findViewById(R.id.tvToast);
        tvMessage.setText(message);
        ImageView ivToast = view.findViewById(R.id.ivToast);
        if (drawableResource != -1) {
            ivToast.setImageDrawable(mContext.getResources().getDrawable(drawableResource));
        } else {
            if(isCorrectMess) {
                ivToast.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_check_toast));
            }else {
                ivToast.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_sad_face));
            }

        }
        setDuration(Toast.LENGTH_SHORT);
        if (gravity == Gravity.BOTTOM){
            setGravity(Gravity.BOTTOM, 0, 20);
        }else {
            setGravity(Gravity.CENTER, 0, 0);
        }
        setView(view);
    }
}