package com.drawn_store.function.ui.home.fragment.fragment_payment.model;

import com.drawn_store.common.api.callback.MyCallback;
import com.drawn_store.common.base.BaseInteractorImpl;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.InteractorCallback;
import com.drawn_store.function.common.api.ApiUtils;
import com.drawn_store.function.common.api.request.OrderRequest;
import com.drawn_store.function.common.api.request.ProductsRequest;
import com.drawn_store.function.common.objects.request.LikeRep;
import com.drawn_store.function.common.objects.request.OrderReq;
import com.drawn_store.function.common.objects.response.AddLikeRes;
import com.drawn_store.function.common.objects.response.DislikeLikeRes;
import com.drawn_store.function.common.objects.response.LikeRes;
import com.drawn_store.function.common.objects.response.OrderRes;
import com.drawn_store.function.common.objects.response.ProductsAllRes;
import com.drawn_store.function.common.objects.response.ProductsHighestViewRes;
import com.drawn_store.function.common.objects.response.ProductsNewRes;
import com.drawn_store.function.common.objects.response.ProductsSaleRes;
import com.drawn_store.function.common.objects.response.ProductsTypeLineId;
import com.drawn_store.function.common.objects.response.ProductsTypeRes;
import com.drawn_store.function.common.objects.response.ProductsViewRes;
import com.drawn_store.function.common.objects.response.ProvinceRes;
import com.drawn_store.function.common.objects.response.RatingRes;

import retrofit2.Call;
import retrofit2.Response;

public class OrderIteratorImpl extends BaseInteractorImpl<BaseObj, Object> implements OrderIterator {

    private OrderRequest orderRequest;

    public OrderIteratorImpl(InteractorCallback callback) {
        super(callback);
        orderRequest = ApiUtils.getOrderRequest();
    }


    @Override
    public void order(OrderReq orderReq) {
        orderRequest.order(orderReq).enqueue(new MyCallback<OrderRes>() {
            @Override
            public void onSuccess(Call<OrderRes> call, Response<OrderRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<OrderRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void showOrder(int userId, String filter) {
        orderRequest.showOrder(userId, filter).enqueue(new MyCallback<OrderRes>() {
            @Override
            public void onSuccess(Call<OrderRes> call, Response<OrderRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<OrderRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

}
