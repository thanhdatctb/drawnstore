package com.drawn_store.common.ultility;

import com.drawn_store.Apps;
import com.drawn_store.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author: Created by Trần Lâm on 2/17/2020
 * @email: lamth1401@gmail.com.
 */
public class DateTimeUtil {
    public static final String DATE_FORMAT_JAPAN = "dd-MM-yyyy";
    public static final String DATE_FORMAT_JAPAN_1 = "yyyy年MM月";
    public static final String DATE_FORMAT_ENGLISH_1 = "yyyy-MM-dd";
    public static final String DATE_FORMAT_ENGLISH_2 = "yyyy/MM/dd HH:mm";
    public static final String DATE_FORMAT_ENGLISH_3 = "yyyy-MM";
    public static final String DATE_FORMAT_ENGLISH_4 = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_ENGLISH_5 = "yyyy/MM/dd";
    public static final String DATE_FORMAT_ENGLISH_6 = "MMM d, yyyy";
    public static final String DATE_FORMAT_ENGLISH_7 = "dd-MM-yyyy HH:mm";
    public static final String DATE_FORMAT_ENGLISH_8 = "dd-MM-yyyy";
    public static final String DATE_FORMAT_ENGLISH_9 = "dd/MM/yyyy";

    public static final String DATETIME_FORMAT_ENGLISH = "yyyy-MM-dd HH:mm";
    public static final String TIME_FORMAT_1 = "HH:mm";
    public static final String TIME_FORMAT_2 = "hh:mm a";


    public static String getDateFromCalendar(Calendar calendar) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_JAPAN, Locale.JAPAN);
        return dateFormat.format(calendar.getTime());
    }

    public static String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_ENGLISH_7, Locale.US);
        return sdf.format(calendar.getTime());
    }

    public static String getCurrentDateTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_ENGLISH_4, Locale.US);
        return sdf.format(calendar.getTime());
    }

    public static String convertDateToDate(final String date, final String fromPattern,
                                           final String toPattern) {
        String result = date;
        final SimpleDateFormat dfFrom = new SimpleDateFormat(fromPattern, Locale.US);
        final SimpleDateFormat dfTo = new SimpleDateFormat(toPattern, Locale.US);
        try {
            result = dfTo.format(dfFrom.parse(date));
        } catch (ParseException e) {
            Logger.e("DateTimeUtil, convertDateToDate: ", e.getMessage());
        }
        return result;
    }

    public static Calendar toCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }


    public static Date convertDateToDate(final String date, final String fromPattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(fromPattern, Locale.US);
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String convertCalendarToYYYYMMDD(Calendar calendar) {
        String result;
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_ENGLISH_1, Locale.US);
        result = format.format(calendar.getTime());
        return result;
    }

    public static String convertCalendarToDDMMYYYY(Calendar calendar) {
        String result;
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_ENGLISH_8, Locale.US);
        result = format.format(calendar.getTime());
        return result;
    }

    public static String convertCalendarToHHmm(Calendar calendar) {
        String result;
        SimpleDateFormat format = new SimpleDateFormat(TIME_FORMAT_1, Locale.US);
        result = format.format(calendar.getTime());
        return result;
    }

    public static String convertCalendarToYYYYMMDD_HHMM(Calendar calendar) {
        String result;
        SimpleDateFormat format = new SimpleDateFormat(DATETIME_FORMAT_ENGLISH, Locale.US);
        result = format.format(calendar.getTime());
        return result;
    }

    public static Calendar getCalendar(final String date, final String pattern) {
        Calendar calendar = Calendar.getInstance(Locale.US);
        final SimpleDateFormat df = new SimpleDateFormat(pattern, Locale.US);
        Date dt;
        try {
            dt = df.parse(date);
        } catch (ParseException e) {
            dt = new Date();
            Logger.e("DateTimeUtil, getCalendar: ", e.getMessage());
        }
        calendar.setTime(dt);
        return calendar;
    }

    public static int checkDates(String endDate, String currentDate) {
        SimpleDateFormat dfDate = new SimpleDateFormat(DateTimeUtil.DATE_FORMAT_ENGLISH_1);
        int b = 0;
        try {
            if (dfDate.parse(endDate).before(dfDate.parse(currentDate))) {
                b = 1;//If start date is before end date
            } else if (dfDate.parse(endDate).equals(dfDate.parse(currentDate))) {
                b = 0;//If two dates are equal
            } else {
                b = 0; //If start date is after the end date
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return b;
    }


    public static int checkTimes(String pickDate, String currentDate) {
        SimpleDateFormat dfDate = new SimpleDateFormat(DateTimeUtil.DATE_FORMAT_ENGLISH_1);
        int b = 0;
        try {
            if (dfDate.parse(pickDate).before(dfDate.parse(currentDate))) {
                b = 1;//If start date is before end date
            } else if (dfDate.parse(pickDate).equals(dfDate.parse(currentDate))) {
                b = 0;//If two dates are equal
            } else {
                b = 0; //If start date is after the end date
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return b;
    }

    public static Calendar setTimeToMidnight(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    public static boolean compareWithToday(String time1) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_ENGLISH_7, Locale.US);
        try {
            Date compareDate = sdf.parse(time1);
            Date currentDate = sdf.parse(sdf.format(new Date()));
            int result = currentDate.compareTo(compareDate);
            return result <= 0;
        } catch (Exception e) {
            //e.printStackTrace();
            return false;
        }
    }


    public static boolean compareWithCurrentTime(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_ENGLISH_7, Locale.US);
        try {
            Date compareTime = sdf.parse(time);
            Date currentTime = sdf.parse(sdf.format(new Date()));
            if (compareTime.before(currentTime)) {
                return true; //If time is before current time
            } else {
                return false; //If time is after or equal the end time
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public static int compareTwoTime(String startTime, String endTime) {
        SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT_1, Locale.US);
        int b = 0;
        try {
            if (sdf.parse(startTime).before(sdf.parse(endTime))) {
                b = 1; //If start time is before end time
            } else if (sdf.parse(startTime).equals(sdf.parse(endTime))) {
                b = 0; //If two times are equal
            } else {
                b = 0; //If start time is after the end time
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }

    public static String showEstimateTime(long timePublished) {
        if (timePublished == 0) {
            return Apps.getInstance().getString(R.string.just_now);
        } else {
            long diffSec = System.currentTimeMillis() / 1000 - timePublished;
            if (diffSec < 60) {
                return Apps.getInstance().getString(R.string.less_than_a_minute);
            } else {
                long diffMin = diffSec / 60;
                if (diffMin == 1) {
                    return Apps.getInstance().getString(R.string.sminute);
                } else if (diffMin < 60) {
                    return Apps.getInstance().getString(R.string.sminutes, diffMin);
                } else {
                    long diffHour = diffMin / 60;
                    if (diffHour == 1) {
                        return Apps.getInstance().getString(R.string.shour);
                    } else if (diffHour < 24) {
                        return Apps.getInstance().getString(R.string.shours, diffHour);
                    } else {
                        long diffDay = diffHour / 24;
                        if (diffDay == 1) {
                            return Apps.getInstance().getString(R.string.sday);
                        } else {
                            return Apps.getInstance().getString(R.string.sdays, diffDay);
                        }
                    }
                }
            }
        }
    }
}
