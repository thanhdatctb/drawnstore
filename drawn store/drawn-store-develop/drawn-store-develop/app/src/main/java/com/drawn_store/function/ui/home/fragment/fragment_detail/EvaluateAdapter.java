package com.drawn_store.function.ui.home.fragment.fragment_detail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.drawn_store.R;
import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.objects.RatingInfo;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EvaluateAdapter extends RecyclerView.Adapter<EvaluateAdapter.ViewHodel> {

    private Context context;
    private List<RatingInfo> ratingInfos;

    public EvaluateAdapter(Context context) {
        this.context = context;
    }
    public void swap(List<RatingInfo> ratingInfos){
        this.ratingInfos = ratingInfos;
    }

    @NonNull
    @Override
    public ViewHodel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_evaluete, parent, false);
        return new ViewHodel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHodel holder, int position) {
        RatingInfo ratingInfo = ratingInfos.get(position);
        holder.contentEVL.setText(ratingInfo.getComment());
        holder.nameEVL.setText(ratingInfo.getName());
        holder.ratingProduct.setRating(ratingInfo.getStars());
        if (ratingInfo.getAvatarUrl() == null) {
            holder.imgEVL.setImageResource(R.drawable.avata_de);
        } else {
            if (ratingInfo.getAvatarUrl().contains("http")) {
                Glide.with(context)
                        .load(ratingInfo.getAvatarUrl())
                        .apply(RequestOptions.circleCropTransform())
                        .into(holder.imgEVL);
            } else {
                Glide.with(context)
                        .load(ConstantApi.MAIN_DNS + "/" + ratingInfo.getAvatarUrl())
                        .apply(RequestOptions.circleCropTransform())
                        .into(holder.imgEVL);
            }
        }
    }

    @Override
    public int getItemCount() {
        return ratingInfos.size();
    }

    public class ViewHodel extends RecyclerView.ViewHolder {
        @BindView(R.id.contentEVL)
        TextView contentEVL;
        @BindView(R.id.nameEVL)
        TextView nameEVL;
        @BindView(R.id.imgEVL)
        ImageView imgEVL;
        @BindView(R.id.ratingProduct)
        SimpleRatingBar ratingProduct;

        public ViewHodel(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
