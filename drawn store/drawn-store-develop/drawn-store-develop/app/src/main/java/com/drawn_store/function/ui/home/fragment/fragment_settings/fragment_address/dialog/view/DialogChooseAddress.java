package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.dialog.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.drawn_store.R;
import com.drawn_store.common.api.ErrorCode;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.pref.PrefManager;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.common.view.MyProgressDialog;
import com.drawn_store.function.common.objects.DistrictInfo;
import com.drawn_store.function.common.objects.ProvinceInfo;
import com.drawn_store.function.common.objects.WardsInfo;
import com.drawn_store.function.common.objects.response.DistrictRes;
import com.drawn_store.function.common.objects.response.ProvinceRes;
import com.drawn_store.function.common.objects.response.WardsRes;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.dialog.presenter.IAddressPresenterImpl;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.dialog.view.adapter.DistrictAdapter;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.dialog.view.adapter.ProvinceAdapter;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.dialog.view.adapter.WardsAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogChooseAddress extends Dialog implements BaseView, ProvinceAdapter.ClickItemName,
        DistrictAdapter.ClickItemNameDistrict, WardsAdapter.ClickItemNameWards {

    public DialogChooseAddress newInstance() {
        return this;
    }

    public DialogChooseAddress(@NonNull Context context) {
        super(context);
    }

    @Nullable
    @BindView(R.id.toolbar_title)
    TextView mTvTitle;
    @Nullable
    @BindView(R.id.btn_back)
    ImageView imgBack;
    @Nullable
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @Nullable
    @BindView(R.id.rvAddress)
    RecyclerView rvAddress;
    String title, code, codeDistrict;
    int typeAddress;
    private String nameProvince, nameDistrict, nameWards;
    private ProvinceAdapter provinceAdapter;
    private DistrictAdapter districtAdapter;
    private WardsAdapter wardsAdapter;
    private List<ProvinceInfo> provinceInfos;
    private List<DistrictInfo> districtInfos;
    private List<WardsInfo> wardsInfos;
    private IAddressPresenterImpl iAddressPresenterImpl;
    public MyProgressDialog progressDialog;
    private PrefManager prefManager;
    private SetNameEditext setNameEditext;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setCodeDistrict(String codeDistrict) {
        this.codeDistrict = codeDistrict;
    }

    public void setTypeAddress(int typeAddress) {
        this.typeAddress = typeAddress;
    }

    public void setNameProvince(String nameProvince) {
        this.nameProvince = nameProvince;
    }

    public void setNameDistrict(String nameDistrict) {
        this.nameDistrict = nameDistrict;
    }

    public void setNameWards(String nameWards) {
        this.nameWards = nameWards;
    }

    public void setNameEditext(SetNameEditext setNameEditext) {
        this.setNameEditext = setNameEditext;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable());
        setContentView(R.layout.dialog_choose_address);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        ButterKnife.bind(this);
        iAddressPresenterImpl = new IAddressPresenterImpl(this);
        prefManager = new PrefManager(getContext());
        if (imgBack != null) {
            final View parent = (View) imgBack.getParent();  // button: the view you want to enlarge hit area
            parent.post(new Runnable() {
                public void run() {
                    final Rect rect = new Rect();
                    imgBack.getHitRect(rect);
                    rect.top -= 30;    // increase top hit area
                    rect.left -= 30;   // increase left hit area
                    rect.bottom += 30; // increase bottom hit area
                    rect.right += 30;  // increase right hit area
                    parent.setTouchDelegate(new TouchDelegate(rect, imgBack));
                }
            });
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });

        }
        provinceInfos = new ArrayList<>();
        districtInfos = new ArrayList<>();
        wardsInfos = new ArrayList<>();
        getDataPut();
        showProgress();
    }


    private void getDataPut() {
        mTvTitle.setText(title);
        mToolbar.setBackground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.colorWhite)));
        if (mTvTitle != null) {
            mTvTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.colorBlack));
            imgBack.setImageResource(R.drawable.ic_back_black);
        }
        showType();
    }

    private void showType() {
        switch (typeAddress) {
            case 0:
                iAddressPresenterImpl.provinces();
                break;
            case 1:
                iAddressPresenterImpl.district(code);
                break;
            case 2:
                iAddressPresenterImpl.wards(codeDistrict);
                break;
        }
    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new MyProgressDialog(getContext());
        }
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void success(Object x) {
        hideProgress();
        if (x instanceof ProvinceRes) {
            ProvinceRes provinceRes = (ProvinceRes) x;
            provinceInfos = provinceRes.getData();
            provinceAdapter = new ProvinceAdapter(getContext(), nameProvince);
            rvAddress.setHasFixedSize(true);
            rvAddress.setLayoutManager(new LinearLayoutManager(getContext()));
            provinceAdapter.setOnClickName(this);
            rvAddress.setAdapter(provinceAdapter);
            provinceAdapter.swap(provinceInfos);
        } else if (x instanceof DistrictRes) {
            DistrictRes districtRes = (DistrictRes) x;
            districtInfos = districtRes.getData();
            districtAdapter = new DistrictAdapter(getContext(), nameDistrict);
            rvAddress.setHasFixedSize(true);
            rvAddress.setLayoutManager(new LinearLayoutManager(getContext()));
            districtAdapter.setOnClickName(this);
            rvAddress.setAdapter(districtAdapter);
            districtAdapter.swap(districtInfos);
        } else if (x instanceof WardsRes) {
            WardsRes wardsRes = (WardsRes) x;
            wardsInfos = wardsRes.getData();
            wardsAdapter = new WardsAdapter(getContext(), nameWards);
            rvAddress.setHasFixedSize(true);
            rvAddress.setLayoutManager(new LinearLayoutManager(getContext()));
            wardsAdapter.setOnClickName(this);
            rvAddress.setAdapter(wardsAdapter);
            wardsAdapter.swap(wardsInfos);
        }
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        hideProgress();
        BaseObj err = (BaseObj) o;
        if (err.getStatusCode() == ErrorCode.BAD_REQUEST) {
            DialogUtil.showDialogMessage(getContext(), getContext().getResources().getString(R.string.data_null));
        } else {
            DialogUtil.showDialogMessage(getContext(), err.getMessage() + " (" + err.getStatusCode() + ")");
        }
    }

    @Override
    public void setName(View view, int position, String name) {
        dismiss();
        setNameEditext.setNameProvince(name);
    }

    @Override
    public void setNameDistrict(View view, int position, String name) {
        dismiss();
        setNameEditext.setNameDistrict(name);
    }

    @Override
    public void setNameWards(View view, int position, String name) {
        dismiss();
        setNameEditext.setNameWards(name);
    }

    public interface SetNameEditext {
        void setNameProvince(String name);

        void setNameDistrict(String name);

        void setNameWards(String name);
    }
}
