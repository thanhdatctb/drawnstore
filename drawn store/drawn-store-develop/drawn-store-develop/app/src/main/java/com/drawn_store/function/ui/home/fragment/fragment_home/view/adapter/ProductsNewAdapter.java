package com.drawn_store.function.ui.home.fragment.fragment_home.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.drawn_store.R;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.objects.ProductsInfo;
import com.drawn_store.function.common.view.OnRecyclerViewItemClickListener;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductsNewAdapter extends RecyclerView.Adapter<ProductsNewAdapter.ViewHolder> {
    private Context context;
    private List<ProductsInfo> productsInfoList;
    private List<ProductsInfo> productsInfoListFi;
    private boolean checkNew = true;
    private OnRecyclerViewItemClickListener itemClickListener;

    public ProductsNewAdapter(Context context, boolean checkNew) {
        this.context = context;
        this.checkNew = checkNew;
    }

    public void swap(List<ProductsInfo> productsInfoList) {
        this.productsInfoList = productsInfoList;
        this.productsInfoListFi = productsInfoList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_products_new, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProductsInfo productsInfo = productsInfoList.get(position);
        holder.tvNameProducts.setText(productsInfo.getName());
        holder.ratingProduct.setRating((float) 3.5);
        if (productsInfo.getDiscountPercent().equals("0%")) {
            holder.tvDiscountPercent.setVisibility(View.GONE);
            holder.tvPric.setVisibility(View.GONE);
            holder.tvDiscountPric.setText(Utils.formatAmount(Double.parseDouble(productsInfo.getPrice())));
        } else {
            holder.tvDiscountPercent.setVisibility(View.VISIBLE);
            holder.tvPric.setVisibility(View.VISIBLE);
            holder.tvDiscountPric.setText(Utils.formatAmount(Double.parseDouble(productsInfo.getDiscountPrice())));
            holder.tvPric.setText(Utils.formatAmount(Double.parseDouble(productsInfo.getPrice())));
            holder.tvDiscountPercent.setText("- " + productsInfo.getDiscountPercent());
        }
        if (checkNew) {
            holder.imgNew.setVisibility(View.VISIBLE);
        } else {
            holder.imgNew.setVisibility(View.GONE);
        }
        if (productsInfo.getImages().size() == 0) {
            holder.imgProducts.setImageResource(R.drawable.logo);
        } else {
            Glide.with(context)
                    .load(ConstantApi.MAIN_DNS + "/" + productsInfo.getImages().get(0))
                    .transform(new CenterCrop(), new RoundedCorners(8))
                    .error(R.drawable.logo)
                    .into(holder.imgProducts);
        }
        if (productsInfo.getIsLiked()) {
            holder.imgLike.setImageResource(R.drawable.ic_like);
//            checkLike = false;
        } else {
            holder.imgLike.setImageResource(R.drawable.ic_no_like);
//            checkLike = true;
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.recyclerViewListClickedData(view, position, productsInfo);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productsInfoList.size();
    }

    public void setOnClickListener(OnRecyclerViewItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvNameProducts)
        TextView tvNameProducts;
        @BindView(R.id.tvDiscountPric)
        TextView tvDiscountPric;
        @BindView(R.id.tvPric)
        TextView tvPric;
        @BindView(R.id.tvDiscountPercent)
        TextView tvDiscountPercent;
        @BindView(R.id.imgProducts)
        ImageView imgProducts;
        @BindView(R.id.imgNew)
        ImageView imgNew;
        @BindView(R.id.ratingProduct)
        SimpleRatingBar ratingProduct;
        @BindView(R.id.imgLike)
        ImageView imgLike;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvPric.setPaintFlags(tvPric.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    productsInfoList = productsInfoListFi;
                } else {
                    List<ProductsInfo> filteredList = new ArrayList<>();
                    for (ProductsInfo row : productsInfoListFi) {
                        if (row.getName().toLowerCase(Locale.getDefault()).contains(charString.toLowerCase(Locale.getDefault()))
                                || row.getDescription().contains(charSequence))
                            filteredList.add(row);
                    }
                    productsInfoList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = productsInfoList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                productsInfoList = (List<ProductsInfo>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
