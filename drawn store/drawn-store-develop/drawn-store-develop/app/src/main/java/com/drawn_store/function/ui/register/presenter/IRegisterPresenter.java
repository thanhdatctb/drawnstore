package com.drawn_store.function.ui.register.presenter;

import com.drawn_store.function.common.objects.request.RegisterReq;

public interface IRegisterPresenter {
    void register(RegisterReq registerReq);
}
