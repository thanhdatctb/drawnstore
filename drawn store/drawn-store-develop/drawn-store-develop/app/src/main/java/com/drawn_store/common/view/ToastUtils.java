package com.drawn_store.common.view;

import android.content.Context;


public class ToastUtils {
    public static void showToast(Context context, int resourceId, String message, int gravity) {
        MyToast toast = new MyToast(context, resourceId, message, gravity);
        toast.show();
    }

    public static void showToast(Context context, String message, int gravity, boolean isCorrectMess) {
        MyToast toast = new MyToast(context, -1, message, gravity, isCorrectMess);
        toast.show();
    }

    public static void showToast(Context context, String message, int gravity) {
        MyToast toast = new MyToast(context, -1, message, gravity);
        toast.show();
    }

    public static void showToast(Context context, int message) {
        MyToast toast = new MyToast(context, -1, message);
        toast.show();
    }
}
