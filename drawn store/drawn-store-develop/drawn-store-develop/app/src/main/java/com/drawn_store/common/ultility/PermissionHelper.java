package com.drawn_store.common.ultility;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class PermissionHelper {
    public static String[] mPermissions = new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.CAMERA};

    public static boolean isPermissionsGrantedCamera(Activity activity, int permissionRequestCode) {
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.CAMERA)) {

            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.CAMERA},
                        permissionRequestCode);
            }
            return false;
        } else {
            return true;
        }
    }

    /**
     * For permission request at MainActivity
     */
    public static boolean isPermissionsGranted(Activity activity, int permissionRequestCode) {
        boolean isGranted = true;
        for (String permission : mPermissions) {
            if (ContextCompat.checkSelfPermission(activity, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                isGranted = false;
            }
        }
        if (!isGranted) {
            ActivityCompat.requestPermissions(activity,
                    mPermissions,
                    permissionRequestCode
            );
            return isGranted;
        } else {
            return isGranted;
        }
    }
}
