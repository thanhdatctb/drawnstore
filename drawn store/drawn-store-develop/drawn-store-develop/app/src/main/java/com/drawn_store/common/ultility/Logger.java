package com.drawn_store.common.ultility;

import android.util.Log;

import com.drawn_store.BuildConfig;

public final class Logger {

    private static final boolean DEBUG = BuildConfig.DEBUG;

    private static final String LOG_PREFIX = "ABA-ERP";
    private static final int LOG_PREFIX_LENGTH = LOG_PREFIX.length();
    private static final int MAX_LOG_TAG_LENGTH = 100;
    /**
     * Use Log.i(tag, msg).
     *
     * @param tag for log showed.
     * @param msg information message.
     */
    public static void i(final String tag, final String msg) {
        if (DEBUG) {
            Log.i(tag, msg);
        }
    }

    /**
     * Use Log.e(tag, msg).
     *
     * @param tag for log showed.
     * @param msg error message.
     */
    public static void e(final String tag, final String msg) {
        if (DEBUG) {
            Log.e(tag, msg);
        }
    }

    /**
     * Use Log.e(tag, msg, throwable).
     *
     * @param tag for log showed.
     * @param msg error message.
     * @param tr  throws showed.
     */
    public static void e(final String tag, final String msg, final Throwable tr) {
        if (DEBUG) {
            Log.e(tag, msg, tr);
        }
    }

    /**
     * Use Log.v(tag, msg).
     *
     * @param tag for log showed.
     * @param msg warning message.
     */
    public static void v(final String tag, final String msg) {
        if (DEBUG) {
            Log.v(tag, msg);
        }
    }

    /**
     * Use Log.d(tag, msg).
     *
     * @param tag for log showed.
     * @param msg debug message.
     */
    public static void d(final String tag, final String msg) {
        if (DEBUG) {
            Log.d(tag, msg);
        }
    }

    /**
     * Use System.out.println(tag, msg).
     *
     * @param tag for log showed.
     * @param msg message.
     */
    public static void out(final String tag, final String msg) {
        if (DEBUG) {
            System.out.println(tag + " ===> " + msg);
        }
    }

    /**
     * Use e.printStackTrace();
     * @param e
     */
    public static void printStackTrace(Exception e){
        if (DEBUG){
            e.printStackTrace();
        }
    }

    public static String makeLogTag(String str) {
        if (str.length() > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            return LOG_PREFIX + str.substring(0, MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH - 1);
        }
        return LOG_PREFIX + str;
    }

    /**
     * Don't use this when obfuscating class names!
     */
    public static String makeLogTag(Class cls) {
        return makeLogTag(cls.getSimpleName());
    }


}