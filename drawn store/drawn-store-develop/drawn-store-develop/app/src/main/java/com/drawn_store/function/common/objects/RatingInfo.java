package com.drawn_store.function.common.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RatingInfo {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("avatarUrl")
    @Expose
    private String avatarUrl;
    @SerializedName("stars")
    @Expose
    private Float stars;
    @SerializedName("comment")
    @Expose
    private String comment;

    public RatingInfo() {
    }

    public RatingInfo(String name, String avatarUrl, Float stars, String comment) {
        this.name = name;
        this.avatarUrl = avatarUrl;
        this.stars = stars;
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public Float getStars() {
        return stars;
    }

    public void setStars(Float stars) {
        this.stars = stars;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
