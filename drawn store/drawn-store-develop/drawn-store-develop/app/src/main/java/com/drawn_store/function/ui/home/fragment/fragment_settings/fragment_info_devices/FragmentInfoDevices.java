package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_info_devices;

import android.annotation.SuppressLint;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.drawn_store.R;
import com.drawn_store.common.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentInfoDevices extends BaseFragment {
    public FragmentInfoDevices newInstance() {
        return this;
    }

    @BindView(R.id.tvVersion)
    TextView tvVersion;
    @BindView(R.id.tvVersionDevices)
    TextView tvVersionDevices;
    @BindView(R.id.tvFormDevices)
    TextView tvFormDevices;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_info_devices, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        isHideButtonBack(false);
        backgroudToolbar(true);
        setTitle(R.string.info_devices);
        setInfo();
    }

    @SuppressLint("SetTextI18n")
    private void setInfo() {
        try {
            PackageInfo pInfo = requireActivity().getPackageManager().getPackageInfo(requireActivity().getPackageName(), 0);
            String version = pInfo.versionName;
            String versionCode = String.valueOf(pInfo.versionCode);
            tvVersion.setText("v" + version + versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        tvVersionDevices.setText("Android " + Build.VERSION.RELEASE);
        tvFormDevices.setText(Build.MODEL);
    }
}
