package com.drawn_store.common.base;

import com.drawn_store.common.presenter.BaseInteractor;
import com.drawn_store.common.presenter.InteractorCallback;

public abstract class BaseInteractorImpl<T, E> implements BaseInteractor {
    protected InteractorCallback<T, E> mCallback;

    public BaseInteractorImpl(InteractorCallback callback) {
        this.mCallback = callback;
    }
}
