package com.drawn_store.function.ui.home.fragment.fragment_cart.presenter;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.presenter.InteractorCallback;
import com.drawn_store.function.common.objects.request.CartRep;
import com.drawn_store.function.common.objects.request.UpdateCartRep;
import com.drawn_store.function.common.objects.response.CartRes;
import com.drawn_store.function.ui.home.fragment.fragment_cart.model.OrdelIteratorImpl;
import com.drawn_store.function.ui.home.fragment.fragment_home.model.ProductIteratorImpl;

public class OrdelPresenterImpl implements OrdelPresenter {
    private OrdelIteratorImpl ordelIterator;

    public OrdelPresenterImpl(BaseView<BaseObj, Object> mBaseView) {
        InteractorCallback<BaseObj, Object> mCallback = new InteractorCallback<BaseObj, Object>() {
            @Override
            public void success(BaseObj x) {
                mBaseView.success(x);
            }

            @Override
            public void error(Object o, boolean isShowDialog) {
                mBaseView.error(o, isShowDialog);
            }
        };
        ordelIterator = new OrdelIteratorImpl(mCallback);
    }

    @Override
    public void addCart(CartRep cartRep) {
        ordelIterator.addCart(cartRep);
    }

    @Override
    public void getCart(int customerId) {
        ordelIterator.getCart(customerId);
    }

    @Override
    public void deleteCart(int cartId) {
        ordelIterator.deleteCart(cartId);
    }

    @Override
    public void updateCart(UpdateCartRep updateCartRep) {
        ordelIterator.updateCart(updateCartRep);
    }
}
