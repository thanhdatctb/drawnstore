package com.drawn_store.common.base;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.TouchDelegate;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.anthonycr.grant.PermissionsManager;
import com.drawn_store.R;
import com.drawn_store.common.constants.Constants;
import com.drawn_store.common.constants.ScreenConfig;
import com.drawn_store.common.pref.PrefManager;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.ultility.AnimationUtils;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.common.view.MyProgressDialog;
import com.drawn_store.function.common.navigator.Navigator;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity implements BaseView, Constants {

    @Nullable
    @BindView(R.id.toolbar_title)
    TextView mTvTitle;
    @Nullable
    @BindView(R.id.btn_back)
    ImageView mIbBack;
    /**
     * progress bar show when request api
     */
    protected MyProgressDialog progressDialog;
    private OnBackToolbarClickListener mOnBackToolbarClickListener;
    protected Toolbar mToolbar;
    public PrefManager prefManager = new PrefManager(this);
    public static PopupWindow mPopupWindow;
    public Navigator navigator = new Navigator();
    protected GoogleSignInClient mGoogleSignInClient;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        this.mToolbar = findViewById(R.id.toolbar);
        setupToolbar();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    public void startActivity(Intent intent, @Nullable Bundle options) {
        super.startActivity(intent, options);
        AnimationUtils.animationRightToLeftActivity(this);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        AnimationUtils.animationRightToLeftActivity(this);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        AnimationUtils.animationRightToLeftActivity(this);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode, @Nullable Bundle options) {
        super.startActivityForResult(intent, requestCode, options);
        AnimationUtils.animationRightToLeftActivity(this);
    }

    @Override
    public void finish() {
        Utils.hideSoftKeyboard(this);
        super.finish();
        AnimationUtils.animationLeftToRightActivity(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionsManager.getInstance().notifyPermissionsChange(permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new MyProgressDialog(this);
        }
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void success(Object x) {
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
    }

    public void replaceFragment( Fragment fragment, boolean addToBackStack,boolean isAnim) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (isAnim){
            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_right, R.anim.exit_to_left);
        }
        if (addToBackStack) {
            transaction.addToBackStack(fragment.getClass().getSimpleName());
        }
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fmContainer);
        if (currentFragment != null) {
            transaction.hide(currentFragment);
        }
       transaction.add(R.id.fmContainer, fragment, fragment.getClass().getSimpleName());
//        transaction.replace(R.id.fmContainer, fragment, fragment.getClass().getSimpleName());
        transaction.commit();
//        getSupportFragmentManager().executePendingTransactions();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            ScreenConfig.hideActionBar(this);
        }
    }

    private void setupToolbar() {
        if (this.mToolbar != null) {
            setSupportActionBar(this.mToolbar);
            ButterKnife.bind(this);
            final View parent = (View) mIbBack.getParent();  // button: the view you want to enlarge hit area
            parent.post(new Runnable() {
                public void run() {
                    final Rect rect = new Rect();
                    mIbBack.getHitRect(rect);
                    rect.top -= 30;    // increase top hit area
                    rect.left -= 30;   // increase left hit area
                    rect.bottom += 30; // increase bottom hit area
                    rect.right += 30;  // increase right hit area
                    parent.setTouchDelegate(new TouchDelegate(rect, mIbBack));
                }
            });
            mIbBack.setVisibility(View.VISIBLE);
            if (mIbBack != null) {
                mIbBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }
        }
    }

    public void isHideButtonBack(boolean bool) {
        if (bool) {
            mIbBack.setVisibility(View.GONE);
        }
    }

    @Override
    public void setTitle(@StringRes int resTitle) {
        if (mTvTitle != null) {
            mTvTitle.setText(resTitle);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        if (mTvTitle != null) {
            mTvTitle.setText(title);
        }
    }

    protected void showHideButtonBack(boolean isShow) {
        if (mIbBack != null) {
            mIbBack.setVisibility(isShow ? View.VISIBLE : View.GONE);
        }
    }

    protected void setBackgroundToolbar(int resColor) {
        if (mToolbar != null) {
            mToolbar.setBackgroundResource(resColor);
        }
    }

    protected void hideToolbar() {
        if (this.mToolbar != null) {
            this.mToolbar.setVisibility(View.GONE);
        }
    }

    protected void showToolbar() {
        if (this.mToolbar != null) {
            this.mToolbar.setVisibility(View.VISIBLE);
        }
    }

    public void hideKeyboard(View view) {
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        hideProgress();
    }

    @Override
    public void onBackPressed() {
        if (mPopupWindow != null && mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
        } else {
            super.onBackPressed();
        }
    }
    public void backgroudToolbar(boolean white) {
        if (mToolbar != null) {
            if (white) {
                mToolbar.setBackground(new ColorDrawable(ContextCompat.getColor(this, R.color.colorWhite)));
                if (mTvTitle != null) {
                    mTvTitle.setTextColor(ContextCompat.getColor(this, R.color.colorBlack));
                }
            }else {
                mToolbar.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_button));
                if (mTvTitle != null) {
                    mTvTitle.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
                }
            }
        }
    }
    public void signOutGoogle() {
        mGoogleSignInClient.signOut().addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }
    public void signRevokeAccessGoogle() {
        mGoogleSignInClient.revokeAccess().addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.e("SSSSSSSSSSSSSSSSSS", "SSSSSSSSSSSSSSSSSSSSSS");
            }
        });
    }
}