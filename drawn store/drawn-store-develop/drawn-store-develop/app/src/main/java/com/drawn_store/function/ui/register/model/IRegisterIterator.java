package com.drawn_store.function.ui.register.model;

import com.drawn_store.common.presenter.BaseInteractor;
import com.drawn_store.function.common.objects.request.RegisterReq;


public interface IRegisterIterator extends BaseInteractor {
    void register(RegisterReq registerReq);
}
