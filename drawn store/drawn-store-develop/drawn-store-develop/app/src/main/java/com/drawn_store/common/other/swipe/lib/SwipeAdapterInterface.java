package com.drawn_store.common.other.swipe.lib;

public interface SwipeAdapterInterface {
    int getSwipeLayoutResourceId(int position);
    void notifyDatasetChanged();
}
