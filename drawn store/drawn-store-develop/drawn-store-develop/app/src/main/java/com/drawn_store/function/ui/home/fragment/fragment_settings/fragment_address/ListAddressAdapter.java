package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.drawn_store.R;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.function.common.objects.ListAddressInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListAddressAdapter extends RecyclerView.Adapter<ListAddressAdapter.ViewHodel> {

    private Context context;
    private List<ListAddressInfo> listAddressInfos;
    private ClickEdit clickEdit;

    public ListAddressAdapter(Context context) {
        this.context = context;

    }

    public void swap(List<ListAddressInfo> listAddressInfos) {
        this.listAddressInfos = listAddressInfos;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ListAddressAdapter.ViewHodel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_address, parent, false);
        return new ListAddressAdapter.ViewHodel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListAddressAdapter.ViewHodel holder, int position) {
        ListAddressInfo listAddressInfo = listAddressInfos.get(position);
        holder.tvNameAddres.setText(listAddressInfo.getName());
        if (listAddressInfo.getIsDefault()) {
            holder.tvDefault.setVisibility(View.VISIBLE);
        } else {
            holder.tvDefault.setVisibility(View.GONE);
        }
        holder.tvNameStreet.setText(Utils.splipStringStart(listAddressInfo.getAddress()));
        holder.tvNameStreetFull.setText(Utils.splipStringEnd(listAddressInfo.getAddress()));
        holder.tvPhone.setText(listAddressInfo.getPhoneNumber());
        holder.tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickEdit.setEdit(view,listAddressInfo,position);
            }
        });
    }

    public void setOnClickEdt(ClickEdit clickEdit) {
        this.clickEdit = clickEdit;
    }


    @Override
    public int getItemCount() {
        return listAddressInfos.size();
    }

    public class ViewHodel extends RecyclerView.ViewHolder {

        @BindView(R.id.tvNameAddres)
        TextView tvNameAddres;

        @BindView(R.id.tvEdit)
        TextView tvEdit;
        @BindView(R.id.tvPhone)
        TextView tvPhone;
        @BindView(R.id.tvNameStreet)
        TextView tvNameStreet;
        @BindView(R.id.tvNameStreetFull)
        TextView tvNameStreetFull;
        @BindView(R.id.tvDefault)
        TextView tvDefault;

        public ViewHodel(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvDefault.setVisibility(View.GONE);
        }
    }

    public interface ClickEdit {
        void setEdit(View view, ListAddressInfo addressInfo, int postion);
    }
}
