package com.drawn_store.function.ui.home.fragment.fragment_search;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.drawn_store.R;
import com.drawn_store.function.common.objects.ProductsInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchTestApdater extends RecyclerView.Adapter<SearchTestApdater.ViewHodel> {

    private Context context;
    private List<ProductsInfo> productInfos;
    private List<ProductsInfo> productInfosList;
    private String searchText;
    private ClickItemName clickItemName;
    public SearchTestApdater(Context context) {
        this.context = context;
    }

    public void swap(List<ProductsInfo> productInfos) {
        this.productInfos = productInfos;
        this.productInfosList = productInfos;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHodel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_text_search, parent, false);
        return new ViewHodel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHodel holder, int position) {
        ProductsInfo productInfo = productInfos.get(position);
        holder.tvTextSearch.setText(Html.fromHtml(productInfo.getName()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickItemName.setName(view,position,productInfo.getName());
            }
        });
//        if (searchText != null && !searchText.isEmpty()) {
//            int startPos = productInfo.getName().toLowerCase(Locale.US).indexOf(searchText.toLowerCase(Locale.US));
//            int endPos = startPos + searchText.length();
//
//            if (startPos != -1) {
//                Spannable spannable = new SpannableString(productInfo.getName());
//                ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.BLUE});
//                TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
//                spannable.setSpan(highlightSpan, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                holder.tvTextSearch.setText(spannable);
//            } else {
//                holder.tvTextSearch.setText(productInfo.getName());
//            }
//        } else {
//            holder.tvTextSearch.setText(productInfo.getName());
//        }
    }
    public void setOnClickName(ClickItemName clickName){
        this.clickItemName = clickName;
    }


    @Override
    public int getItemCount() {
        return productInfos.size();
    }

    public class ViewHodel extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTextSearch)
        TextView tvTextSearch;

        public ViewHodel(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    public void setFilter(String searchText) {
        productInfos.addAll(productInfosList);
        this.searchText = searchText;
        notifyDataSetChanged();
    }


    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    productInfos = productInfosList;
                } else {
                    List<ProductsInfo> filteredList = new ArrayList<>();
                    for (ProductsInfo row : productInfosList) {
                        if (row.getName().toLowerCase(Locale.getDefault()).contains(charString.toLowerCase(Locale.getDefault()))
                        || row.getDescription().contains(charSequence))
                            filteredList.add(row);
                    }
                    productInfos = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = productInfos;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                productInfos = (List<ProductsInfo>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
    public interface ClickItemName{
        void setName(View view, int position, String name);
    }
}
