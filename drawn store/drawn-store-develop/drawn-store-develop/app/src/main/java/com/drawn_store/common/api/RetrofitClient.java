package com.drawn_store.common.api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.drawn_store.Apps;
import com.drawn_store.BuildConfig;
import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.common.constants.Constants;
import com.drawn_store.common.pref.PrefManager;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Dispatcher;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit retrofit = null;
    private static final int CONNECT_TIMEOUT = 15; // 30s

    private RetrofitClient() {
    }

    public static Retrofit getInstance() {
        if (retrofit == null) {
            Context context = Apps.getInstance();

            Dispatcher dispatcher = new Dispatcher();
            dispatcher.setMaxRequests(1);
            HttpLoggingInterceptor interceptorLog = new HttpLoggingInterceptor();
            if (BuildConfig.DEBUG) {
                interceptorLog.setLevel(HttpLoggingInterceptor.Level.BODY);
            }

            Interceptor interceptorRequest = new Interceptor() {
                @NotNull
                @Override
                public Response intercept(@NotNull Chain chain) throws IOException {
                    Request.Builder builder = chain.request().newBuilder()
                            .header("Accept", "application/json")
                            .header("Content-Type", "application/json");
                    return chain.proceed(builder.build());
                }
            };
            OkHttpClient okClient =
                    new OkHttpClient.Builder()
                            .addInterceptor(interceptorRequest)
                            .addInterceptor(interceptorLog)
                            .connectTimeout(CONNECT_TIMEOUT,
                                    TimeUnit.SECONDS)
                            .readTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                            .writeTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                            .dispatcher(dispatcher)
                            .build();

            Gson gson = new GsonBuilder()
                    .setLenient().create();
            retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl(ConstantApi.BASE_URL).client(okClient).build();
        }
        return retrofit;
    }
}
