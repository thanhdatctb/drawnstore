package com.drawn_store.function.ui.register.presenter;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.presenter.InteractorCallback;
import com.drawn_store.function.common.objects.request.RegisterReq;
import com.drawn_store.function.ui.register.model.IRegisterIterator;
import com.drawn_store.function.ui.register.model.RegisterIteratorImpl;

public class RegisterPresenterImpl implements IRegisterPresenter{

    IRegisterIterator mIRegisterIterator;

    public RegisterPresenterImpl(BaseView<BaseObj,Object> mBaseView){
        InteractorCallback<BaseObj,Object> mCallBack = new InteractorCallback<BaseObj, Object>() {
            @Override
            public void success(BaseObj x) {
                mBaseView.success(x);
            }

            @Override
            public void error(Object o, boolean isShowDialog) {

                mBaseView.error(o,isShowDialog);
            }
        };
        mIRegisterIterator = new RegisterIteratorImpl(mCallBack);
    }


    @Override
    public void register(RegisterReq registerReq) {
        mIRegisterIterator.register(registerReq);
    }
}
