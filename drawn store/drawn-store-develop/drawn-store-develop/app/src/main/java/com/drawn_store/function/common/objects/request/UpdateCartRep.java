package com.drawn_store.function.common.objects.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateCartRep {
    @SerializedName("cartId")
    @Expose
    private Integer cartId;
    @SerializedName("amount")
    @Expose
    private Integer amount;

    public UpdateCartRep() {
    }

    public UpdateCartRep(Integer cartId, Integer amount) {
        this.cartId = cartId;
        this.amount = amount;
    }

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
