package com.drawn_store.function.common.api.request;

import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.objects.request.AddressRep;
import com.drawn_store.function.common.objects.request.UpdateAddressRep;
import com.drawn_store.function.common.objects.response.AddAddressRes;
import com.drawn_store.function.common.objects.response.DeleteAddressRes;
import com.drawn_store.function.common.objects.response.DistrictRes;
import com.drawn_store.function.common.objects.response.ListAddressRes;
import com.drawn_store.function.common.objects.response.ProvinceRes;
import com.drawn_store.function.common.objects.response.UpdateAddressRes;
import com.drawn_store.function.common.objects.response.WardsRes;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AddressRequest {

    @GET(ConstantApi.URL_PROVINCE)
    Call<ProvinceRes> getProvince();

    @GET(ConstantApi.URL_DISTRICT)
    Call<DistrictRes> getDistrict(@Path("code") String code);


    @GET(ConstantApi.URL_WARDS)
    Call<WardsRes> getWards(@Path("code") String code);

    @GET(ConstantApi.URL_LIST_ADDRESS)
    Call<ListAddressRes> getListAddress(@Path("userId") int userId);

    @PUT(ConstantApi.URL_ADD_ADDRESS)
    Call<AddAddressRes> addAddress(@Body AddressRep addressRep);

    @POST(ConstantApi.URL_ADD_ADDRESS)
    Call<UpdateAddressRes> updateAddress(@Body UpdateAddressRep updateAddressRep);


    @HTTP(method = "DELETE", path = ConstantApi.URL_DELETE_ADDRESS, hasBody = true)
    Call<DeleteAddressRes> deleteAddress(@Path("infoId") int infoId);
}
