package com.drawn_store.function.common.api.request;

import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.objects.request.OrderReq;
import com.drawn_store.function.common.objects.response.OrderRes;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface OrderRequest {
    @POST(ConstantApi.URL_ORDER)
    Call<OrderRes> order(@Body OrderReq orderReq);

    @GET(ConstantApi.URL_SHOW_ORDER)
    Call<OrderRes> showOrder(@Path("userId") int userId, @Path("filter") String filter);
}
