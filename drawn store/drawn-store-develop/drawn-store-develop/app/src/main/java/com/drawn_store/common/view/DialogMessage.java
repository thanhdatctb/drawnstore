package com.drawn_store.common.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.drawn_store.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class DialogMessage extends Dialog {
    @BindView(R.id.tvTitle)
    protected TextView tvTtilte;
    @BindView(R.id.tvMessage)
    protected TextView tvMessage;
    @BindView(R.id.tvNo)
    protected TextView tvNo;
    @BindView(R.id.tvYes)
    protected TextView tvYes;
    @BindView(R.id.dividerVertical)
    protected View dividerVertical;

    private OnDialogMessageListener mListener;
    private OnDismissListener onDismissListener;
    private String title;
    private String message;
    private String textNO;
    private String textYes;
    private boolean isAutoHide = true;

    public DialogMessage(@NonNull Context context) {
        super(context, R.style.DialogMessageStyle);
    }

    public DialogMessage(@NonNull Context context, String title, String message, String textNO, String textYes, OnDialogMessageListener mListener) {
        super(context, R.style.DialogMessageStyle);
        this.mListener = mListener;
        this.title = title;
        this.message = message;
        this.textNO = textNO;
        this.textYes = textYes;
    }

    public DialogMessage(@NonNull Context context, String message, String textYes, OnDialogMessageListener mListener) {
        super(context, R.style.DialogMessageStyle);
        this.mListener = mListener;
        this.message = message;
        this.textYes = textYes;
    }

    public DialogMessage(@NonNull Context context, String message, String textYes,
                         OnDialogMessageListener mListener, OnDismissListener onDismissListener) {
        super(context, R.style.DialogMessageStyle);
        this.mListener = mListener;
        this.message = message;
        this.textYes = textYes;
        this.onDismissListener = onDismissListener;
    }

    public DialogMessage(@NonNull Context context, String title, String message, String textNO, String textYes) {
        super(context, R.style.DialogMessageStyle);
        this.title = title;
        this.message = message;
        this.textNO = textNO;
        this.textYes = textYes;
    }

    public DialogMessage(@NonNull Context context, String title, String message, String textYes, OnDialogMessageListener mListener) {
        super(context, R.style.DialogMessageStyle);
        this.mListener = mListener;
        this.title = title;
        this.message = message;
        this.textYes = textYes;
    }

    public DialogMessage(@NonNull Context context, String title, String message, String textYes) {
        super(context, R.style.DialogMessageStyle);
        this.title = title;
        this.message = message;
        this.textYes = textYes;
    }

    public DialogMessage(@NonNull Context context, String title, String message) {
        super(context, R.style.DialogMessageStyle);
        this.title = title;
        this.message = message;
        this.textYes = getContext().getString(R.string.ok);
    }

    public DialogMessage(@NonNull Context context, String title, String message,boolean isAutoHide) {
        super(context, R.style.DialogMessageStyle);
        this.title = title;
        this.message = message;
        this.textYes = getContext().getString(R.string.ok);
        this.isAutoHide = isAutoHide;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_message);
        ButterKnife.bind(this);

        tvTtilte.setText(title != null ? title : getContext().getString(R.string.announce));
        tvMessage.setText(message != null ? message : "");
        if (textNO != null) {
            tvNo.setVisibility(View.VISIBLE);
            dividerVertical.setVisibility(View.VISIBLE);
            tvNo.setText(textNO);
            tvYes.setBackground(getContext().getResources().getDrawable(R.drawable.bg_selector_button_right_dialog));
        } else {
            tvNo.setVisibility(View.GONE);
            dividerVertical.setVisibility(View.GONE);
            tvYes.setBackground(getContext().getResources().getDrawable(R.drawable.bg_selector_button_full_dialog));
        }
        tvYes.setText(textYes != null ? textYes : "");

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if(DialogMessage.this!=null && DialogMessage.this.isShowing() && isAutoHide){
                    dismiss();
                }
            }
        };
        new Handler().postDelayed(runnable, 15*1000);
    }
    static int ui_flags =
            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
    @Override
    public void show() {
        // Set the dialog to not focusable.
        getWindow().getDecorView().setSystemUiVisibility(ui_flags);

        // Show the dialog with NavBar hidden.
        super.show();

        // Set the dialog to focusable again.
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }

    @OnClick(R.id.tvYes)
    public void yesClick() {
        if (mListener != null) {
            mListener.yesClick();
        }
        dismiss();
    }

    @OnClick(R.id.tvNo)
    public void noClick() {
        dismiss();
    }

    public interface OnDialogMessageListener {
        void yesClick();
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }
}

