package com.drawn_store.function.ui.home.fragment.fragment_payment.model;


import com.drawn_store.function.common.objects.request.LikeRep;
import com.drawn_store.function.common.objects.request.OrderReq;

public interface OrderIterator {
    void order(OrderReq orderReq);
    void showOrder(int userId,String filter);
}
