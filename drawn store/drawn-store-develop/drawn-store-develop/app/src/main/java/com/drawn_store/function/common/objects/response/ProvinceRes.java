package com.drawn_store.function.common.objects.response;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.function.common.objects.ProductType;
import com.drawn_store.function.common.objects.ProvinceInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProvinceRes extends BaseObj {

    @SerializedName("data")
    @Expose
    private List<ProvinceInfo> data = null;

    public List<ProvinceInfo> getData() {
        return data;
    }

    public void setData(List<ProvinceInfo> data) {
        this.data = data;
    }
}
