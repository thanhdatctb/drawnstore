package com.drawn_store.common.view;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.drawn_store.R;
import com.drawn_store.common.base.BaseBottomSheetDialogFragment;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.function.common.constants.FConstants;
import com.drawn_store.function.common.objects.request.ChangerPasswordRep;
import com.drawn_store.function.common.objects.response.UpdateAccountRes;
import com.drawn_store.function.common.objects.response.UserRes;
import com.drawn_store.function.ui.login.presenter.LoginPresenterImpl;
import com.google.android.material.button.MaterialButton;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class DialogChangerInfoUser extends BaseBottomSheetDialogFragment implements View.OnClickListener {

    public DialogChangerInfoUser newInstance(boolean changerPassword) {
        Bundle args = new Bundle();
        args.putBoolean(FConstants.KEY_CHANGER_PASSWORD, changerPassword);
        DialogChangerInfoUser fragment = new DialogChangerInfoUser();
        fragment.setArguments(args);
        return fragment;
    }


    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.imgClose)
    ImageView imgClose;
    @BindView(R.id.imgDeleteText)
    ImageView imgDeleteText;
    @BindView(R.id.rlChangerName)
    LinearLayout rlChangerName;
    @BindView(R.id.rlChangerPassword)
    LinearLayout rlChangerPassword;
    @BindView(R.id.edtName)
    EditText edtName;
    @BindView(R.id.view1)
    View view1;
    @BindView(R.id.bntSave)
    MaterialButton bntSave;
    private boolean changerPassword;
    @BindView(R.id.edPasswordOld)
    EditText edPasswordOld;
    @BindView(R.id.edPasswordNew)
    EditText edPasswordNew;
    @BindView(R.id.edPasswordNewConfirm)
    EditText edPasswordNewConfirm;
    @BindView(R.id.bntChangerPassword)
    MaterialButton bntChangerPassword;
    @BindView(R.id.view2)
    View view2;
    @BindView(R.id.view3)
    View view3;
    @BindView(R.id.view4)
    View view4;
    private LoginPresenterImpl mLoginPresenter;
    private Bitmap mBitmap;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_changer_infouser, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        changerPassword = getArguments().getBoolean(FConstants.KEY_CHANGER_PASSWORD);
        mLoginPresenter = new LoginPresenterImpl(this);
        showUI();
        setListener();
    }

    private void showUI() {
        if (changerPassword) {
            tvTitle.setText(R.string.changer_password);
            rlChangerPassword.setVisibility(View.VISIBLE);
            rlChangerName.setVisibility(View.GONE);
            showEventChangerPassword();
        } else {
            tvTitle.setText(R.string.name);
            rlChangerName.setVisibility(View.VISIBLE);
            rlChangerPassword.setVisibility(View.GONE);
            showEventChangerName();
        }
    }
    public DialogChangerInfoUser setBitmap(Bitmap bitmap) {
        this.mBitmap = bitmap;
        return this;
    }
    private void setListener() {
        imgClose.setOnClickListener(this);
        imgDeleteText.setOnClickListener(this);
        bntSave.setOnClickListener(this);
        bntChangerPassword.setOnClickListener(this);
    }

    private void functionChangerPassword() {
        String oldPass = edPasswordOld.getText().toString();
        String newPass = edPasswordNew.getText().toString();
        String confirmPass = edPasswordNewConfirm.getText().toString();
        if (oldPass.equals("") || newPass.equals("") || confirmPass.equals("")) {
            DialogUtil.showDialogMessage(requireContext(), getResources().getString(R.string.fill_full_info));
        } else if (!newPass.equals(confirmPass)) {
            DialogUtil.showDialogMessage(requireContext(), getResources().getString(R.string.do_not_match));
        } else {
            showProgress();
            mLoginPresenter.changerPassword(new ChangerPasswordRep(loginRes.getData().getEmail(),
                    oldPass, confirmPass));
        }
    }

    @Override
    public void success(Object x) {
        super.success(x);
        hideProgress();
        if (x instanceof UserRes) {
            DialogUtil.showDialogMessage(requireContext(), getResources().getString(R.string.changer_password_success), R.string.ok, new DialogMessage.OnDialogMessageListener() {
                @Override
                public void yesClick() {
                    dismiss();
                }
            });
        }
     else  if (x instanceof UpdateAccountRes) {
            UpdateAccountRes updateAccountRes = (UpdateAccountRes) x;
//            loginRess.getData().setId(loginRes.getData().getId());
            loginRes.getData().setAvatar(updateAccountRes.getData().getAvatar());
            loginRes.getData().setName(edtName.getText().toString());
            String personJsonString = Utils.getGsonParser().toJson(loginRes);
            prefManager.saveSetting(FConstants.KEY_PUT_USERINFO, personJsonString);
            DialogUtil.showDialogMessage(requireContext(), getResources().getString(R.string.update_success));
            dismiss();

        }
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj err = (BaseObj) o;
        if (err.getStatusCode() == 404) {
            DialogUtil.showDialogMessage(requireContext(), getResources().getString(R.string.old_password_incorrect));
        } else {
            DialogUtil.showDialogMessage(requireContext(), err.getMessage());
        }
    }

    private void showEventChangerName() {
        edtName.setText(loginRes.getData().getName());
        edtName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    view1.setBackgroundColor(getResources().getColor(R.color.colorGradientEnd));
                } else {
                    view1.setBackgroundColor(getResources().getColor(R.color.color_light_gray));
                }
            }
        });
    }

    private void showEventChangerPassword() {
        edPasswordOld.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    view2.setBackgroundColor(getResources().getColor(R.color.colorGradientEnd));
                } else {
                    view2.setBackgroundColor(getResources().getColor(R.color.color_light_gray));
                }
            }
        });
        edPasswordNew.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    view3.setBackgroundColor(getResources().getColor(R.color.colorGradientEnd));
                } else {
                    view3.setBackgroundColor(getResources().getColor(R.color.color_light_gray));
                }
            }
        });
        edPasswordNewConfirm.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    view4.setBackgroundColor(getResources().getColor(R.color.colorGradientEnd));
                } else {
                    view4.setBackgroundColor(getResources().getColor(R.color.color_light_gray));
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgClose:
                dismiss();
                break;
            case R.id.imgDeleteText:
                edtName.setText("");
                break;
            case R.id.bntSave:
                if (edtName.getText().toString().equals("")){
                    DialogUtil.showDialogMessage(requireContext(), "Vui lòng nhập Họ & Tên");
                }else {
                    showProgress();
                    multipartImageUpload();
                }
                break;
            case R.id.bntChangerPassword:
                functionChangerPassword();
                break;
        }
    }

    private void multipartImageUpload() {
        try {
            File filesDir = requireActivity().getFilesDir();
            File file = new File(filesDir, "image" + ".png");

            OutputStream os;
            try {
                os = new FileOutputStream(file);
                mBitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
                os.flush();
                os.close();
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
            }

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            mBitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), reqFile);
            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), edtName.getText().toString());
            RequestBody email = RequestBody.create(MediaType.parse("text/plain"), loginRes.getData().getEmail());
            mLoginPresenter.updateAccount(email, name, body);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
