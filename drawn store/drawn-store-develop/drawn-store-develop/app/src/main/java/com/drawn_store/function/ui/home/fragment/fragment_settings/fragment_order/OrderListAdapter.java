package com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.drawn_store.R;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.function.common.objects.OrderInfo;
import com.drawn_store.function.common.objects.ProductsOrderInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.ViewHodel> implements OrderAdapter.SetDataOrder {

    private Context context;
    private List<OrderInfo> orderInfoList;
    private List<ProductsOrderInfo> productsInfos;
    private OrderAdapter orderAdapter;
    private int amount;
    private double countSale;
    public OrderListAdapter(Context context) {
        this.context = context;
    }

    public void swap(List<OrderInfo> orderInfoList) {
        this.orderInfoList = orderInfoList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OrderListAdapter.ViewHodel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_order_list, parent, false);
        return new ViewHodel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderListAdapter.ViewHodel holder, int position) {
        OrderInfo orderInfo = orderInfoList.get(position);
        holder.tvStatus.setText(orderInfo.getStatus());
        holder.tvTotal.setText(Utils.formatAmount(Double.parseDouble(orderInfo.getTotalPrice())));
        productsInfos = orderInfo.getProducts();
        orderAdapter = new OrderAdapter(context);
        holder.rvList.setHasFixedSize(true);
        holder.rvList.setLayoutManager(new LinearLayoutManager(context));
        holder.rvList.setAdapter(orderAdapter);
        orderAdapter.swap(productsInfos);
        orderAdapter.setOnDataOrder(this);
        //        amount = productsInfo
//        holder.tvSizeNote.setText(amount+" Sản phẩm, tổng số:");
//        holder.tvCountSale.setText(Utils.formatAmount(countSale));
    }

    @Override
    public int getItemCount() {
        return orderInfoList.size();
    }

    @Override
    public void dataOrder(double countSale, int amount) {
        this.amount = amount;
        this.countSale = countSale;
    }

    public class ViewHodel extends RecyclerView.ViewHolder {
        @BindView(R.id.tvStatus)
        TextView tvStatus;
        @BindView(R.id.tvTotal)
        TextView tvTotal;
        @BindView(R.id.rvList)
        RecyclerView rvList;
        @BindView(R.id.tvSizeNote)
        TextView tvSizeNote;
        @BindView(R.id.tvCountSale)
        TextView tvCountSale;
        public ViewHodel(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
