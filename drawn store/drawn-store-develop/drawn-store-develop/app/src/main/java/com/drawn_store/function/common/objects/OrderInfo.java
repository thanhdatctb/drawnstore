package com.drawn_store.function.common.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderInfo {
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("totalPrice")
    @Expose
    private String totalPrice;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("products")
    @Expose
    private List<ProductsOrderInfo> products = null;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<ProductsOrderInfo> getProducts() {
        return products;
    }

    public void setProducts(List<ProductsOrderInfo> products) {
        this.products = products;
    }

}
