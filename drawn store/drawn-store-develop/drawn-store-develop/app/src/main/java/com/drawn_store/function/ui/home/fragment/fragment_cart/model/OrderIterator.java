package com.drawn_store.function.ui.home.fragment.fragment_cart.model;

import com.drawn_store.function.common.objects.request.CartRep;
import com.drawn_store.function.common.objects.request.UpdateCartRep;


public interface OrderIterator {
    void addCart(CartRep cartRep);
    void getCart(int customerId);
    void deleteCart(int cartId);
    void updateCart(UpdateCartRep updateCartRep);
}
