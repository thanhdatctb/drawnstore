package com.drawn_store.function.ui.home.fragment.fragment_payment.view;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.drawable.PaintDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.drawn_store.R;
import com.drawn_store.common.api.ErrorCode;
import com.drawn_store.common.base.BaseFragment;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.function.common.constants.FConstants;
import com.drawn_store.function.common.objects.ListAddressInfo;
import com.drawn_store.function.common.objects.PaymentOrderInfo;
import com.drawn_store.function.common.objects.request.OrderReq;
import com.drawn_store.function.common.objects.request.Product;
import com.drawn_store.function.common.objects.response.ListAddressRes;
import com.drawn_store.function.common.objects.response.OrderRes;
import com.drawn_store.function.common.view.BackReload;
import com.drawn_store.function.ui.home.fragment.fragment_payment.presenter.OrderPresenterImpl;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.FragmentAddress;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.FragmentEnterAddress;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_address.dialog.presenter.IAddressPresenterImpl;
import com.drawn_store.function.ui.home.fragment.fragment_settings.fragment_order.FragmentOrder;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentPayment extends BaseFragment implements View.OnClickListener,
        PaymentOrderAdapter.SetDataOrder, BackReload {

    public FragmentPayment newInstance(List<PaymentOrderInfo> paymentOrderInfos) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(FConstants.KEY_LIST_PAYMENT_ORDER, (ArrayList<PaymentOrderInfo>) paymentOrderInfos);
        FragmentPayment fragment = new FragmentPayment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.rlAddress)
    RelativeLayout rlAddress;
    @BindView(R.id.lnShowAdd)
    LinearLayout lnShowAdd;
    @BindView(R.id.tvNameAddres)
    TextView tvNameAddres;
    @BindView(R.id.tvNameStreetFull)
    TextView tvNameStreetFull;
    @BindView(R.id.tvPhoneAddres)
    TextView tvPhoneAddres;
    @BindView(R.id.tvEdit)
    TextView tvEdit;
    @BindView(R.id.viewTwoColors)
    View viewTwoColors;
    @BindView(R.id.rvOrder)
    RecyclerView rvOrder;
    @BindView(R.id.tvSizePack)
    TextView tvSizePack;
    @BindView(R.id.tvTotal)
    TextView tvTotal;
    @BindView(R.id.tvTotalPay)
    TextView tvTotalPay;
    @BindView(R.id.tvAmountShip)
    TextView tvAmountShip;
    @BindView(R.id.tvAmountProvisional)
    TextView tvAmountProvisional;
    @BindView(R.id.tvProvisional)
    TextView tvProvisional;
    @BindView(R.id.btnPayment)
    MaterialButton btnPayment;
    private IAddressPresenterImpl iAddressPresenterImpl;
    private OrderPresenterImpl orderPresenter;
    private List<ListAddressInfo> listAddressInfos;
    private boolean checkDefault = true;
    private List<PaymentOrderInfo> paymentOrderInfos;
    private PaymentOrderAdapter paymentOrderAdapter;
    private double totalA = 0.0;
    private double amountShip = 40000;
    int AmountA;
    private PaymentOrderInfo paymentOrderInfo;
    double totalPayment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_payment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        isHideButtonBack(false);
        backgroudToolbar(true);
        setTitle(R.string.payment);
        listAddressInfos = new ArrayList<>();
        paymentOrderInfos = new ArrayList<>();
        paymentOrderInfos = getArguments().getParcelableArrayList(FConstants.KEY_LIST_PAYMENT_ORDER);
        iAddressPresenterImpl = new IAddressPresenterImpl(this);
        orderPresenter = new OrderPresenterImpl(this);
        iAddressPresenterImpl.listAddres(loginRes.getData().getId());
        showProgress();
        setListener();
    }

    @Override
    public void success(Object x) {
        super.success(x);
        hideProgress();
        if (x instanceof ListAddressRes) {
            ListAddressRes listAddressRes = (ListAddressRes) x;
            listAddressInfos = listAddressRes.getData();
            checkDefault = false;
            for (int i = 0; i < listAddressInfos.size(); i++) {
                if (listAddressInfos.get(i).getDefault().equals(true)) {
                    lnShowAdd.setVisibility(View.VISIBLE);
                    rlAddress.setVisibility(View.GONE);
                    tvNameAddres.setText(listAddressInfos.get(i).getName());
                    tvNameStreetFull.setText(listAddressInfos.get(i).getAddress());
                    tvPhoneAddres.setText(listAddressInfos.get(i).getPhoneNumber());
                }
            }
        } else if (x instanceof OrderRes) {
            DialogUtil.showDialogMessage(requireContext(), getResources().getString(R.string.order_suc));
            replaceFragment(new FragmentOrder().newInstance(0, getResources().getString(R.string.wating_pack)), true, false);
        }
        showListProduct();
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj err = (BaseObj) o;
        if (err.getStatusCode() == ErrorCode.CODE_NOT_FOUND) {
            checkDefault = true;
            lnShowAdd.setVisibility(View.GONE);
            rlAddress.setVisibility(View.VISIBLE);
        } else {
            DialogUtil.showDialogMessage(getContext(), err.getMessage() + " (" + err.getStatusCode() + ")");
        }
    }

    private void setListener() {
        rlAddress.setOnClickListener(this);
        tvEdit.setOnClickListener(this);
        btnPayment.setOnClickListener(this);
    }

    private void showListProduct() {
        tvSizePack.setText("Tổng gói hàng " + paymentOrderInfos.size());
        paymentOrderAdapter = new PaymentOrderAdapter(requireContext());
        rvOrder.setHasFixedSize(true);
        rvOrder.setLayoutManager(new LinearLayoutManager(requireContext()));
        rvOrder.setAdapter(paymentOrderAdapter);
        paymentOrderAdapter.swap(paymentOrderInfos);
        paymentOrderAdapter.setOnDataOrder(this);

    }

    private void setColorView() {
        ShapeDrawable.ShaderFactory sf = new ShapeDrawable.ShaderFactory() {
            @Override
            public Shader resize(int i, int i1) {
                LinearGradient lg = new LinearGradient(7, 0, viewTwoColors.getWidth(), 0,
                        new int[]{
                                Color.BLUE,
                                Color.BLUE,
                                Color.RED,
                                Color.RED
                        },
                        new float[]{
                                0, 0.50f, 0.50f, 1
                        }, Shader.TileMode.REPEAT);
                return lg;
            }
        };
        PaintDrawable paintDrawable = new PaintDrawable();
        paintDrawable.setShape(new RectShape());
        paintDrawable.setShaderFactory(sf);
        viewTwoColors.setBackgroundDrawable(paintDrawable);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvEdit:
                replaceFragment(new FragmentAddress().newInstance().setReloadFragment(this), true, false);
                break;
            case R.id.rlAddress:
                replaceFragment(new FragmentEnterAddress().newInstance(checkDefault).checkAdd(true), true, false);
                break;
            case R.id.btnPayment:
                showProgress();
                List<Product> products = new ArrayList<>();
                for (int i = 0; i < paymentOrderInfos.size(); i++) {
                    products.add(new Product(paymentOrderInfos.get(i).getId(), paymentOrderInfos.get(i).getAmount(),
                            paymentOrderInfos.get(i).getSize(), paymentOrderInfos.get(i).getColor()));
                }
                orderPresenter.order(new OrderReq(loginRes.getData().getId(), "waiting",
                        String.valueOf(totalPayment), tvNameStreetFull.getText().toString(),
                        products));
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void dataOrder(double total, PaymentOrderInfo paymentOrderInfo, int amount) {
        amount = AmountA + amount;
        AmountA = amount;
        tvProvisional.setText("Tạm tính ( " + AmountA + " )");
        tvAmountShip.setText(Utils.formatAmount(amountShip));
        total = this.totalA + total;
        this.totalA = total;
        totalPayment = totalA + amountShip;
        tvAmountProvisional.setText(Utils.formatAmount(totalA));
        tvTotalPay.setText(Utils.formatAmount(totalPayment));
        tvTotal.setText(Utils.formatAmount(totalPayment));
        this.paymentOrderInfo = paymentOrderInfo;
    }

    @Override
    public void reloadFragment() {
        showProgress();
        iAddressPresenterImpl.listAddres(loginRes.getData().getId());
    }
}
