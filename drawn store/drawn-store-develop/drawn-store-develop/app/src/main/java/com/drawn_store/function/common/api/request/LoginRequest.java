package com.drawn_store.function.common.api.request;


import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.objects.request.CheckEmailRep;
import com.drawn_store.function.common.objects.request.LoginReq;
import com.drawn_store.function.common.objects.request.LoginReqGoogle;
import com.drawn_store.function.common.objects.response.UserRes;
import com.drawn_store.function.common.objects.response.LoginRes;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface LoginRequest {
    @POST(ConstantApi.URL_LOGIN)
    Call<LoginRes> login(@Body LoginReq loginReq);

    @POST(ConstantApi.URL_LOGIN_GOOGLE)
    Call<LoginRes> loginGoogle(@Body LoginReqGoogle loginReq);


}
