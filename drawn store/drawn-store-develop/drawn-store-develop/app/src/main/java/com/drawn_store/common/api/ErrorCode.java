package com.drawn_store.common.api;


public class ErrorCode {
    public static final int NO_INTERNET = -1;
    public static final int CODE_401 = 401;
    public static final int CODE_NOT_FOUND = 404;
    public static final int CODE_INTERNAL_SERVER_ERROR = 500;
    public static final int BAD_REQUEST = 403;
}
