package com.drawn_store.function.common.objects.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartRep {

    @SerializedName("customerId")
    @Expose
    private Integer customerId;
    @SerializedName("productId")
    @Expose
    private Integer productId;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("size")
    @Expose
    private String size;

    public CartRep() {
    }

    public CartRep(Integer customerId, Integer productId, Integer amount, String color, String size) {
        this.customerId = customerId;
        this.productId = productId;
        this.amount = amount;
        this.color = color;
        this.size = size;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
