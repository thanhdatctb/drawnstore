package com.drawn_store.function.ui.forgot_password.view;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.drawn_store.R;
import com.drawn_store.common.base.BaseActivity;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.common.view.DialogMessage;
import com.drawn_store.common.view.ToastUtils;
import com.drawn_store.function.common.constants.FConstants;
import com.drawn_store.function.common.objects.UserInfo;
import com.drawn_store.function.common.objects.request.ChangerPasswordForgotRep;
import com.drawn_store.function.common.objects.request.EmployeeReq;
import com.drawn_store.function.common.objects.response.UserRes;
import com.drawn_store.function.common.view.PinEntryEditText;
import com.drawn_store.function.ui.forgot_password.presenter.ForgotPasswordPresenterImpl;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForgotPasswordActivity extends BaseActivity implements BaseView, View.OnClickListener {

    @BindView(R.id.btn_back)
    ImageButton imgBack;
    @BindView(R.id.tvMessage)
    TextView tvMessage;
    @BindView(R.id.lnFormNewPassword)
    LinearLayout lnFormNewPassword;
    @BindView(R.id.edNewPassword)
    EditText edNewPassword;
    @BindView(R.id.edConfirmNewPassword)
    EditText edConfirmNewPassword;
    @BindView(R.id.edPinCode)
    PinEntryEditText edPinCode;
    @BindView(R.id.btnSendCode)
    Button btnSendCode;
    @BindView(R.id.btnUpdatePassword)
    Button btnUpdatePassword;
    @BindView(R.id.lnResendMessage)
    LinearLayout lnResendMessage;
    @BindView(R.id.tvResend)
    TextView tvResend;
    @BindView(R.id.tvCountTime)
    TextView tvCountTime;
    @BindView(R.id.lnEnterPinCode)
    LinearLayout lnEnterPinCode;
    private ForgotPasswordPresenterImpl mForgotPasswordPresenter;
    private int mCode;
    private String mEmail;
    private CountDownTimer mCountDownTimeResend;
    private boolean mIsResend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);

        setTitle(R.string.reset_password);

        imgBack.setOnClickListener(this);
        tvResend.setOnClickListener(this);
        btnSendCode.setOnClickListener(this);
        btnUpdatePassword.setOnClickListener(this);

        btnSendCode.setEnabled(false);
        mCode = getIntent().getExtras().getInt(FConstants.KEY_PUT_CODE);
        mEmail = getIntent().getExtras().getString(FConstants.KEY_EMAIL);
//        showProgress();
        mForgotPasswordPresenter = new ForgotPasswordPresenterImpl(this);
        setUpCountDownTimer(120);

        edPinCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String pin = edPinCode.getText().toString();
                if (pin.length() == 4) {
                    btnSendCode.setEnabled(true);
                } else {
                    btnSendCode.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                finish();
                break;

            case R.id.tvResend:
                if (!mIsResend) {
//                    mForgotPasswordPresenter.sendMessage(mEmployee.getPhone());
                    mIsResend = true;
                } else {
                    DialogUtil.showDialogMessage(this, getResources().getString(R.string.maybe_the_phone_number_is_wrong));
                }
                break;

            case R.id.btnSendCode:
                if (edPinCode.getText().toString().equals(String.valueOf(mCode))) {
                    lnEnterPinCode.setVisibility(View.GONE);
                    lnFormNewPassword.setVisibility(View.VISIBLE);
                } else {
                    DialogUtil.showDialogMessage(this, getResources().getString(R.string.the_code_is_wrong));
                }
                break;

            case R.id.btnUpdatePassword:
                String newPassword = edNewPassword.getText().toString();
                String confirmNewPassword = edConfirmNewPassword.getText().toString();
                if (newPassword.equals("") || newPassword == null) {
                    ToastUtils.showToast(this, R.string.input_your_new_password);
                } else if (confirmNewPassword.equals("") || confirmNewPassword == null) {
                    ToastUtils.showToast(this, R.string.input_confirm_new_password);
                } else if (!newPassword.equals(confirmNewPassword)) {
                    ToastUtils.showToast(this, R.string.password_confirm_not_correct);
                } else {
                    showProgress();
                    mForgotPasswordPresenter.updatePassword(new ChangerPasswordForgotRep(mEmail,edConfirmNewPassword.getText().toString()));

                }
                break;
        }
    }

    @Override
    public void success(Object x) {
        super.success(x);
        hideProgress();
        if (x instanceof UserRes){
            DialogUtil.showDialogMessage(this, getResources().getString(R.string.update_password_successfully), R.string.ok, new DialogMessage.OnDialogMessageListener() {
                @Override
                public void yesClick() {
                    finish();
                }
            });

        }
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj error = (BaseObj) o;
        if (error.getStatusCode() == 400) {
            DialogUtil.showDialogMessage(this, error.getMessage());
        } else if (error.getStatusCode() == 404) {
            DialogUtil.showDialogMessage(this, getResources().getString(R.string.not_found));
        } else {
            DialogUtil.showDialogMessage(this, error.getMessage());
        }
    }

    private void setUpCountDownTimer(final int limitEnterCodeTime) {

        /*
         * limitEnterCodeTime return second, so we need to convert it to millisecond for countDownTimer
         */
        int limitEnterCodeSecond = (int) TimeUnit.SECONDS.toMillis(limitEnterCodeTime);
        mCountDownTimeResend = new CountDownTimer(limitEnterCodeSecond, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);

                int second = seconds % 60;
                int hour = seconds / 60;
                int minute = hour % 60;


                /**
                 * String.format("%02d", minute) to make minute type is mm not m. example: 09 not 9
                 * String.format("%02d", second) to make second type is ss not s. example: 09 not 9
                 * result is 09:09 not 9:9
                 */
                tvCountTime.setText(String.format(getResources().getString(R.string.resend_message_affter), String.format("%02d", minute) + ":"
                        + String.format("%02d", second)));

            }

            public void onFinish() {
                //time out.
                lnResendMessage.setVisibility(View.VISIBLE);
                tvCountTime.setVisibility(View.GONE);
            }

        }.start();
    }
}
