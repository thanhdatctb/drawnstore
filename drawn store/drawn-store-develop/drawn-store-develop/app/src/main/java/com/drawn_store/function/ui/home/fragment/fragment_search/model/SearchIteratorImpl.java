package com.drawn_store.function.ui.home.fragment.fragment_search.model;

import com.drawn_store.common.api.callback.MyCallback;
import com.drawn_store.common.base.BaseInteractorImpl;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.InteractorCallback;
import com.drawn_store.function.common.api.ApiUtils;
import com.drawn_store.function.common.api.request.LoginRequest;
import com.drawn_store.function.common.api.request.SearchRequest;
import com.drawn_store.function.common.api.request.UserRequest;
import com.drawn_store.function.common.objects.request.CheckEmailRep;
import com.drawn_store.function.common.objects.request.LoginReq;
import com.drawn_store.function.common.objects.request.LoginReqGoogle;
import com.drawn_store.function.common.objects.response.LoginRes;
import com.drawn_store.function.common.objects.response.SearchByImageRes;
import com.drawn_store.function.common.objects.response.SearchResObj;
import com.drawn_store.function.common.objects.response.UserRes;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchIteratorImpl extends BaseInteractorImpl<BaseObj, Object> implements ISearchIterator {

    private SearchRequest mSearchRequest;

    public SearchIteratorImpl(InteractorCallback callback) {
        super(callback);
        mSearchRequest = ApiUtils.getSearchRequest();
    }


    @Override
    public void search(MultipartBody.Part image, RequestBody customerId) {
        mSearchRequest.search(image, customerId).enqueue(new MyCallback<SearchResObj>() {
            @Override
            public void onSuccess(Call<SearchResObj> call, Response<SearchResObj> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<SearchResObj> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }
}
