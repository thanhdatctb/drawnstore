package com.drawn_store.function.ui.login.model;

import com.drawn_store.common.api.callback.MyCallback;
import com.drawn_store.common.base.BaseInteractorImpl;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.InteractorCallback;
import com.drawn_store.function.common.api.ApiUtils;
import com.drawn_store.function.common.api.request.LoginRequest;
import com.drawn_store.function.common.api.request.UserRequest;
import com.drawn_store.function.common.objects.request.ChangerPasswordRep;
import com.drawn_store.function.common.objects.request.CheckEmailRep;
import com.drawn_store.function.common.objects.request.LoginReq;
import com.drawn_store.function.common.objects.request.LoginReqGoogle;
import com.drawn_store.function.common.objects.response.LoginRes;
import com.drawn_store.function.common.objects.response.UpdateAccountRes;
import com.drawn_store.function.common.objects.response.UserCodeRes;
import com.drawn_store.function.common.objects.response.UserRes;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

public class LoginIteratorImpl extends BaseInteractorImpl<BaseObj, Object> implements ILoginIterator {

    private LoginRequest mLoginRequest;
    private UserRequest mUserRequest;

    public LoginIteratorImpl(InteractorCallback callback) {
        super(callback);
        mLoginRequest = ApiUtils.getLoginRequest();
        mUserRequest = ApiUtils.getUserRequest();
    }

    @Override
    public void login(LoginReq loginReq) {
        mLoginRequest.login(loginReq).enqueue(new MyCallback<LoginRes>() {
            @Override
            public void onSuccess(Call<LoginRes> call, Response<LoginRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<LoginRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void loginGoogle(LoginReqGoogle loginReq) {
        mLoginRequest.loginGoogle(loginReq).enqueue(new MyCallback<LoginRes>() {
            @Override
            public void onSuccess(Call<LoginRes> call, Response<LoginRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<LoginRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void checkEmail(CheckEmailRep email) {
        mUserRequest.checkEmail(email).enqueue(new MyCallback<UserCodeRes>() {
            @Override
            public void onSuccess(Call<UserCodeRes> call, Response<UserCodeRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<UserCodeRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void deleteAccount(int userId) {
        mUserRequest.deleteAccount(userId).enqueue(new MyCallback<UserRes>() {
            @Override
            public void onSuccess(Call<UserRes> call, Response<UserRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<UserRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void changerPassword(ChangerPasswordRep changerPasswordRep) {
        mUserRequest.changerPassword(changerPasswordRep).enqueue(new MyCallback<UserRes>() {
            @Override
            public void onSuccess(Call<UserRes> call, Response<UserRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<UserRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void updateAccount(RequestBody email, RequestBody name, MultipartBody.Part avatar) {
        mUserRequest.updateAccount(email, name, avatar).enqueue(new MyCallback<UpdateAccountRes>() {
            @Override
            public void onSuccess(Call<UpdateAccountRes> call, Response<UpdateAccountRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<UpdateAccountRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }
}
