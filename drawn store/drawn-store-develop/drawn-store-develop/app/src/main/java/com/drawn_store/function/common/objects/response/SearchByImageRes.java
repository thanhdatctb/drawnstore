package com.drawn_store.function.common.objects.response;

import com.drawn_store.function.common.objects.ProductsInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchByImageRes {

    @SerializedName("detected")
    @Expose
    private String detected;
    @SerializedName("colors")
    @Expose
    private List<String> colors = null;
    @SerializedName("genders")
    @Expose
    private List<Object> genders = null;
    @SerializedName("results")
    @Expose
    private List<ProductsInfo> results = null;

    public String getDetected() {
        return detected;
    }

    public void setDetected(String detected) {
        this.detected = detected;
    }

    public List<String> getColors() {
        return colors;
    }

    public void setColors(List<String> colors) {
        this.colors = colors;
    }

    public List<Object> getGenders() {
        return genders;
    }

    public void setGenders(List<Object> genders) {
        this.genders = genders;
    }

    public List<ProductsInfo> getResults() {
        return results;
    }

    public void setResults(List<ProductsInfo> results) {
        this.results = results;
    }

}