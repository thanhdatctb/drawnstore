package com.drawn_store.function.ui.home.fragment.fragment_home.presenter;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.presenter.InteractorCallback;
import com.drawn_store.function.common.objects.request.LikeRep;
import com.drawn_store.function.ui.home.fragment.fragment_home.model.ProductIteratorImpl;

public class ProductPresenterImpl implements ProductPresenter {
    private ProductIteratorImpl productIterator;

    public ProductPresenterImpl(BaseView<BaseObj, Object> mBaseView) {
        InteractorCallback<BaseObj, Object> mCallback = new InteractorCallback<BaseObj, Object>() {
            @Override
            public void success(BaseObj x) {
                mBaseView.success(x);
            }

            @Override
            public void error(Object o, boolean isShowDialog) {
                mBaseView.error(o, isShowDialog);
            }
        };
        productIterator = new ProductIteratorImpl(mCallback);
    }
    @Override
    public void productsSale(int userId,String limit) {
        productIterator.productsSale(userId,limit);
    }

    @Override
    public void productsNew(int userId,String limit) {
        productIterator.productsNew(userId,limit);
    }

    @Override
    public void productsHighestView(int userId,String limit) {
        productIterator.productsHighestView(userId,limit);
    }

    @Override
    public void productsType() {
        productIterator.productsType();
    }

    @Override
    public void productsAll(int userId) {
        productIterator.productsAll(userId);
    }

    @Override
    public void productsTypeLineId(int userId,int lineId) {
        productIterator.productsTypeLineId(userId,lineId);
    }

    @Override
    public void addView(int productId) {
        productIterator.addView(productId);
    }

    @Override
    public void addLike(LikeRep likeRep) {
        productIterator.addLike(likeRep);
    }

    @Override
    public void dislikeLike(LikeRep likeRep) {
        productIterator.dislikeLike(likeRep);
    }

    @Override
    public void showLike(int userId) {
        productIterator.showLike(userId);
    }

    @Override
    public void showRating(int productId) {
        productIterator.showRating(productId);
    }
}
