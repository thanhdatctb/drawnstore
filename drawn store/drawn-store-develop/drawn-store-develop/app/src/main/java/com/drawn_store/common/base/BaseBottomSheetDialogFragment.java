package com.drawn_store.common.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.fragment.app.Fragment;

import com.drawn_store.R;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.pref.PrefManager;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.common.view.MyProgressDialog;
import com.drawn_store.function.common.constants.FConstants;
import com.drawn_store.function.common.objects.response.LoginRes;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class BaseBottomSheetDialogFragment extends BottomSheetDialogFragment implements BaseView {

    protected MyProgressDialog progressDialog;
    public PrefManager prefManager;
    public LoginRes loginRes;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Context contextThemeWrapper = new ContextThemeWrapper(requireActivity(), getThemID());
        inflater.cloneInContext(contextThemeWrapper);
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefManager = new PrefManager(getContext());
        String personJsonString = prefManager.getString(FConstants.KEY_PUT_USERINFO);
        loginRes = Utils.getGsonParser().fromJson(personJsonString, LoginRes.class);

    }
    private int getThemID() {
        return R.style.ThemeOverlay_Demo_BottomSheetDialog;
    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new MyProgressDialog(getActivity());
        }
        if (progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void success(Object x) {

    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        BaseObj baseObj = (BaseObj) o;
        if (getActivity() != null && !getActivity().isFinishing()) {
//            DialogUtil.showDialogMessage(getActivity(), getString(R.string.announce), baseObj.getMessage());
        }
    }

    public void replaceFragment(Fragment fragment, boolean addToBackStack, boolean isAnim) {
//        getChildFragmentManager().executePendingTransactions();
        ((BaseActivity) getActivity()).replaceFragment(fragment,addToBackStack,isAnim);
    }
}
