package com.drawn_store.function.ui.home.fragment.fragment_home.model;

import com.drawn_store.common.api.callback.MyCallback;
import com.drawn_store.common.base.BaseInteractorImpl;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.InteractorCallback;
import com.drawn_store.function.common.api.ApiUtils;
import com.drawn_store.function.common.api.request.ProductsRequest;
import com.drawn_store.function.common.objects.request.LikeRep;
import com.drawn_store.function.common.objects.response.AddLikeRes;
import com.drawn_store.function.common.objects.response.DislikeLikeRes;
import com.drawn_store.function.common.objects.response.LikeRes;
import com.drawn_store.function.common.objects.response.ProductsAllRes;
import com.drawn_store.function.common.objects.response.ProductsHighestViewRes;
import com.drawn_store.function.common.objects.response.ProductsNewRes;
import com.drawn_store.function.common.objects.response.ProductsSaleRes;
import com.drawn_store.function.common.objects.response.ProductsTypeLineId;
import com.drawn_store.function.common.objects.response.ProductsTypeRes;
import com.drawn_store.function.common.objects.response.ProductsViewRes;
import com.drawn_store.function.common.objects.response.RatingRes;

import retrofit2.Call;
import retrofit2.Response;

public class ProductIteratorImpl extends BaseInteractorImpl<BaseObj, Object> implements ProductIterator {

    private ProductsRequest productsRequest;

    public ProductIteratorImpl(InteractorCallback callback) {
        super(callback);
        productsRequest = ApiUtils.getProductsRequest();
    }

    @Override
    public void productsSale(int userId, String limit) {
        productsRequest.getProductSale(userId, limit).enqueue(new MyCallback<ProductsSaleRes>() {
            @Override
            public void onSuccess(Call<ProductsSaleRes> call, Response<ProductsSaleRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<ProductsSaleRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void productsNew(int userId, String limit) {
        productsRequest.getProductNew(userId, limit).enqueue(new MyCallback<ProductsNewRes>() {
            @Override
            public void onSuccess(Call<ProductsNewRes> call, Response<ProductsNewRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<ProductsNewRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void productsHighestView(int userId, String limit) {
        productsRequest.getProducTHighestView(userId, limit).enqueue(new MyCallback<ProductsHighestViewRes>() {
            @Override
            public void onSuccess(Call<ProductsHighestViewRes> call, Response<ProductsHighestViewRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<ProductsHighestViewRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void productsType() {
        productsRequest.getProductType().enqueue(new MyCallback<ProductsTypeRes>() {
            @Override
            public void onSuccess(Call<ProductsTypeRes> call, Response<ProductsTypeRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<ProductsTypeRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void productsAll(int userId) {
        productsRequest.getProductAll(userId).enqueue(new MyCallback<ProductsAllRes>() {
            @Override
            public void onSuccess(Call<ProductsAllRes> call, Response<ProductsAllRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<ProductsAllRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void productsTypeLineId(int userId, int lineId) {
        productsRequest.getProductTypeLineId(userId, lineId).enqueue(new MyCallback<ProductsTypeLineId>() {
            @Override
            public void onSuccess(Call<ProductsTypeLineId> call, Response<ProductsTypeLineId> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<ProductsTypeLineId> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void addView(int productId) {
        productsRequest.addView(productId).enqueue(new MyCallback<ProductsViewRes>() {
            @Override
            public void onSuccess(Call<ProductsViewRes> call, Response<ProductsViewRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<ProductsViewRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void addLike(LikeRep likeRep) {
        productsRequest.addLike(likeRep).enqueue(new MyCallback<AddLikeRes>() {
            @Override
            public void onSuccess(Call<AddLikeRes> call, Response<AddLikeRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<AddLikeRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void dislikeLike(LikeRep likeRep) {
        productsRequest.dislikeLike(likeRep).enqueue(new MyCallback<DislikeLikeRes>() {
            @Override
            public void onSuccess(Call<DislikeLikeRes> call, Response<DislikeLikeRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<DislikeLikeRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void showLike(int userId) {
        productsRequest.showLike(userId).enqueue(new MyCallback<LikeRes>() {
            @Override
            public void onSuccess(Call<LikeRes> call, Response<LikeRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<LikeRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void showRating(int productId) {
        productsRequest.showRating(productId).enqueue(new MyCallback<RatingRes>() {
            @Override
            public void onSuccess(Call<RatingRes> call, Response<RatingRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<RatingRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }
}
