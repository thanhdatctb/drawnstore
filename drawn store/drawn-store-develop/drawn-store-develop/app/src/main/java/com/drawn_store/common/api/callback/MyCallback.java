package com.drawn_store.common.api.callback;

import com.google.gson.Gson;
import com.drawn_store.Apps;
import com.drawn_store.common.api.ErrorCode;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.ultility.Logger;
import com.drawn_store.common.ultility.NetworkUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class MyCallback<T extends BaseObj> implements Callback<T> {
    public static final String TAG = MyCallback.class.getSimpleName();

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (!response.isSuccessful() && response.errorBody() != null) {
            String errorMessage = null;
            try {
                Gson gson = new Gson();
                errorMessage = response.errorBody().string();
                BaseObj apiErrorObj = gson.fromJson(errorMessage, BaseObj.class);
                apiErrorObj.setStatusCode(response.code() );
                onError(call, apiErrorObj);
            } catch (IOException e) {
                BaseObj apiErrorObj = new BaseObj();
                apiErrorObj.setStatusCode(response.code() );
                onError(call, apiErrorObj);
                Logger.printStackTrace(e);
            } catch (Exception e) {
                BaseObj apiErrorObj = new BaseObj();
                apiErrorObj.setStatusCode(response.code());
                BaseObj.createError("server error", apiErrorObj.getStatusCode() + "=> " + errorMessage);
                onError(call, apiErrorObj);
                Logger.printStackTrace(e);
            }
            return;
        }

        T t = response.body();
        if (t == null) {
            //NO DATA
            BaseObj apiErrorObj = new BaseObj();
            apiErrorObj.setStatusCode(response.code() );
            BaseObj.createError("null", "No data");
            Logger.e(TAG, "Error4: ======> " + apiErrorObj.getStatusCode());
            onError(call, apiErrorObj);
            return;
        }
        onSuccess(call, response);
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        BaseObj apiErrorObj;
        if (!NetworkUtils.isConnect(Apps.getInstance())) {
            apiErrorObj = BaseObj.createError("", "No Connect");
            apiErrorObj.setStatusCode(ErrorCode.NO_INTERNET);
            onError(call, apiErrorObj);
        } else if (!call.isCanceled()){
            apiErrorObj = BaseObj.createError("", t.getMessage());
            onError(call, apiErrorObj);
        }
    }

    public abstract void onSuccess(Call<T> call, Response<T> response);

    public abstract void onError(Call<T> call, Object object);
}
