package com.drawn_store.function.ui.home;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.drawn_store.R;
import com.drawn_store.common.base.BaseActivity;
import com.drawn_store.function.ui.home.fragment.fragment_home.view.FragmentHome;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.fmContainer)
    FrameLayout fmContainer;
    FragmentHome fragmentHome = new FragmentHome();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        replaceFragment(fragmentHome,false,false);

    }


    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
