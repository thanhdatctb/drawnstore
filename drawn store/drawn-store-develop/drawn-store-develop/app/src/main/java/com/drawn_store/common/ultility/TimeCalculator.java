package com.drawn_store.common.ultility;

import android.util.Log;

import com.drawn_store.common.constants.AppConfig;

import java.util.Calendar;

public final class TimeCalculator {

    public static long GetTimeRemainRigister()
    {
        Calendar rightNow = Calendar.getInstance();
        int currentHourIn24Format = rightNow.get(Calendar.HOUR_OF_DAY);
        Calendar calendar = Calendar.getInstance();
        Log.d("Time", "hour remain: "+ GetHourRemain(currentHourIn24Format));

        calendar.set(rightNow.get(Calendar.YEAR),rightNow.get(Calendar.MONTH), rightNow.get(Calendar.DAY_OF_MONTH),
                rightNow.get(Calendar.HOUR_OF_DAY), 0, 0);
        calendar.add(Calendar.HOUR_OF_DAY, GetHourRemain(currentHourIn24Format)+1);

        return calendar.getTimeInMillis() - rightNow.getTimeInMillis();
    }

    private static int GetHourRemain(int hour)
    {
        int nextHour = 0;

        switch(GetTimeOfDay(hour))
        {
            case MORNING:
            {
                nextHour = AppConfig.s_morningTime;
            }break;
            case NOON:
            {
                nextHour = AppConfig.s_noonTime;
            }break;
            case AFTERNOON:
            {
                nextHour = AppConfig.s_afternoonTime;
            }break;
        }

        if(hour==0)
        {
            nextHour = 0;
        }
        return  nextHour - hour;
    }

    public static AppConfig.TimeOfDay GetTimeOfDay(int hour)
    {
        if(hour >= 0 && hour < AppConfig.s_morningTime)
        {
            return  AppConfig.TimeOfDay.MORNING;
        }
        else if( hour < AppConfig.s_noonTime)
        {
            return  AppConfig.TimeOfDay.NOON;
        }
        else
        {
            return  AppConfig.TimeOfDay.AFTERNOON;
        }
    }
}
