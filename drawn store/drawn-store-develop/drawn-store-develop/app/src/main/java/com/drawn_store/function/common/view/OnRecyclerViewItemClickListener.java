package com.drawn_store.function.common.view;

import android.view.View;

import com.drawn_store.function.common.objects.ProductsInfo;

public interface OnRecyclerViewItemClickListener {
    void recyclerViewListClicked(View v, int position);
    void recyclerViewListClickedData(View v, int position, ProductsInfo productsInfo);
}