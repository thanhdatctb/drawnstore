package com.drawn_store.function.ui.home.fragment.fragment_home.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.drawn_store.R;
import com.drawn_store.common.api.ErrorCode;
import com.drawn_store.common.base.BaseFragment;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.function.common.constants.FConstants;
import com.drawn_store.function.common.objects.ProductsInfo;
import com.drawn_store.function.common.objects.response.ProductsTypeLineId;
import com.drawn_store.function.common.view.BackReload;
import com.drawn_store.function.common.view.OnRecyclerViewItemClickListener;
import com.drawn_store.function.ui.home.fragment.fragment_cart.view.FragmentCart;
import com.drawn_store.function.ui.home.fragment.fragment_detail.FragmentDetailProduct;
import com.drawn_store.function.ui.home.fragment.fragment_home.presenter.ProductPresenterImpl;
import com.drawn_store.function.ui.home.fragment.fragment_home.view.adapter.ProductSeeMoreAdapter;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentProduct extends BaseFragment implements View.OnClickListener, OnRecyclerViewItemClickListener
        , BackReload {

    public FragmentProduct newInstance(String title, int lineId) {
        Bundle args = new Bundle();
        args.putString(FConstants.KEY_IS_TITLE, title);
        args.putInt(FConstants.KEY_LINE_ID, lineId);
        FragmentProduct fragment = new FragmentProduct();
        fragment.setArguments(args);
        return fragment;
    }

    private ProductPresenterImpl productPresenter;
    private Bundle args;
    private ProductSeeMoreAdapter productSeeMoreAdapter;
    private List<ProductsInfo> productsInfoList;
    private int lineId;
    LinearLayoutManager linearLayoutManager;
    @BindView(R.id.rvListProductID)
    RecyclerView rvListProductID;
    @BindView(R.id.lnSortPrice)
    LinearLayout lnSortPrice;
    @BindView(R.id.lnSortDate)
    LinearLayout lnSortDate;
    @BindView(R.id.lnResult)
    LinearLayout lnResult;
    @BindView(R.id.lnSortSee)
    LinearLayout lnSortSee;
    @BindView(R.id.lnSearch)
    LinearLayout lnSearch;
    @BindView(R.id.lnSort)
    LinearLayout lnSort;
    @BindView(R.id.imgSortPrice)
    ImageView imgSortPrice;
    @BindView(R.id.imgSortViewSee)
    ImageView imgSortViewSee;
    @BindView(R.id.imgSortDate)
    ImageView imgSortDate;
    @BindView(R.id.imgSearch)
    ImageView imgSearch;
    @BindView(R.id.searchView)
    MaterialSearchView searchView;
    @BindView(R.id.tvNotFound)
    TextView tvNotFound;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    private boolean checkClick = true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        isHideButtonBack(false);
        backgroudToolbar(false);
        setIconToolBar();
        args = getArguments();
        setTitle(args.getString(FConstants.KEY_IS_TITLE));
        lineId = args.getInt(FConstants.KEY_LINE_ID);
        productPresenter = new ProductPresenterImpl(this);
        productPresenter.productsTypeLineId(loginRes.getData().getId(), lineId);
        productsInfoList = new ArrayList<>();
        showProgress();
        loadSwipe();
        setClick();
    }

    private void setIconToolBar() {
        if (mToolbar != null) {
            mToolbar.getMenu().clear();
            mToolbar.inflateMenu(R.menu.menu_cart_product);
            mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.itemMenuCart:
                            replaceFragment(new FragmentCart().newInstance(), true, false);
                            break;
                    }
                    return true;
                }
            });
        }

    }

    @Override
    public void success(Object x) {
        super.success(x);
        hideProgress();
        if (x instanceof ProductsTypeLineId) {
            ProductsTypeLineId productsTypeLineId = (ProductsTypeLineId) x;
            productsInfoList = productsTypeLineId.getData();
            swipeRefresh.setRefreshing(false);
            if (productsInfoList.size() == 0) {
                tvNotFound.setVisibility(View.VISIBLE);
                rvListProductID.setVisibility(View.GONE);
            } else {
                tvNotFound.setVisibility(View.GONE);
                rvListProductID.setVisibility(View.VISIBLE);
            }
        }
        getDataProductLoad();
        productSeeMoreAdapter.swap(productsInfoList);
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj err = (BaseObj) o;
        if (err.getStatusCode() == ErrorCode.BAD_REQUEST) {
            DialogUtil.showDialogMessage(requireActivity(), getResources().getString(R.string.data_null));
        } else {
            DialogUtil.showDialogMessage(requireActivity(), err.getMessage() + " (" + err.getStatusCode() + ")");
        }
        swipeRefresh.setRefreshing(false);
    }

    private void loadSwipe() {
        swipeRefresh.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorRed,
                R.color.colorBlack);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                productPresenter.productsTypeLineId(loginRes.getData().getId(), lineId);
            }
        });
    }

    private void getDataProductLoad() {
        productSeeMoreAdapter = new ProductSeeMoreAdapter(requireContext(), false);
        productSeeMoreAdapter.setOnClickListener(this);
        linearLayoutManager = new LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false);
        rvListProductID.setLayoutManager(linearLayoutManager);
        rvListProductID.setItemAnimator(new DefaultItemAnimator());
        rvListProductID.setAdapter(productSeeMoreAdapter);
    }

    private void setClick() {
        searchData();
        lnSortPrice.setOnClickListener(this);
        lnSortDate.setOnClickListener(this);
        lnResult.setOnClickListener(this);
        lnSortSee.setOnClickListener(this);
        imgSearch.setOnClickListener(this);

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                lnSort.setVisibility(View.GONE);
                lnSearch.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSearchViewClosed() {
                lnSort.setVisibility(View.VISIBLE);
                lnSearch.setVisibility(View.GONE);
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lnResult:
                setSelectTab(FConstants.TAB_RESULT);
                dateSortAscending();
                break;
            case R.id.lnSortPrice:
                setSelectTab(FConstants.TAB_PRICE);
                if (checkClick) {
                    checkClick = false;
                    priceSortAscending();
                    imgSortPrice.setScaleY(-1);
                } else {
                    checkClick = true;
                    priceSortDescending();
                    imgSortPrice.setScaleY(1);
                }
                break;
            case R.id.lnSortDate:
                setSelectTab(FConstants.TAB_DATE);
                if (checkClick) {
                    checkClick = false;
                    dateSortAscending();
                    imgSortDate.setScaleY(-1);
                } else {
                    checkClick = true;
                    dateSortDescending();
                    imgSortDate.setScaleY(1);
                }
                break;
            case R.id.lnSortSee:
                setSelectTab(FConstants.TAB_SEE);
                if (checkClick) {
                    checkClick = false;
                    seeSortAscending();
                    imgSortViewSee.setScaleY(-1);
                } else {
                    checkClick = true;
                    seeSortDescending();
                    imgSortViewSee.setScaleY(1);
                }
                break;
            case R.id.imgSearch:
                searchView.showSearch();
                break;


        }
    }

    private void searchData() {
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText != null && !newText.isEmpty()) {
                    List<ProductsInfo> listSearch = new ArrayList<>();
                    for (ProductsInfo item : productsInfoList) {
                        if (item.getName().toLowerCase(Locale.getDefault())
                                .contains(newText.toLowerCase(Locale.getDefault()))
                                || item.getDescription().toLowerCase(Locale.getDefault())
                                .contains(newText.toLowerCase(Locale.getDefault()))) {
                            listSearch.add(item);
                        }
                    }
                    productSeeMoreAdapter.swap(listSearch);
                } else {
                    productSeeMoreAdapter.swap(productsInfoList);
                }
                return true;
            }
        });
    }

    private void priceSortAscending() {
        Collections.sort(productsInfoList, new Comparator<ProductsInfo>() {
            @Override
            public int compare(ProductsInfo productsInfo, ProductsInfo t1) {
                if (Double.parseDouble(productsInfo.getDiscountPrice()) < Double.parseDouble(t1.getDiscountPrice()))
                    return 1;
                else {
                    if (Double.parseDouble(productsInfo.getDiscountPrice()) == Double.parseDouble(t1.getDiscountPrice()))
                        return 0;
                    else
                        return -1;

                }
            }
        });
        productSeeMoreAdapter.swap(productsInfoList);
    }

    private void priceSortDescending() {
        Collections.sort(productsInfoList, new Comparator<ProductsInfo>() {
            @Override
            public int compare(ProductsInfo productsInfo, ProductsInfo t1) {
                if (Double.parseDouble(productsInfo.getPrice()) < Double.parseDouble(t1.getPrice()))
                    return -1;
                else {
                    if (Double.parseDouble(productsInfo.getPrice()) == Double.parseDouble(t1.getPrice()))
                        return 0;
                    else
                        return 1;

                }
            }
        });
        productSeeMoreAdapter.swap(productsInfoList);
    }

    private void seeSortAscending() {
        Collections.sort(productsInfoList, new Comparator<ProductsInfo>() {
            @Override
            public int compare(ProductsInfo productsInfo, ProductsInfo t1) {
                if ((double) productsInfo.getTotalView() < (double) t1.getTotalView())
                    return 1;
                else {
                    if ((double) productsInfo.getTotalView() == (double) t1.getTotalView())
                        return 0;
                    else
                        return -1;

                }
            }
        });
        productSeeMoreAdapter.swap(productsInfoList);

    }

    private void seeSortDescending() {
        Collections.sort(productsInfoList, new Comparator<ProductsInfo>() {
            @Override
            public int compare(ProductsInfo productsInfo, ProductsInfo t1) {
                if ((double) productsInfo.getTotalView() < (double) t1.getTotalView())
                    return -1;
                else {
                    if ((double) productsInfo.getTotalView() == (double) t1.getTotalView())
                        return 0;
                    else
                        return 1;

                }
            }
        });
        productSeeMoreAdapter.swap(productsInfoList);
    }

    private void dateSortAscending() {
        Collections.sort(productsInfoList, new Comparator<ProductsInfo>() {
            @Override
            public int compare(ProductsInfo productsInfo, ProductsInfo t1) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", new Locale("vi", "VN"));
                try {
                    return Objects.requireNonNull(dateFormat.parse(t1.getCreatedAt())).compareTo(dateFormat.parse(productsInfo.getCreatedAt()));
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        });
        productSeeMoreAdapter.swap(productsInfoList);
    }

    private void dateSortDescending() {
        Collections.sort(productsInfoList, new Comparator<ProductsInfo>() {
            @Override
            public int compare(ProductsInfo productsInfo, ProductsInfo t1) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
                try {
                    return Objects.requireNonNull(dateFormat.parse(productsInfo.getCreatedAt())).compareTo(dateFormat.parse(t1.getCreatedAt()));
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        });
        productSeeMoreAdapter.swap(productsInfoList);
    }


    private void setSelectTab(int tab) {
        switch (tab) {
            case FConstants.TAB_RESULT:
                lnResult.setBackgroundResource(R.drawable.bg_button);
                lnSortSee.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorBlack));
                lnSortPrice.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorBlack));
                lnSortDate.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorBlack));
                break;
            case FConstants.TAB_PRICE:
                lnSortPrice.setBackgroundResource(R.drawable.bg_button);
                lnSortSee.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorBlack));
                lnResult.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorBlack));
                lnSortDate.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorBlack));
                break;
            case FConstants.TAB_SEE:
                lnSortSee.setBackgroundResource(R.drawable.bg_button);
                lnSortDate.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorBlack));
                lnResult.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorBlack));
                lnSortPrice.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorBlack));
                break;
            case FConstants.TAB_DATE:
                lnSortDate.setBackgroundResource(R.drawable.bg_button);
                lnSortSee.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorBlack));
                lnResult.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorBlack));
                lnSortPrice.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorBlack));
                break;

        }
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        //
    }

    @Override
    public void recyclerViewListClickedData(View v, int position, ProductsInfo productsInfo) {
        productPresenter.addView(productsInfo.getId());
        replaceFragment(new FragmentDetailProduct().newInstance(productsInfo).setReloadFragment(this), true, false);
    }

    @Override
    public void reloadFragment() {
        productPresenter.productsTypeLineId(loginRes.getData().getId(), lineId);
    }
}
