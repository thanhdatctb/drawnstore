package com.drawn_store.function.ui.login.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.drawn_store.R;
import com.drawn_store.common.base.BaseActivity;
import com.drawn_store.common.constants.AppConfig;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.pref.PrefManager;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.common.view.ToastUtils;
import com.drawn_store.function.common.constants.FConstants;
import com.drawn_store.function.common.objects.UserInfo;
import com.drawn_store.function.common.objects.request.CheckEmailRep;
import com.drawn_store.function.common.objects.request.LoginReq;
import com.drawn_store.function.common.objects.request.LoginReqGoogle;
import com.drawn_store.function.common.objects.response.LoginRes;
import com.drawn_store.function.common.objects.response.UserCodeRes;
import com.drawn_store.function.common.objects.response.UserRes;
import com.drawn_store.function.ui.forgot_password.view.ForgotPasswordActivity;
import com.drawn_store.function.ui.home.MainActivity;
import com.drawn_store.function.ui.login.presenter.LoginPresenterImpl;
import com.drawn_store.function.ui.register.view.RegisterActivity;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements View.OnClickListener, BaseView {


    @BindView(R.id.btn_back)
    ImageButton imgBack;
    @BindView(R.id.edEmail)
    EditText edEmail;
    @BindView(R.id.edPassword)
    EditText edPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.imgRegistration)
    ImageView imgRegistration;
    @BindView(R.id.tvForgotPassword)
    TextView tvForgotPassword;
    @BindView(R.id.imgGooglePlus)
    ImageView imgGooglePlus;
    private LoginPresenterImpl mLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setTitle(R.string.login);
        isHideButtonBack(true);
        backgroudToolbar(false);
        imgRegistration.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        imgGooglePlus.setOnClickListener(this);
        mLoginPresenter = new LoginPresenterImpl(this);

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken("11106347802-lfdagui7sebpprjhv0sd5oldtjtvkhr8.apps.googleusercontent.com")
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (edEmail.getText().toString().equals("")) {
                    ToastUtils.showToast(this, getResources().getString(R.string.input_your_email), Gravity.BOTTOM);
                } else if (edPassword.getText().toString().equals("")) {
                    ToastUtils.showToast(this, getResources().getString(R.string.input_your_password), Gravity.BOTTOM);
                } else {
                    LoginReq loginReq = new LoginReq();
                    loginReq.setmEmail(edEmail.getText().toString().trim());
                    loginReq.setPassword(edPassword.getText().toString().trim());
                    showProgress();
                    mLoginPresenter.login(loginReq);
                }
                break;

            case R.id.imgRegistration:
                navigator.screenRegistration(LoginActivity.this);
                break;
            case R.id.imgGooglePlus:
                signInGoogle();
                break;

            case R.id.tvForgotPassword:
                String email = edEmail.getText().toString();
                if (email.equals("") || email == null) {
                    ToastUtils.showToast(this, getResources().getString(R.string.input_your_email), Gravity.BOTTOM);
                } else {
                    showProgress();
                    mLoginPresenter.checkEmail(new CheckEmailRep(email));
                }
                break;
        }
    }

    @Override
    public void success(Object x) {
        super.success(x);
        if (x instanceof LoginRes) {
            LoginRes loginRes = (LoginRes) x;
            String personJsonString = Utils.getGsonParser().toJson(loginRes);
            prefManager.saveSetting(FConstants.KEY_EMAIL, loginRes.getData().getEmail());
            prefManager.saveSetting(FConstants.KEY_PUT_USERINFO, personJsonString);
            hideProgress();
            navigator.screenMain(LoginActivity.this);
            finish();
        } else if (x instanceof UserCodeRes) {
            UserCodeRes userCodeRes = (UserCodeRes) x;
            hideProgress();
            Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
            intent.putExtra(FConstants.KEY_PUT_CODE, userCodeRes.getData().getCode());
            intent.putExtra(FConstants.KEY_EMAIL, edEmail.getText().toString());
            startActivity(intent);
        }
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj err = (BaseObj) o;
        if (err.getStatusCode() == 400) {
            DialogUtil.showDialogMessage(this, getResources().getString(R.string.username_or_passowrd_incorrect));
        } else if (err.getStatusCode() == 404) {
            DialogUtil.showDialogMessage(this, getResources().getString(R.string.username_does_not_exist));
        } else {
            DialogUtil.showDialogMessage(this, err.getMessage());
        }
    }

    private void signInGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, AppConfig.RC_SIGN_IN);
    }

    //Client ID: 11106347802-lfdagui7sebpprjhv0sd5oldtjtvkhr8.apps.googleusercontent.com

    //Client Secret: ghPRN7WTYIx6AzzhboC5p0t4

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConfig.RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            useLoginInformationGoogle(task);
        }
    }


    private void useLoginInformationGoogle(Task<GoogleSignInAccount> task) {
        signRevokeAccessGoogle();
        try {
            // Google Sign In was successful, authenticate with Firebase
            GoogleSignInAccount account = task.getResult(ApiException.class);
            Log.d("TAG", "firebaseAuthWithGoogle:" + account.getId());
            LoginReqGoogle loginReqGoogle = new LoginReqGoogle();
            loginReqGoogle.setName(account.getDisplayName());
            loginReqGoogle.setEmail(account.getEmail());
            loginReqGoogle.setPassword("");
            loginReqGoogle.setAvatar(account.getPhotoUrl().toString());
            mLoginPresenter.loginGoogle(loginReqGoogle);
            signOutGoogle();
        } catch (ApiException e) {
            Log.w("TAG", "Google sign in failed", e);
            ToastUtils.showToast(this, String.valueOf(e.getStatusCode()), Gravity.BOTTOM);
        }
    }

}
