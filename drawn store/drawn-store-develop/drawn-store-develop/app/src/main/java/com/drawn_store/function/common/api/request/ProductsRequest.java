package com.drawn_store.function.common.api.request;

import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.objects.request.LikeRep;
import com.drawn_store.function.common.objects.response.AddLikeRes;
import com.drawn_store.function.common.objects.response.DislikeLikeRes;
import com.drawn_store.function.common.objects.response.LikeRes;
import com.drawn_store.function.common.objects.response.ProductsAllRes;
import com.drawn_store.function.common.objects.response.ProductsHighestViewRes;
import com.drawn_store.function.common.objects.response.ProductsNewRes;
import com.drawn_store.function.common.objects.response.ProductsSaleRes;
import com.drawn_store.function.common.objects.response.ProductsTypeLineId;
import com.drawn_store.function.common.objects.response.ProductsTypeRes;
import com.drawn_store.function.common.objects.response.ProductsViewRes;
import com.drawn_store.function.common.objects.response.RatingRes;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ProductsRequest {
    @GET(ConstantApi.URL_PRODUCT_SALE)
    Call<ProductsSaleRes> getProductSale(@Path("userId") int userId
            ,@Path("limit") String limit);

    @GET(ConstantApi.URL_PRODUCT_NEW)
    Call<ProductsNewRes> getProductNew(@Path("userId") int userId
            ,@Path("limit") String limit);

    @GET(ConstantApi.URL_PRODUCT_HIGHESTVIEW)
    Call<ProductsHighestViewRes> getProducTHighestView(@Path("userId") int userId
            ,@Path("limit") String limit);

    @GET(ConstantApi.URL_PRODUCT_TYPE)
    Call<ProductsTypeRes> getProductType();


    @GET(ConstantApi.URL_PRODUCT_ALL)
    Call<ProductsAllRes> getProductAll(@Path("userId") int userId);

    @GET(ConstantApi.URL_PRODUCT_LINEID)
    Call<ProductsTypeLineId> getProductTypeLineId(@Path("userId") int userId
            ,@Path("productLineId") int productLineId);


    @PUT(ConstantApi.URL_ADD_VIEW)
    Call<ProductsViewRes> addView(@Path("productId") int productId);

    @POST(ConstantApi.URL_LIKE)
    Call<AddLikeRes> addLike(@Body LikeRep likeRep);

    @GET(ConstantApi.URL_LIKE_SHOW)
    Call<LikeRes> showLike(@Path("userId") int userId);

    @HTTP(method = "DELETE", path = ConstantApi.URL_LIKE, hasBody = true)
    Call<DislikeLikeRes> dislikeLike(@Body LikeRep likeRep);

    @GET(ConstantApi.URL_SHOW_RATING)
    Call<RatingRes> showRating(@Path("productId") int productId);
}
