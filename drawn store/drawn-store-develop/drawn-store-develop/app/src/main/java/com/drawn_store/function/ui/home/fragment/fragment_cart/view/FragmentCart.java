package com.drawn_store.function.ui.home.fragment.fragment_cart.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.drawn_store.R;
import com.drawn_store.common.base.BaseFragment;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.other.swipe.lib.Mode;
import com.drawn_store.common.ultility.DialogUtil;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.common.view.ToastUtils;
import com.drawn_store.function.common.objects.CartInfo;
import com.drawn_store.function.common.objects.PaymentOrderInfo;
import com.drawn_store.function.common.objects.request.UpdateCartRep;
import com.drawn_store.function.common.objects.response.CartDeleteRes;
import com.drawn_store.function.common.objects.response.CartRes;
import com.drawn_store.function.common.view.BackReload;
import com.drawn_store.function.ui.home.fragment.fragment_cart.presenter.OrdelPresenterImpl;
import com.drawn_store.function.ui.home.fragment.fragment_payment.view.FragmentPayment;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class FragmentCart extends BaseFragment implements CartAdapter.PriceCheck, BackReload {


    public FragmentCart newInstance() {
        return this;
    }

    @BindView(R.id.rvListCart)
    RecyclerView rvListCart;
    @BindView(R.id.tvTotal)
    TextView tvTotal;
    //    @BindView(R.id.checkAll)
//    CheckBox checkAll;
    @BindView(R.id.btnPayment)
    MaterialButton btnPayment;
    private OrdelPresenterImpl ordelPresenter;
    private CartAdapter cartAdapter;
    private double total = 0.0;
    private List<PaymentOrderInfo> paymentOrderInfoList;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cart, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isHideButtonBack(false);
        backgroudToolbar(true);
        setTitle(R.string.cart);
        paymentOrderInfoList = new ArrayList<>();
        ordelPresenter = new OrdelPresenterImpl(this);
        ordelPresenter.getCart(loginRes.getData().getId());
        tvTotal.setText(Utils.formatAmount(total));
        tvTotal.addTextChangedListener(textAmount);
        showProgress();
    }

    @Override
    public void success(Object x) {
        super.success(x);
        hideProgress();
        if (x instanceof CartRes) {
            CartRes cartRes = (CartRes) x;
            showListCart();
            cartAdapter.swap(cartRes.getData());
        } else if (x instanceof CartDeleteRes) {
            ToastUtils.showToast(requireContext(), getResources().getString(R.string.delete_success), Gravity.BOTTOM);
        }
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
        BaseObj err = (BaseObj) o;
        DialogUtil.showDialogMessage(requireContext(), err.getMessage());
    }

    private void showListCart() {
        cartAdapter = new CartAdapter(requireContext());
        rvListCart.setLayoutManager(new LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false));
        cartAdapter.setMode(Mode.Single);
        cartAdapter.setPriceCheck(this);
        rvListCart.setAdapter(cartAdapter);
        rvListCart.setOnScrollListener(onScrollListener);
    }

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            Log.e("ListView", "onScrollStateChanged");
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            // Could hide open views here if you wanted. //
        }
    };

    @Override
    public void getPriceCheck(int position, double price, boolean checkClick) {
        if (checkClick) {
            price = total + price;
            total = price;
            tvTotal.setText(Utils.formatAmount(price));
        } else {
            price = total - price;
            total = price;
            tvTotal.setText(Utils.formatAmount(price));

        }
    }

    @Override
    public void getDissAll(int position, boolean checkClick, double price) {
        total = total - total;
        total = price;
        tvTotal.setText(Utils.formatAmount(total));
    }

    @Override
    public void getCheckAdd(int position, boolean checkClick, List<PaymentOrderInfo> paymentOrderInfos) {
        this.paymentOrderInfoList = paymentOrderInfos;
        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(new FragmentPayment().newInstance(paymentOrderInfos), true, false);
            }
        });
    }

    @Override
    public void clickDeleteItem(View view, int position, int cartId) {
        showProgress();
        ordelPresenter.deleteCart(cartId);
        cartAdapter.removeItem(position);
    }

    @Override
    public void clickPlus(View view, int cartId, int amount) {
        showProgress();
        ordelPresenter.updateCart(new UpdateCartRep(cartId, amount));
        ordelPresenter.getCart(loginRes.getData().getId());
    }

    private void showCheckButton() {

    }

    private TextWatcher textAmount = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
//            String total = tvTotal.getText().toString();
            if (total == 0) {
                btnPayment.setEnabled(false);
            } else {
                btnPayment.setEnabled(true);
            }
        }
    };

    @Override
    public void reloadFragment() {
        ordelPresenter.getCart(loginRes.getData().getId());
    }

    @Override
    public void recyclerViewListClickedData(View v, int position, CartInfo cartInfo) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        paymentOrderInfoList.clear();
    }
}
