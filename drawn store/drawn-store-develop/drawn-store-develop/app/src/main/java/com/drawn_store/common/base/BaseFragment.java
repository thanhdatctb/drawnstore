package com.drawn_store.common.base;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.TouchDelegate;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.drawn_store.R;
import com.drawn_store.common.constants.Constants;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.pref.PrefManager;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.common.view.MyProgressDialog;
import com.drawn_store.function.common.constants.FConstants;
import com.drawn_store.function.common.navigator.Navigator;
import com.drawn_store.function.common.objects.response.LoginRes;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseFragment extends Fragment implements Constants, BaseView {
    private static final String TAG = BaseFragment.class.getSimpleName();

    @Nullable
    @BindView(R.id.toolbar_title)
    TextView mTvTitle;
    @Nullable
    @BindView(R.id.btn_back)
    ImageView imgBack;
    @Nullable
//    @BindView(R.id.imgIconToolbar)
//    ImageButton imgIconToolbar;
    /**
     * progress bar show when request api
     */
    protected MyProgressDialog progressDialog;
    private OnBackToolbarClickListener mOnBackToolbarClickListener;
    protected Toolbar mToolbar;
    private OnBackPressListener mOnBackPressListener;
    public PrefManager prefManager;
    // flag to define screen: contact, potential, customer
    protected String mTypeScreen;
    protected int layoutResID;
    public Navigator navigator = new Navigator();
    public LoginRes loginRes;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTypeScreen = getArguments().getString(Constants.KEY_SCREEN);
        }
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    public interface OnBackPressListener {
        boolean onBackPressed();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, getView());
        prefManager = new PrefManager(getContext());
        String personJsonString = prefManager.getString(FConstants.KEY_PUT_USERINFO);
        loginRes = Utils.getGsonParser().fromJson(personJsonString, LoginRes.class);
        this.mToolbar = view.findViewById(R.id.toolbar);
        if (imgBack != null) {
            final View parent = (View) imgBack.getParent();  // button: the view you want to enlarge hit area
            parent.post(new Runnable() {
                public void run() {
                    final Rect rect = new Rect();
                    imgBack.getHitRect(rect);
                    rect.top -= 30;    // increase top hit area
                    rect.left -= 30;   // increase left hit area
                    rect.bottom += 30; // increase bottom hit area
                    rect.right += 30;  // increase right hit area
                    parent.setTouchDelegate(new TouchDelegate(rect, imgBack));
                }
            });
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnBackPressListener != null) {
                        mOnBackPressListener.onBackPressed();
                    } else {
                        getActivity().onBackPressed();
                    }
                }
            });
        }
    }

    public void replaceFragment(Fragment fragment, boolean addToBackStack, boolean isAnim) {
//        getChildFragmentManager().executePendingTransactions();
        ((BaseActivity) getActivity()).replaceFragment(fragment,addToBackStack,isAnim);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new MyProgressDialog(getActivity());
        }
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void success(Object x) {

    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        BaseObj baseObj = (BaseObj) o;
        if (getActivity() != null && !getActivity().isFinishing()) {
//            DialogUtil.showDialogMessage(getActivity(), getString(R.string.announce), baseObj.getMessage());
        }
    }

    public void isHideButtonBack(boolean bool) {
        if (bool) {
            imgBack.setVisibility(View.GONE);
        }
    }

    public void setTitle(@StringRes int resTitle) {
        if (mTvTitle != null) {
            mTvTitle.setText(resTitle);
        }
    }

    public void setTitle(CharSequence title) {
        if (mTvTitle != null) {
            mTvTitle.setText(title);
        }
    }

    protected void showHideButtonBack(boolean isShow) {
        if (imgBack != null) {
            imgBack.setVisibility(isShow ? View.VISIBLE : View.GONE);
        }
    }

    protected void setBackgroundToolbar(int resColor) {
        if (mToolbar != null) {
            mToolbar.setBackgroundColor(resColor);
        }
    }

    protected void hideToolbar() {
        if (this.mToolbar != null) {
            this.mToolbar.setVisibility(View.GONE);
        }
    }

    protected void showToolbar() {
        if (this.mToolbar != null) {
            this.mToolbar.setVisibility(View.VISIBLE);
        }
    }

    public void hideKeyboard(View view) {
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        hideProgress();
    }

    public void backgroudToolbar(boolean white) {
        if (white) {
            mToolbar.setBackground(new ColorDrawable(ContextCompat.getColor(requireActivity(), R.color.colorWhite)));
            if (mTvTitle != null) {
                mTvTitle.setTextColor(ContextCompat.getColor(requireActivity(), R.color.colorBlack));
                imgBack.setImageResource(R.drawable.ic_back_black);
            }
        } else {
            mToolbar.setBackground(ContextCompat.getDrawable(requireActivity(), R.drawable.bg_button));
            if (mTvTitle != null) {
                mTvTitle.setTextColor(ContextCompat.getColor(requireActivity(), R.color.colorWhite));
                imgBack.setImageResource(R.drawable.ic_back);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        prefManager = new PrefManager(getContext());
        String personJsonString = prefManager.getString(FConstants.KEY_PUT_USERINFO);
        loginRes = Utils.getGsonParser().fromJson(personJsonString, LoginRes.class);
    }
}