package com.drawn_store.function.ui.login.presenter;

import com.drawn_store.function.common.objects.request.ChangerPasswordRep;
import com.drawn_store.function.common.objects.request.CheckEmailRep;
import com.drawn_store.function.common.objects.request.LoginReq;
import com.drawn_store.function.common.objects.request.LoginReqGoogle;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface ILoginPresenter {
    void login(LoginReq loginReq);
    void loginGoogle(LoginReqGoogle loginReq);
    void checkEmail(CheckEmailRep email);
    void deleteAccount(int userId);
    void changerPassword(ChangerPasswordRep changerPasswordRep);
    void updateAccount(RequestBody email, RequestBody name, MultipartBody.Part avatar);
}
