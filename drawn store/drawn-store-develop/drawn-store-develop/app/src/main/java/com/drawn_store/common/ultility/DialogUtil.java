package com.drawn_store.common.ultility;

import android.content.Context;

import androidx.annotation.NonNull;

import com.drawn_store.R;
import com.drawn_store.common.view.DialogMessage;


public class DialogUtil {
    public static void showDialogMessage(@NonNull Context context, int title, int message) {
        DialogMessage dialogMessage = new DialogMessage(context, context.getString(title),
                context.getString(message));
        dialogMessage.show();
    }

    public static void showDialogMessage(@NonNull Context context, int message) {
        DialogMessage dialogMessage = new DialogMessage(context, context.getString(R.string.announce),
                context.getString(message));
        dialogMessage.show();
    }

    public static void showDialogMessage(@NonNull Context context, String message) {
        DialogMessage dialogMessage = new DialogMessage(context, context.getString(R.string.announce),
                message);
        dialogMessage.show();
    }

    public static void showDialogMessage(@NonNull Context context, String message, boolean isAutoHide) {
        DialogMessage dialogMessage = new DialogMessage(context, context.getString(R.string.announce),
                message, isAutoHide);
        dialogMessage.show();
    }

    public static void showDialogMessage(@NonNull Context context, String title, String message) {
        DialogMessage dialogMessage = new DialogMessage(context, title, message);
        dialogMessage.show();
    }

    public static void showDialogMessage(@NonNull Context context, int title, int message,
                                         int textNo, int textYes,
                                         DialogMessage.OnDialogMessageListener listener) {
        DialogMessage dialogMessage = new DialogMessage(context, context.getString(title),
                context.getString(message),
                context.getString(textNo),
                context.getString(textYes),
                listener);
        dialogMessage.show();
    }

    public static void showDialogMessage(@NonNull Context context, int title, String message,
                                         int textNo, int textYes,
                                         DialogMessage.OnDialogMessageListener listener) {
        DialogMessage dialogMessage = new DialogMessage(context, context.getString(title),
                message,
                context.getString(textNo),
                context.getString(textYes),
                listener);
        dialogMessage.show();
    }

    public static void showDialogMessage(@NonNull Context context, String message, int textYes,
                                         DialogMessage.OnDialogMessageListener listener) {
        DialogMessage dialogMessage = new DialogMessage(context, message,
                context.getString(textYes),
                listener);
        dialogMessage.show();
    }

    public static DialogMessage showDialogMessage(@NonNull Context context, String message, int textYes,
                                                  DialogMessage.OnDialogMessageListener listener, DialogMessage.OnDismissListener dismissListener) {
        DialogMessage dialogMessage = new DialogMessage(context, message,
                context.getString(textYes),
                listener, dismissListener);
        dialogMessage.setOnDismissListener(dismissListener);
        dialogMessage.show();
        return dialogMessage;
    }

}
