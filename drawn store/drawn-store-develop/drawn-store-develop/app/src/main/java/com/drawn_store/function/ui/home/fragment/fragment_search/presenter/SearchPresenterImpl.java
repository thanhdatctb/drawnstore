package com.drawn_store.function.ui.home.fragment.fragment_search.presenter;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.common.presenter.InteractorCallback;
import com.drawn_store.function.common.objects.request.CheckEmailRep;
import com.drawn_store.function.common.objects.request.LoginReq;
import com.drawn_store.function.common.objects.request.LoginReqGoogle;
import com.drawn_store.function.ui.home.fragment.fragment_search.model.SearchIteratorImpl;
import com.drawn_store.function.ui.login.model.LoginIteratorImpl;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SearchPresenterImpl implements ISearchPresenter {

    private SearchIteratorImpl mSearchIterator;

    public SearchPresenterImpl(BaseView<BaseObj, Object> mBaseView) {
        InteractorCallback<BaseObj, Object> mCallback = new InteractorCallback<BaseObj, Object>() {
            @Override
            public void success(BaseObj x) {
                mBaseView.success(x);
            }

            @Override
            public void error(Object o, boolean isShowDialog) {
                mBaseView.error(o, isShowDialog);
            }
        };
        mSearchIterator = new SearchIteratorImpl(mCallback);
    }


    @Override
    public void search(MultipartBody.Part image, RequestBody customerId) {
        mSearchIterator.search(image, customerId);
    }
}
