package com.drawn_store.function.common.api.request;


import com.drawn_store.function.common.constants.ConstantApi;
import com.drawn_store.function.common.objects.request.LoginReq;
import com.drawn_store.function.common.objects.request.LoginReqGoogle;
import com.drawn_store.function.common.objects.response.LoginRes;
import com.drawn_store.function.common.objects.response.SearchByImageRes;
import com.drawn_store.function.common.objects.response.SearchResObj;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface SearchRequest {
    @Multipart
    @POST(ConstantApi.URL_SEARCH_BY_IMAGE)
    Call<SearchResObj> search(@Part MultipartBody.Part image,
                              @Part("customerId") RequestBody customerId);


}
