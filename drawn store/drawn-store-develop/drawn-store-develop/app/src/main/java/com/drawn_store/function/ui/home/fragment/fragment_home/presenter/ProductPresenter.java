package com.drawn_store.function.ui.home.fragment.fragment_home.presenter;


import com.drawn_store.function.common.objects.request.LikeRep;

public interface ProductPresenter {
    void productsSale(int userId,String limit);
    void productsNew(int userId,String limit);
    void productsHighestView(int userId,String limit);
    void productsType();
    void productsAll(int userId);
    void productsTypeLineId(int userId,int lineId);
    void addView(int productId);
    void addLike(LikeRep likeRep);
    void dislikeLike(LikeRep likeRep);
    void showLike(int userId);
    void showRating(int productId);
}
