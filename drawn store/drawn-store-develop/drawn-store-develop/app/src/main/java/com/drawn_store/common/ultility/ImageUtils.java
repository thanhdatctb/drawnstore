package com.drawn_store.common.ultility;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class ImageUtils {

    /**
     * screentshot the view
     * @param v
     * @param width
     * @param height
     * @return : bitmap
     */
    public static Bitmap getImageFromView(View v, int width, int height) {

        Bitmap bitmap = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = v.getBackground();
        if (bgDrawable != null) {
            bgDrawable.draw(canvas);
        } else {
            canvas.drawColor(Color.WHITE);
        }
        v.draw(canvas);
        return bitmap;
    }


    /**
     * @param drawable
     * @param sizeBitmap : size of the bitmap
     * @return
     */
    public static Bitmap drawableToBitmap(Drawable drawable, int sizeBitmap) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(sizeBitmap, sizeBitmap, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    /**
     * load image url
     *
     * @param activity
     * @param url
     * @param imageView
     * @param option
     */
    public static void loadImageByUrl(Activity activity, String url, ImageView imageView, RequestOptions option) {
        if (option != null)
            Glide.with(activity).setDefaultRequestOptions(option).load(url).into(imageView);
        else
            Glide.with(activity).load(url).into(imageView);
    }

    /**
     * load image url without requestOption
     *
     * @param activity
     * @param url
     * @param imageView
     */
    public static void loadImageByUrl(Activity activity, String url, ImageView imageView) {
        loadImageByUrl(activity, url, imageView, null);
    }

    //Rotate Bitmap
    public final static Bitmap rotate(Bitmap b, float degrees) {
        if (degrees != 0 && b != null) {
            Matrix m = new Matrix();
            m.setRotate(degrees, (float) b.getWidth() / 2,
                    (float) b.getHeight() / 2);

            Bitmap b2 = Bitmap.createBitmap(b, 0, 0, b.getWidth(),
                    b.getHeight(), m, true);
            if (b != b2) {
                b.recycle();
                b = b2;
            }

        }
        return b;
    }
}
