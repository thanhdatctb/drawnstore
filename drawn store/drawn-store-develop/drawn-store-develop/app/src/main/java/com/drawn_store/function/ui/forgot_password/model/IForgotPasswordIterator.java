package com.drawn_store.function.ui.forgot_password.model;

import com.drawn_store.common.presenter.BaseInteractor;
import com.drawn_store.function.common.objects.request.ChangerPasswordForgotRep;
import com.drawn_store.function.common.objects.request.EmployeeReq;

public interface IForgotPasswordIterator extends BaseInteractor {
    void updatePassword(ChangerPasswordForgotRep changerPasswordForgotRep);
}
