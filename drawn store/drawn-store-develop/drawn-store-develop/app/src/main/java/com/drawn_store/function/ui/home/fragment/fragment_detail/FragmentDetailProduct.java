package com.drawn_store.function.ui.home.fragment.fragment_detail;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.drawn_store.R;
import com.drawn_store.common.base.BaseActivity;
import com.drawn_store.common.base.BaseFragment;
import com.drawn_store.common.ultility.Utils;
import com.drawn_store.common.view.DialogAddCart;
import com.drawn_store.function.common.constants.FConstants;
import com.drawn_store.function.common.objects.ProductsInfo;
import com.drawn_store.function.common.objects.RatingInfo;
import com.drawn_store.function.common.objects.request.LikeRep;
import com.drawn_store.function.common.objects.response.AddLikeRes;
import com.drawn_store.function.common.objects.response.DislikeLikeRes;
import com.drawn_store.function.common.objects.response.RatingRes;
import com.drawn_store.function.common.view.BackReload;
import com.drawn_store.function.common.view.ProductColorAdapter;
import com.drawn_store.function.ui.home.fragment.fragment_home.presenter.ProductPresenterImpl;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentDetailProduct extends BaseFragment implements View.OnClickListener {

    public FragmentDetailProduct newInstance(ProductsInfo productsInfo) {
        Bundle args = new Bundle();
        args.putSerializable(FConstants.KEY_PRODUCT_INFO, productsInfo);
        FragmentDetailProduct fragment = new FragmentDetailProduct();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.collapsing)
    CollapsingToolbarLayout collapsing;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.imageSlider)
    SliderView imageSlider;
    @BindView(R.id.tvNameProducts)
    TextView tvNameProducts;
    @BindView(R.id.tvDiscountPric)
    TextView tvDiscountPric;
    @BindView(R.id.tvPric)
    TextView tvPric;
    @BindView(R.id.tvDiscountPercent)
    TextView tvDiscountPercent;
    @BindView(R.id.tvViewRating)
    TextView tvViewRating;
    @BindView(R.id.tvtotalView)
    TextView tvtotalView;
    @BindView(R.id.tvAmountShip)
    TextView tvAmountShip;
    @BindView(R.id.tvDescription)
    TextView tvDescription;
    @BindView(R.id.tvSizeColor)
    TextView tvSizeColor;
    @BindView(R.id.tvProgerEvalueteFif)
    TextView tvProgerEvalueteFif;
    @BindView(R.id.tvProgerEvalueteFour)
    TextView tvProgerEvalueteFour;
    @BindView(R.id.tvProgerEvalueteThree)
    TextView tvProgerEvalueteThree;
    @BindView(R.id.tvProgerEvalueteTwo)
    TextView tvProgerEvalueteTwo;
    @BindView(R.id.tvProgerEvalueteOne)
    TextView tvProgerEvalueteOne;
    @BindView(R.id.rvOptions)
    RecyclerView rvOptions;
    @BindView(R.id.rvEvaluate)
    RecyclerView rvEvaluate;
    @BindView(R.id.lnOptions)
    LinearLayout lnOptions;
    @BindView(R.id.btnBuyNow)
    MaterialButton btnBuyNow;
    @BindView(R.id.btnAddCart)
    MaterialButton btnAddCart;
    @BindView(R.id.tvStuff)
    TextView tvStuff;
    @BindView(R.id.tvBrand)
    TextView tvBrand;
    @BindView(R.id.tvStore)
    TextView tvStore;
    @BindView(R.id.numberEvaluete)
    TextView numberEvaluete;
    @BindView(R.id.favLike)
    FloatingActionButton favLike;
    private ProductsInfo productsInfo;
    private ProductColorAdapter productColorAdapter;
    private double amountShip = 40000;
    private ProductPresenterImpl productPresenter;
    private boolean checkLike;
    private EvaluateAdapter evaluateAdapter;
    private List<RatingInfo> ratingInfos;
    private BackReload backReload;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detail_product, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        isHideButtonBack(true);
        setBackgroundToolbar(Color.TRANSPARENT);
        setIconToolBar();
        showListener();
        collapsing.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsing.setCollapsedTitleTextAppearance(R.style.Collapsingbar);
        productPresenter = new ProductPresenterImpl(this);
        showProgress();
        getDataPut();
    }

    public FragmentDetailProduct setReloadFragment(BackReload backReload) {
        this.backReload = backReload;
        return this;
    }

    private void showListener() {
        lnOptions.setOnClickListener(this);
        btnAddCart.setOnClickListener(this);
        btnBuyNow.setOnClickListener(this);
        favLike.setOnClickListener(this);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void success(Object x) {
        super.success(x);
        hideProgress();
        DecimalFormat DCNuberRating = new DecimalFormat("0.#");
        if (x instanceof AddLikeRes) {
            showIconLike();
        } else if (x instanceof DislikeLikeRes) {
            showIconDisLike();
        } else if (x instanceof RatingRes) {
            ratingInfos = new ArrayList<>();
            RatingRes ratingRes = (RatingRes) x;
            ratingInfos = ratingRes.getData();
            evaluateAdapter = new EvaluateAdapter(requireContext());
            rvEvaluate.setHasFixedSize(true);
            rvEvaluate.setLayoutManager(new LinearLayoutManager(requireContext()));
            rvEvaluate.setAdapter(evaluateAdapter);
            evaluateAdapter.swap(ratingInfos);
            numberEvaluete.setText(ratingInfos.size() + " đánh giá");
            for (int i = 0; i < ratingInfos.size(); i++) {
                if (ratingInfos.get(i).getStars() == 5) {
                    List<Float> ratingOne = new ArrayList<>();
                    ratingOne.add(ratingInfos.get(i).getStars());
                    double totalRating = (ratingOne.size() / ratingInfos.size()) * 100;
                    String formatNuberRating = DCNuberRating.format(totalRating);
                    tvProgerEvalueteOne.setText(formatNuberRating + "%");
                }
            }
        }
    }

    @Override
    public void error(Object o, boolean isShowDialog) {
        super.error(o, isShowDialog);
        hideProgress();
    }

    private void showIconLike() {
        favLike.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPink)));
        favLike.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorRed)));
        checkLike = false;
    }

    private void showIconDisLike() {
        favLike.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryDark)));
        favLike.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorWhite)));
        checkLike = true;
    }

    @SuppressLint("SetTextI18n")
    private void getDataPut() {
        tvProgerEvalueteFif.setText("0%");
        tvProgerEvalueteFour.setText("0%");
        tvProgerEvalueteThree.setText("0%");
        tvProgerEvalueteTwo.setText("0%");
        tvProgerEvalueteOne.setText("0%");
        productsInfo = (ProductsInfo) getArguments().getSerializable(FConstants.KEY_PRODUCT_INFO);
        collapsing.setTitle(getResources().getString(R.string.app_name));
        showSliderImager(productsInfo.getImages());
        showListColor(productsInfo.getColors());
        tvNameProducts.setText(productsInfo.getName());
        tvSizeColor.setText("( " + productsInfo.getColors().size() + " Màu, " + productsInfo.getSizes().size() + " Size )");
        tvPric.setPaintFlags(tvPric.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        tvViewRating.setText("3/5");
        tvtotalView.setText("Lượt xem " + productsInfo.getTotalView());
        tvDescription.setText(productsInfo.getDescription());
        tvAmountShip.setText(Utils.formatAmount(amountShip));
        tvStuff.setText(productsInfo.getStuff().equals("") ? "null" : productsInfo.getStuff());
        tvBrand.setText(productsInfo.getBrand());
        tvStore.setText(productsInfo.getStore());
        if (productsInfo.getIsLiked()) {
            favLike.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPink)));
            favLike.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorRed)));
            checkLike = false;
        } else {
            checkLike = true;
        }
        if (productsInfo.getDiscountPercent().equals("0%")) {
            tvDiscountPercent.setVisibility(View.GONE);
            tvPric.setVisibility(View.GONE);
            tvDiscountPric.setText(Utils.formatAmount(Double.parseDouble(productsInfo.getPrice())));
        } else {
            tvDiscountPercent.setVisibility(View.VISIBLE);
            tvPric.setVisibility(View.VISIBLE);
            tvDiscountPric.setText(Utils.formatAmount(Double.parseDouble(productsInfo.getDiscountPrice())));
            tvPric.setText(Utils.formatAmount(Double.parseDouble(productsInfo.getPrice())));
            tvDiscountPercent.setText("- " + productsInfo.getDiscountPercent());
        }
        productPresenter.showRating(productsInfo.getId());
    }

    private void showSliderImager(List<String> images) {
        SliderAdapterExample sliderAdapterExample = new SliderAdapterExample(requireContext(), images);
        imageSlider.setSliderAdapter(sliderAdapterExample);
        imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        imageSlider.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        imageSlider.setIndicatorSelectedColor(Color.WHITE);
        imageSlider.setIndicatorUnselectedColor(Color.BLACK);
        imageSlider.setScrollTimeInSec(4); //set scroll delay in seconds :
        imageSlider.startAutoCycle();
    }

    private void showListColor(List<String> colors) {
        productColorAdapter = new ProductColorAdapter(requireContext(), false);
        rvOptions.setHasFixedSize(true);
        rvOptions.setLayoutManager(new LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false));
        rvOptions.setAdapter(productColorAdapter);
        productColorAdapter.swap(colors);
    }

    private void setIconToolBar() {
        if (mToolbar != null) {
            appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    if ((collapsing.getHeight() + verticalOffset) < (2 * ViewCompat.getMinimumHeight(collapsing))) {
                        mToolbar.setNavigationIcon(ContextCompat.getDrawable(requireContext(), R.drawable.ic_back_black));
                    } else {
                        mToolbar.setNavigationIcon(ContextCompat.getDrawable(requireContext(), R.drawable.bg_img_back));
                    }
                }
            });
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((BaseActivity) getActivity()).onBackPressed();
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lnOptions:
                showDialogAddCart(FConstants.SHOW_ALL_BOTTON);
                break;
            case R.id.btnBuyNow:
                showDialogAddCart(FConstants.SHOW_BOTTON_BUY_NOW);
                break;
            case R.id.btnAddCart:
                showDialogAddCart(FConstants.SHOW_BOTTIN_ADD_CART);
                break;
            case R.id.favLike:
                showProgress();
                if (checkLike) {
                    productPresenter.addLike(new LikeRep(loginRes.getData().getId(), productsInfo.getId()));
                } else {
                    productPresenter.dislikeLike(new LikeRep(loginRes.getData().getId(), productsInfo.getId()));
                }
                break;
        }
    }

    private void showDialogAddCart(int showposition) {
        DialogAddCart dialogAddCart = new DialogAddCart().newInstance(showposition, productsInfo);
        dialogAddCart.show(getChildFragmentManager(), "botton");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        backReload.reloadFragment();
    }
}
