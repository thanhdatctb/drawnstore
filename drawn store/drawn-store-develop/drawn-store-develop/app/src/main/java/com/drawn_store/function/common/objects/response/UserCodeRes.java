package com.drawn_store.function.common.objects.response;

import com.drawn_store.common.object.BaseObj;
import com.drawn_store.function.common.objects.UserCodeInfo;
import com.drawn_store.function.common.objects.UserInfo;
import com.google.gson.annotations.SerializedName;

public class UserCodeRes extends BaseObj {
    @SerializedName("data")
    private UserCodeInfo mData;

    public UserCodeInfo getData() {
        return mData;
    }

    public void setData(UserCodeInfo data) {
        mData = data;
    }
}
