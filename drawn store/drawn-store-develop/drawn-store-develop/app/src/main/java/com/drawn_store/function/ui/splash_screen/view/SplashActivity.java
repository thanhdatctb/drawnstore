package com.drawn_store.function.ui.splash_screen.view;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;


import com.drawn_store.R;
import com.drawn_store.common.base.BaseActivity;
import com.drawn_store.common.constants.AppConfig;
import com.drawn_store.common.presenter.BaseView;
import com.drawn_store.function.common.constants.FConstants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity implements BaseView {

    @BindView(R.id.tvVersion)
    TextView tvVersion;
    @BindView(R.id.imgSplash)
    ImageView imgSplash;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        imgSplash.animate()
                .scaleX(0.5f).scaleY(0.5f)//scale to quarter(half x,half y)
                .translationY(Float.parseFloat(String.valueOf(imgSplash.getHeight()/4))).translationX(Float.parseFloat(String.valueOf(imgSplash.getWidth()/4)))// move to bottom / right
                .alpha(0.5f) // make it less visible
                .rotation(360f) // one round turns
                .setDuration(1000) // all take 1 seconds
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {

                    }
                });
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            tvVersion.setText("#version: " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String email = prefManager.getString(FConstants.KEY_EMAIL);
        if (email == null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    navigator.screenLogin(SplashActivity.this);
                }
            }, AppConfig.TIME_SHOW_SPLASH_SCREEN);
        }else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    navigator.screenMain(SplashActivity.this);
                }
            }, AppConfig.TIME_SHOW_SPLASH_SCREEN);
        }
    }


}
