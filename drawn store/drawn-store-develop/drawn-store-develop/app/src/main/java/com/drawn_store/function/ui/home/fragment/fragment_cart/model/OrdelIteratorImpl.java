package com.drawn_store.function.ui.home.fragment.fragment_cart.model;

import com.drawn_store.common.api.callback.MyCallback;
import com.drawn_store.common.base.BaseInteractorImpl;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.InteractorCallback;
import com.drawn_store.function.common.api.ApiUtils;
import com.drawn_store.function.common.api.request.CartRequest;
import com.drawn_store.function.common.objects.request.CartRep;
import com.drawn_store.function.common.objects.request.UpdateCartRep;
import com.drawn_store.function.common.objects.response.CartDeleteRes;
import com.drawn_store.function.common.objects.response.CartRes;
import com.drawn_store.function.common.objects.response.CartUpdateRes;
import com.drawn_store.function.common.objects.response.ProductsSaleRes;

import retrofit2.Call;
import retrofit2.Response;

public class OrdelIteratorImpl extends BaseInteractorImpl<BaseObj, Object> implements OrderIterator {

    private CartRequest cartRequest;

    public OrdelIteratorImpl(InteractorCallback callback) {
        super(callback);
        cartRequest = ApiUtils.getCartRequest();
    }

    @Override
    public void addCart(CartRep cartRep) {
        cartRequest.addCart(cartRep).enqueue(new MyCallback<CartRes>() {
            @Override
            public void onSuccess(Call<CartRes> call, Response<CartRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<CartRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void getCart(int customerId) {
        cartRequest.getCart(customerId).enqueue(new MyCallback<CartRes>() {
            @Override
            public void onSuccess(Call<CartRes> call, Response<CartRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<CartRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void deleteCart(int cartId) {
        cartRequest.deleteCart(cartId).enqueue(new MyCallback<CartDeleteRes>() {
            @Override
            public void onSuccess(Call<CartDeleteRes> call, Response<CartDeleteRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<CartDeleteRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }

    @Override
    public void updateCart(UpdateCartRep updateCartRep) {
        cartRequest.updateCart(updateCartRep).enqueue(new MyCallback<CartUpdateRes>() {
            @Override
            public void onSuccess(Call<CartUpdateRes> call, Response<CartUpdateRes> response) {
                if (response.body().getStatusCode() == 200) {
                    mCallback.success(response.body());
                } else {
                    mCallback.error(response.body(), true);
                }
            }

            @Override
            public void onError(Call<CartUpdateRes> call, Object object) {
                mCallback.error(object, true);
            }
        });
    }
}
