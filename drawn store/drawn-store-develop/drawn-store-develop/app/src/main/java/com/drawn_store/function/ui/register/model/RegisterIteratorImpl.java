package com.drawn_store.function.ui.register.model;

import com.drawn_store.common.api.callback.MyCallback;
import com.drawn_store.common.base.BaseInteractorImpl;
import com.drawn_store.common.object.BaseObj;
import com.drawn_store.common.presenter.InteractorCallback;
import com.drawn_store.function.common.api.ApiUtils;
import com.drawn_store.function.common.api.request.UserRequest;
import com.drawn_store.function.common.objects.request.RegisterReq;
import com.drawn_store.function.common.objects.response.RegisterRes;

import retrofit2.Call;
import retrofit2.Response;

public class RegisterIteratorImpl extends BaseInteractorImpl<BaseObj, Object> implements  IRegisterIterator {

    private UserRequest userRequest;

    public RegisterIteratorImpl(InteractorCallback callback) {
        super(callback);
        userRequest = ApiUtils.getUserRequest();
    }


    @Override
    public void register(RegisterReq registerReq) {
        userRequest.register(registerReq).enqueue(new MyCallback<RegisterRes>() {
            @Override
            public void onSuccess(Call<RegisterRes> call, Response<RegisterRes> response) {
                if(response.body().getStatusCode() == 200 ){
                    mCallback.success(response.body());
                }else{
                    mCallback.error(response.body(),true);
                }
            }

            @Override
            public void onError(Call<RegisterRes> call, Object object) {
                mCallback.error(object,true);
            }
        });
    }
}
