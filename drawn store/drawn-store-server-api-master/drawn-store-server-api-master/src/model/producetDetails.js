'use strict';

module.exports = (sequelize, DataType) => {
    const ProductDetails = sequelize.define(
        'productDetail',
        {
            id: {
                type: DataType.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            productId: {
                type: DataType.INTEGER,
                allowNull: false
            },
            productImage: {
                type: DataType.STRING,
                allowNull: false
            }
        },
        {
            timestamps: false,
            freezeTableName: true,
            tableName: 'ProductDetails'
        }
    );
    return ProductDetails;
};
