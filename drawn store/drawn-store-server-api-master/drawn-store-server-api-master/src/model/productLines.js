'use strict';

module.exports = (sequelize, DataType) => {
    const ProductLines = sequelize.define(
        'productline',
        {
            id: {
                type: DataType.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: DataType.STRING,
                allowNull: false
            }
        },
        {
            timestamps: true,
            freezeTableName: true,
            tableName: 'ProductLines'
        }
    );

    ProductLines.associate = (models) => {
        ProductLines.hasMany(models.product);
    };

    return ProductLines;
};
