'use strict';

module.exports = (sequelize, DataType) => {
    const Products = sequelize.define(
        'product',
        {
            id: {
                type: DataType.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: DataType.STRING,
                allowNull: false
            },
            productlineId: {
                type: DataType.INTEGER,
                allowNull: false
            },
            price: {
                type: DataType.STRING,
                allowNull: false
            },
            description: {
                type: DataType.STRING,
                allowNull: true
            },
            discountPrice: {
                type: DataType.STRING,
                allowNull: true
            },
            totalView: {
                type: DataType.INTEGER,
                allowNull: true
            },
            brands: {
                type: DataType.STRING,
                allowNull: true
            },
            stuff: {
                type: DataType.STRING,
                allowNull: true
            },
            store: {
                type: DataType.STRING,
                allowNull: true
            }
        },
        {
            timestamps: true,
            freezeTableName: true,
            tableName: 'Products'
        }
    );

    Products.associate = (models) => {
        Products.hasMany(models.productDetail);
        Products.hasMany(models.ratingStar);
        Products.hasMany(models.orderDetail);
        Products.hasMany(models.history);
        Products.hasMany(models.productSizes);
        Products.hasMany(models.productColors);
        Products.hasMany(models.cart);
        Products.hasMany(models.productLiked);
    };

    return Products;
};
