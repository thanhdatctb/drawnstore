'use strict';

module.exports = (sequelize, DataType) => {
    const OrderDetails = sequelize.define(
        'orderDetail',
        {
            id: {
                type: DataType.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            orderId: {
                type: DataType.INTEGER,
                allowNull: false
            },
            amount: {
                type: DataType.INTEGER,
                allowNull: false
            },
            productId: {
                type: DataType.INTEGER,
                allowNull: false
            },
            size: {
                type: DataType.STRING,
                allowNull: false
            },
            color: {
                type: DataType.STRING,
                allowNull: false
            }
        },
        {
            timestamps: true,
            freezeTableName: true,
            tableName: 'OrderDetails'
        }
    );
    return OrderDetails;
};
