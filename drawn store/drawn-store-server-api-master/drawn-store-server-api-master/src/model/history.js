'use strict';

module.exports = (sequelize, DataType) => {
    const History = sequelize.define(
        'history',
        {
            id: {
                type: DataType.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            productId: {
                type: DataType.INTEGER,
                allowNull: false
            },
            customerId: {
                type: DataType.INTEGER,
                allowNull: false
            },
            amount: {
                type: DataType.INTEGER,
                allowNull: false
            },
            totalPrice: {
                type: DataType.STRING,
                allowNull: false
            }
        },
        {
            timestamps: true,
            freezeTableName: true,
            tableName: 'History'
        }
    );
    return History;
};
