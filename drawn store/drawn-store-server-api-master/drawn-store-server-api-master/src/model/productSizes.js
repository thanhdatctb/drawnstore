'use strict';

module.exports = (sequelize, DataType) => {
    const ProductSizes = sequelize.define(
        'productSizes',
        {
            id: {
                type: DataType.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            productId: {
                type: DataType.INTEGER,
                allowNull: false
            },
            sizes: {
                type: DataType.STRING,
                allowNull: false
            }
        },
        {
            timestamps: false,
            freezeTableName: true,
            tableName: 'ProductSizes'
        }
    );
    return ProductSizes;
};
