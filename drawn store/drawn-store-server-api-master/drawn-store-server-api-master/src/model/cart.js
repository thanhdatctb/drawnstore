'use strict';

module.exports = (sequelize, DataType) => {
    const Cart = sequelize.define(
        'cart',
        {
            id: {
                type: DataType.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            productId: {
                type: DataType.INTEGER,
                allowNull: false
            },
            customerId: {
                type: DataType.INTEGER,
                allowNull: false
            },
            amount: {
                type: DataType.INTEGER,
                allowNull: false
            },
            size: {
                type: DataType.STRING,
                allowNull: false
            },
            color: {
                type: DataType.STRING,
                allowNull: false
            }
        },
        {
            timestamps: true,
            freezeTableName: true,
            tableName: 'Cart'
        }
    );
    return Cart;
};
