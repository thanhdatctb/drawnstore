'use strict';

module.exports = (sequelize, DataType) => {
    const Orders = sequelize.define(
        'order',
        {
            id: {
                type: DataType.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            customerId: {
                type: DataType.INTEGER,
                allowNull: false
            },
            totalPrice: {
                type: DataType.STRING,
                allowNull: false
            },
            status: {
                type: DataType.STRING, // canceled, waiting, sent
                allowNull: false
            },
            address: {
                type: DataType.STRING,
                allowNull: false
            }
        },
        {
            timestamps: true,
            freezeTableName: true,
            tableName: 'Orders'
        }
    );
    Orders.associate = (models) => {
        Orders.hasMany(models.orderDetail);
    };
    return Orders;
};
