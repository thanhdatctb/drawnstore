'use strict';

module.exports = (sequelize, DataType) => {
    const ProductColors = sequelize.define(
        'productColors',
        {
            id: {
                type: DataType.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            productId: {
                type: DataType.INTEGER,
                allowNull: false
            },
            color: {
                type: DataType.STRING,
                allowNull: false
            }
        },
        {
            timestamps: false,
            freezeTableName: true,
            tableName: 'ProductColors'
        }
    );
    return ProductColors;
};
