'use strict';

module.exports = (sequelize, DataType) => {
    const CustomerOrderInfo = sequelize.define(
        'customerOrderInfo',
        {
            id: {
                type: DataType.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: DataType.STRING,
                allowNull: false
            },
            customerId: {
                type: DataType.INTEGER,
                allowNull: false
            },
            address: {
                type: DataType.STRING,
                allowNull: false
            },
            phoneNumber: {
                type: DataType.STRING,
                allowNull: false
            },
            isDefault: {
                type: DataType.BOOLEAN,
                allowNull: false
            },
            districtCode: {
                type: DataType.INTEGER,
                allowNull: false
            },
            provinceCode: {
                type: DataType.INTEGER,
                allowNull: false
            }
        },
        {
            timestamps: true,
            freezeTableName: true,
            tableName: 'CustomerOrderInfo'
        }
    );
    return CustomerOrderInfo;
};
