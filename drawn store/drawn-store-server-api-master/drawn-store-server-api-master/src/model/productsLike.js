'use strict';

module.exports = (sequelize, DataType) => {
    const ProductLiked = sequelize.define(
        'productLiked',
        {
            id: {
                type: DataType.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            productId: {
                type: DataType.INTEGER,
                allowNull: false
            },
            customerId: {
                type: DataType.INTEGER,
                allowNull: false
            }
        },
        {
            timestamps: true,
            freezeTableName: true,
            tableName: 'ProductLiked'
        }
    );
    return ProductLiked;
};
