'use strict';

module.exports = (sequelize, DataType) => {
    const RatingStars = sequelize.define(
        'ratingStar',
        {
            id: {
                type: DataType.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            productId: {
                type: DataType.INTEGER,
                allowNull: false
            },
            customerId: {
                type: DataType.INTEGER,
                allowNull: false
            },
            stars: {
                type: DataType.STRING,
                allowNull: false
            },
            comments: {
                type: DataType.STRING,
                allowNull: false
            }
        },
        {
            timestamps: true,
            freezeTableName: true,
            tableName: 'RatingStars'
        }
    );
    return RatingStars;
};
