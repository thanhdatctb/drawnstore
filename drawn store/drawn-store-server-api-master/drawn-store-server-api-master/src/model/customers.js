'use strict';

module.exports = (sequelize, DataType) => {
    const Customers = sequelize.define(
        'customer',
        {
            id: {
                type: DataType.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: DataType.STRING,
                allowNull: false
            },
            email: {
                type: DataType.STRING,
                allowNull: false
            },
            password: {
                type: DataType.STRING,
                allowNull: false
            },
            avatar: {
                type: DataType.STRING,
                allowNull: true
            }
        },
        {
            timestamps: true,
            freezeTableName: true,
            tableName: 'Customers'
        }
    );

    Customers.associate = (models) => {
        Customers.hasMany(models.customerOrderInfo);
        Customers.hasMany(models.order);
        Customers.hasMany(models.ratingStar);
        Customers.hasMany(models.history);
        Customers.hasMany(models.cart);
        Customers.hasMany(models.productLiked);
    };

    return Customers;
};
