const Sequelize = require('sequelize');
const dotenv = require('dotenv');

dotenv.config();
// const op = Sequelize.Op;
// const operatorsAliases = {
//     $between: op.between // create an alias for Op.between
// };
// Override timezone formatting
Sequelize.DATE.prototype._stringify = function _stringify(date, options) {
    date = this._applyTimezone(date, options);

    // Z here means current timezone, _not_ UTC
    // return date.format('YYYY-MM-DD HH:mm:ss.SSS Z');
    return date.format('YYYY-MM-DD HH:mm:ss.SSS');
};

module.exports = {
    config: {
        database: process.env.DB_NAME,
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        options: {
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            dialect: 'mysql',
            encrypt: true,
            // operatorsAliases,
            // Specify options, which are used when sequelize.define is called.
            define: {
                charset: process.env.DB_CHARSET,
                collate: process.env.DB_COLLATE
            },
            // pool configuration used to pool database connections
            logging: false,
            pool: {
                max: 5,
                min: 0,
                acquire: 120000,
                idle: 10000
            }
        }
    }
};
