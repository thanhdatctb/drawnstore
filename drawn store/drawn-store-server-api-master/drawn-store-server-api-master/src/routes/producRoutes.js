const express = require('express');
const router = express.Router();
const productControler = require('../controller/productControler');

router.get('/sale/:customerId/:limit', productControler.getSaleProducts);

router.get('/highest-view/:customerId/:limit', productControler.getHighestProducts);

router.get('/latest/:customerId/:limit', productControler.getLatestProducts);

router.get('/line', productControler.getProductLines);

router.get('/products/:customerId/:productLineId', productControler.getProductsFollowProductLine);

router.get('/products/:customerId', productControler.getAllProducts);

router.put('/view/:productId', productControler.incrementView);

module.exports = router;
