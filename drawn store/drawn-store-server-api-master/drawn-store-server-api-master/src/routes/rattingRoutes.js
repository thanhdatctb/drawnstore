const express = require('express');
const router = express.Router();
const rattingController = require('../controller/rattingControler');

router.post('/', rattingController.ratting);

router.get('/:productId', rattingController.getRatting);

module.exports = router;
