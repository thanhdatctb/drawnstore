const express = require('express');
const router = express.Router();
const searchControler = require('../controller/searchControler');
const upload = require('../utils/uploadMiddleware');

router.post('/', upload.single('image'), searchControler.searchWithImage);

module.exports = router;
