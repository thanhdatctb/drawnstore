const express = require('express');
const router = express.Router();
const orderControler = require('../controller/orderController');

router.post('/', orderControler.order);

router.put('/', orderControler.updateOrderStatus);

router.get('/:userId/:filter', orderControler.orderHistory);

module.exports = router;
