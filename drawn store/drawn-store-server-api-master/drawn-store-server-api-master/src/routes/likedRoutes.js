const express = require('express');
const router = express.Router();
const likedController = require('../controller/likedControler');

router.post('/like', likedController.addLike);

router.delete('/like', likedController.disLike);

router.get('/like/:customerId', likedController.likes);

module.exports = router;
