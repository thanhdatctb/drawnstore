const express = require('express');
const router = express.Router();
const loginRoutes = require('./loginRoutes');
const customerRoutes = require('./customerRoutes');
const productRoutes = require('./producRoutes');
const customerDetailRoutes = require('./customerDetailRoutes');
const cartRoutes = require('./cartRoutes');
const likedRoutes = require('./likedRoutes');
const rattingRoutes = require('./rattingRoutes');
const stearchRoutes = require('./searchRoutes');
const orderRoutes = require('./orderRoutes');

router.use('/', loginRoutes);
router.use('/user', customerRoutes);
router.use('/product', productRoutes);
router.use('/user', customerDetailRoutes);
router.use('/cart', cartRoutes);
router.use('/product', likedRoutes);
router.use('/ratting', rattingRoutes);
router.use('/search', stearchRoutes);
router.use('/order', orderRoutes);

module.exports = router;
