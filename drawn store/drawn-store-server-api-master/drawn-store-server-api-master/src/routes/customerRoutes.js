const express = require('express');
const router = express.Router();
const customerController = require('../controller/cusomerController');
const upload = require('../utils/uploadMiddleware');

router.put('/register', customerController.register);

router.get('/info/:userId', customerController.getUserInfo);

router.post('/forgot-password', customerController.fogotPassword);

router.post('/change-password', customerController.changePassword);

router.post('/forgot-password-change', customerController.fogotPasswordChange);

router.post('/update-account-info', upload.single('avatar'), customerController.updateAccountInfo);

router.delete('/:userId', customerController.deleteAccount);

module.exports = router;
