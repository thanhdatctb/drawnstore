const express = require('express');
const router = express.Router();
const customerDetailController = require('../controller/customerDetailController');

router.post('/order-info', customerDetailController.updateAccountOderInfo);

router.put('/order-info', customerDetailController.addAccountOderInfo);

router.get('/order-info/:userId', customerDetailController.getAccountOderInfo);

router.delete('/order-info/:infoId', customerDetailController.deleteAccountOderInfo);

router.get('/provinces', customerDetailController.getProvinceList);

router.get('/districts/:code', customerDetailController.getDistrictList);

router.get('/wards/:code', customerDetailController.getWardsList);

module.exports = router;
