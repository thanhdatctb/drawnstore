const express = require('express');
const router = express.Router();
const loginControler = require('../controller/login');

router.post('/login', loginControler.doLogin);
router.post('/login-with-google', loginControler.loginWithGoogle);

module.exports = router;
