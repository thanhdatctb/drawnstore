const express = require('express');
const router = express.Router();
const cartController = require('../controller/cartController');

router.post('/', cartController.addCart);

router.get('/:customerId', cartController.getCarts);

router.delete('/:cartId', cartController.removeCartItem);

router.put('/', cartController.updateAmount);

module.exports = router;
