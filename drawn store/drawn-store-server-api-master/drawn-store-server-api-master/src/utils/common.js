module.exports = {
    isNumeric(text) {
        return /^\d+$/.test(text);
    }

};
