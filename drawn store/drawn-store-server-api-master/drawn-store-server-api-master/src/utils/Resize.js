const sharp = require('sharp');
const path = require('path');

class Resize {
    constructor(folder) {
        this.folder = folder;
    }

    async save(buffer, nameFile) {
        const filename = Resize.filename(nameFile);
        const filepath = this.filepath(filename);

        await sharp(buffer)
            .resize(1024, 1024, {
                fit: sharp.fit.inside,
                withoutEnlargement: true
            })
            .toFile(filepath);

        return filename;
    }

    static filename(nameFile) {
        return nameFile + Date.now() + '.png';
    }

    filepath(filename) {
        return path.resolve(`${ this.folder }/${ filename }`);
    }
}
module.exports = Resize;
