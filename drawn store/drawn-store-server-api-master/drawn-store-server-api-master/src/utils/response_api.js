const statusCodeSuccess = 200;
const statusCodeBadRequest = 403;
const statusCodeNotFound = 404;
const statusCodeServerError = 500;
const statusCodeError = 400;

/**
 *
 * @param {*} res
 * @param {*} statusCode //200,400,500
 * @param {*} message //Success, request format invalid, server error
 * @param {*} data
 */
function responseAPI(res, statusCode, message, data) {
    res.setHeader('Content-Type', 'application/json');
    res.status(statusCode).send({
        statusCode: statusCode,
        message: message,
        data: data
    });
}

module.exports = {
    responseAPI,
    statusCodeSuccess,
    statusCodeBadRequest,
    statusCodeNotFound,
    statusCodeServerError,
    statusCodeError
};
