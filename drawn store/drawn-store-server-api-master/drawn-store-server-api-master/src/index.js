var createError = require('http-errors');
var express = require('express');
var path = require('path');
var logger = require('morgan');
const bodyParser = require('body-parser');
const { sequelize } = require('./model');
var indexRouter = require('./routes/index');

var app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));

app.use(logger('dev'));

app.use('/api', indexRouter);
// Wait for DB connection
let dbConnectRetryCnt = process.env.DB_CONN_RETRY_MAX_COUNT || 4;

function waitForConnection() {
    sequelize
        .authenticate()
        .then(() => {
            // We sync db on production on start
            if (process.env.RUN_ENV === 'prod') {
                sequelize
                    .sync({
                        force: false,
                        alter: false
                    })
                    .then(() => {
                        console.log('Database synced!');
                    });
            }
        })
        .catch((err) => {
            if (dbConnectRetryCnt > 0) {
                dbConnectRetryCnt--;
                setTimeout(waitForConnection, 5000);
            } else {
                console.error('Cannot connect to DB', sequelize, err);
            }
        });
}
waitForConnection();

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: err
    });
});

module.exports = app;
