const response = require('../utils/response_api');
const {
    cart: Cart,
    product: Products,
    productDetail: ProductDetails,
    productSizes: ProductSizes,
    productColors: ProductColors,
    productLiked: ProductLiked
} = require('../model/');
const { isNumeric } = require('../utils/common');

exports.addCart = (req, res) => {
    const customerId = req.body.customerId;
    const productId = req.body.productId;
    const amount = req.body.amount;
    const color = req.body.color;
    const size = req.body.size;

    if (!customerId || !productId || !amount || !color || !size) {
        response.responseAPI(
            res,
            403,
            'Bad request',
            null
        );
    } else {
        Cart.findOne({
            where: {
                customerId,
                productId,
                color,
                size
            }
        }).then((data) => {
            if (data != null) {
                Cart.update(
                    {
                        amount: parseInt(data.amount) + amount
                    },
                    {
                        where: {
                            customerId,
                            productId
                        }
                    }
                ).then((result) => {
                    if (result != null) {
                        response.responseAPI(
                            res,
                            response.statusCodeSuccess,
                            'Add success',
                            null
                        );
                    } else {
                        response.responseAPI(
                            res,
                            response.statusCodeError,
                            'Add failure',
                            null
                        );
                    }
                });
            } else {
                Cart.create({
                    customerId,
                    productId,
                    amount,
                    size,
                    color
                }).then((result) => {
                    if (result != null) {
                        response.responseAPI(
                            res,
                            response.statusCodeSuccess,
                            'Add success',
                            null
                        );
                    } else {
                        response.responseAPI(
                            res,
                            response.statusCodeError,
                            'Add failure',
                            null
                        );
                    }
                });
            }
        }).catch((err) => {
            response.responseAPI(
                res,
                response.statusCodeServerError,
                err.message,
                null
            );
        });
    }
};

exports.getCarts = (req, res) => {
    const customerId = req.params.customerId;
    Cart.findAll({
        where: {
            customerId: customerId
        }
    }).then(async (itemCarts) => {
        if (itemCarts != null) {
            const result = await Promise.all(itemCarts.map(async e => {
                const products = await getProductInfo(e.productId, customerId);
                return {
                    cartId: e.id,
                    amount: e.amount,
                    size: e.size,
                    color: e.color,
                    ...products
                };
            }));

            response.responseAPI(
                res,
                response.statusCodeSuccess,
                'Sucess',
                result
            );
        } else {
            response.responseAPI(
                res,
                response.statusCodeError,
                'Add failure',
                null
            );
        }
    }).catch((err) => {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message,
            null
        );
    });
};

exports.removeCartItem = (req, res) => {
    const cartId = req.params.cartId;
    Cart.destroy({
        where: {
            id: cartId
        }
    }).then((result) => {
        response.responseAPI(
            res,
            response.statusCodeSuccess,
            'Delete success',
            null
        );
    }).catch((err) => {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message,
            null
        );
    });
};

exports.updateAmount = (req, res) => {
    const cartId = req.body.cartId;
    const amount = req.body.amount;
    const color = req.body.color;
    const size = req.body.size;
    Cart.update({ amount, color, size }, {
        where: {
            id: cartId
        }
    }).then((result) => {
        response.responseAPI(
            res,
            response.statusCodeSuccess,
            'Update success',
            null
        );
    }).catch((err) => {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message,
            null
        );
    });
};

async function getProductInfo(productId, customerId) {
    const products = await Products.findOne({
        where: { id: productId }
    });
    const imageRes = await ProductDetails.findAll({
        attributes: ['productImage'],
        where: {
            productId: products.id
        }
    });
    const images = imageRes.map(e => {
        return e.productImage;
    });
    let discountPercent = 0;
    if (isNumeric(products.discountPrice) && isNumeric(products.price) && parseInt(products.discountPrice) > 0) {
        discountPercent = 100 - ((parseInt(products.discountPrice) / parseInt(products.price)) * 100);
    }
    const sizes = await getProductSizes(products.id);
    const colors = await getProductColors(products.id);
    const isLiked = await isLikedProduct(products.id, customerId);
    const result = {
        id: products.id,
        name: products.name,
        productLinesId: products.productLinesId,
        price: products.price,
        description: products.description,
        discountPrice: products.discountPrice,
        totalView: products.totalView,
        images: images,
        discountPercent: Math.round(discountPercent) + '%',
        brand: products.brands,
        stuff: products.stuff,
        store: products.store,
        isLiked,
        colors,
        sizes,
        createdAt: products.createdAt,
        updatedAt: products.updatedAt
    };

    return result;
}

async function getProductSizes(productId) {
    const sizes = await ProductSizes.findAll({
        where: { productId: productId },
        order: [['sizes', 'ASC']]
    });
    const result = sizes.map(e => e.sizes);
    return result;
}

async function getProductColors(productId) {
    const colors = await ProductColors.findAll({
        where: { productId: productId },
        order: [['color', 'ASC']]
    });
    const result = colors.map(e => e.color);
    return result;
}

async function isLikedProduct(productId, customerId) {
    const result = await ProductLiked.findAll({
        where: { productId, customerId }
    });
    return result.length > 0;
}
