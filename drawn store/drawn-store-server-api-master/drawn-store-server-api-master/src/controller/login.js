const response = require('../utils/response_api');
const md5 = require('md5');
const {
    customer: Customers
} = require('../model/');

exports.doLogin = (req, res) => {
    const email = req.body.email;
    const password = req.body.password;

    if (!password || !email) {
        response.responseAPI(
            res,
            403,
            'Bad request',
            null
        );
    } else {
        Customers.findOne({
            attributes: ['id', 'name', 'email', 'avatar', 'createdAt', 'updatedAt'],
            where: {
                email: email,
                PassWord: md5(password)
            }
        }).then((user) => {
            if (user != null) {
                response.responseAPI(
                    res,
                    response.statusCodeSuccess,
                    'Login success',
                    user
                );
            } else {
                response.responseAPI(
                    res,
                    response.statusCodeNotFound,
                    'Wrong username or password',
                    null
                );
            }
        }).catch((err) => {
            response.responseAPI(
                res,
                500,
                err.message,
                null
            );
        });
    }
};

exports.loginWithGoogle = (req, res) => {
    const paramBody = req.body;

    if (!paramBody.name || !paramBody.email) {
        response.responseAPI(
            res,
            403,
            'Bad request',
            null
        );
    } else {
        const name = paramBody.name;
        const email = paramBody.email;
        const password = md5(paramBody.email);
        const avatar = paramBody.avatar;
        Customers.findOne({
            attributes: ['id', 'name', 'email', 'avatar', 'createdAt', 'updatedAt'],
            where: {
                email: email
            }
        }).then((user) => {
            if (user != null) {
                response.responseAPI(
                    res,
                    response.statusCodeSuccess,
                    'Login success',
                    user
                );
            } else {
                Customers.create(
                    {
                        name: name,
                        email: email,
                        password: password,
                        avatar: avatar
                    }
                ).then((result) => {
                    response.responseAPI(
                        res,
                        response.statusCodeSuccess,
                        'Login success',
                        {
                            id: result.id,
                            name: result.name,
                            email: result.email,
                            avatar: result.avatar,
                            createdAt: result.createdAt,
                            updatedAt: result.updatedAt
                        }
                    );
                }).catch((err) => {
                    response.responseAPI(
                        res,
                        500,
                        err.message,
                        null
                    );
                });
            }
        }).catch((err) => {
            response.responseAPI(
                res,
                500,
                err.message,
                null
            );
        });
    }
};
