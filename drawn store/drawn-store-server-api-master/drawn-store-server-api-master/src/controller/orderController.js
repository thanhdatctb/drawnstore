const response = require('../utils/response_api');
const {
    order: Orders,
    orderDetail: OrderDetails,
    product: Products,
    productDetail: ProductDetails,
    productLiked: ProductLiked
} = require('../model/');
const { isNumeric } = require('../utils/common');

exports.order = (req, res) => {
    const userId = req.body.userId;
    const totalPrice = req.body.totalPrice;
    const status = req.body.status;
    const address = req.body.address;
    const products = req.body.products;

    if (!userId || !totalPrice || !status || !address) {
        response.responseAPI(
            res,
            403,
            'Bad request',
            null
        );
    } else {
        const order = {
            customerId: userId,
            totalPrice: totalPrice,
            status: status,
            address: address
        };

        Orders.create(order).then(async (user) => {
            await Promise.all(products.map(async element => {
                const orderDetail = {
                    orderId: user.id,
                    ...element
                };
                await OrderDetails.create(orderDetail);
            }));
            response.responseAPI(
                res,
                200,
                'Success',
                null
            );
        }).catch((err) => {
            response.responseAPI(
                res,
                500,
                err.message,
                null
            );
        });
    }
};

exports.updateOrderStatus = (req, res) => {
    const userId = req.body.userId;
    const orderId = req.body.orderId;
    const status = req.body.status;

    if (!userId || !status || !orderId) {
        response.responseAPI(
            res,
            403,
            'Bad request',
            null
        );
    } else {
        Orders.update({
            status: status
        }, {
            where: {
                id: orderId,
                customerId: userId
            }
        }).then(async (result) => {
            if (result[0] === 1) {
                response.responseAPI(
                    res,
                    200,
                    'Success',
                    null
                );
            } else {
                response.responseAPI(
                    res,
                    400,
                    'Failure',
                    null
                );
            }
        }).catch((err) => {
            response.responseAPI(
                res,
                500,
                err.message,
                null
            );
        });
    }
};

exports.orderHistory = (req, res) => {
    const filterParam = req.params.filter;
    const userId = req.params.userId;

    if (!filterParam) {
        response.responseAPI(
            res,
            403,
            'Bad request',
            null
        );
    } else {
        if (filterParam === 'all') {
            Orders.findAll({
                where: {
                    customerId: userId
                }
            }).then(async orders => {
                const result = await Promise.all(
                    orders.map(async order => {
                        const orderDetail = await getOrderDetail(order.id, userId);
                        return {
                            userId: order.customerId,
                            totalPrice: order.totalPrice,
                            status: order.status,
                            address: order.address,
                            products: orderDetail
                        };
                    })
                );

                response.responseAPI(
                    res,
                    200,
                    'Success',
                    result
                );
            });
        } else {
            Orders.findAll({
                where: {
                    customerId: userId,
                    status: filterParam
                }
            }).then(async orders => {
                const result = await Promise.all(
                    orders.map(async order => {
                        const orderDetail = await getOrderDetail(order.id, userId);
                        return {
                            userId: order.customerId,
                            totalPrice: order.totalPrice,
                            status: order.status,
                            address: order.address,
                            products: orderDetail
                        };
                    })
                );

                response.responseAPI(
                    res,
                    200,
                    'Success',
                    result
                );
            });
        }
    }
};

async function getOrderDetail(orderId, customerId) {
    const orderDetails = await OrderDetails.findAll({ where: { orderId: orderId } });

    const result = await Promise.all(
        orderDetails.map(async orderDetail => {
            const result = await getProductInfo(orderDetail.productId, customerId);
            return {
                size: orderDetail.size,
                color: orderDetail.color,
                amount: orderDetail.amount,
                ...result
            };
        })
    );
    return result;
}

async function getProductInfo(productId, customerId) {
    const e = await Products.findOne({
        where: {
            id: productId
        }
    });
    const imageRes = await ProductDetails.findAll({
        attributes: ['productImage'],
        where: {
            productId: e.id
        }
    });
    const images = imageRes.map(e => {
        return e.productImage;
    });
    let discountPercent = 0;
    if (isNumeric(e.discountPrice) && isNumeric(e.price) && parseInt(e.discountPrice) > 0) {
        discountPercent = 100 - ((parseInt(e.discountPrice) / parseInt(e.price)) * 100);
    }

    const isLiked = await isLikedProduct(e.id, customerId);
    return {
        id: e.id,
        name: e.name,
        productLinesId: e.productLinesId,
        price: e.price,
        description: e.description,
        discountPrice: e.discountPrice,
        totalView: e.totalView,
        images: images,
        discountPercent: `${Math.round(discountPercent)}%`,
        brand: e.brands,
        stuff: e.stuff,
        store: e.store,
        isLiked,
        createdAt: e.createdAt,
        updatedAt: e.updatedAt
    };
}

async function isLikedProduct(productId, customerId) {
    const result = await ProductLiked.findAll({
        where: { productId, customerId }
    });
    return result.length > 0;
}
