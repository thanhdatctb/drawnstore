const response = require('../utils/response_api');

const {
    product: Products,
    productDetail: ProductDetails,
    productline: ProductLines,
    productSizes: ProductSizes,
    productColors: ProductColors,
    productLiked: ProductLiked
} = require('../model/');
const { Op } = require('sequelize');
const { isNumeric } = require('../utils/common');

exports.getSaleProducts = async (req, res) => {
    const limit = req.params.limit;
    const customerId = req.params.customerId;

    if (!customerId) {
        response.responseAPI(
            res,
            response.statusCodeBadRequest,
            'customerId is required',
            []
        );
        return;
    }

    if (limit.toLowerCase() !== 'all' && !isNumeric(limit)) {
        response.responseAPI(
            res,
            response.statusCodeBadRequest,
            'Params is \'all\' or is a number',
            []
        );
    } else {
        const products = limit.toLowerCase() === 'all'
            ? await Products.findAll({
                order: [['createdAt', 'DESC']],
                where: {
                    discountPrice: { [Op.gt]: 0 }
                }
            })
            : await Products.findAll({
                limit: parseInt(limit),
                order: [['createdAt', 'DESC']],
                where: {
                    discountPrice: { [Op.gt]: 0 }
                }
            });

        const result = await Promise.all(
            products.map(async e => {
                const imageRes = await ProductDetails.findAll({
                    attributes: ['productImage'],
                    where: {
                        productId: e.id
                    }
                });
                const images = imageRes.map(e => {
                    return e.productImage;
                });
                let discountPercent = 0;
                if (isNumeric(e.discountPrice) && isNumeric(e.price) && parseInt(e.discountPrice) > 0) {
                    discountPercent = 100 - ((parseInt(e.discountPrice) / parseInt(e.price)) * 100);
                }
                const sizes = await getProductSizes(e.id);
                const colors = await getProductColors(e.id);
                const isLiked = await isLikedProduct(e.id, customerId);
                return {
                    id: e.id,
                    name: e.name,
                    productLinesId: e.productLinesId,
                    price: e.price,
                    description: e.description,
                    discountPrice: e.discountPrice,
                    totalView: e.totalView,
                    images: images,
                    discountPercent: `${ Math.round(discountPercent) }%`,
                    brand: e.brands,
                    stuff: e.stuff,
                    store: e.store,
                    isLiked,
                    colors,
                    sizes,
                    createdAt: e.createdAt,
                    updatedAt: e.updatedAt
                };
            })
        );
        response.responseAPI(
            res,
            response.statusCodeSuccess,
            'Success',
            result
        );
    }
};

exports.getHighestProducts = async (req, res) => {
    const limit = req.params.limit;
    const customerId = req.params.customerId;

    if (!customerId) {
        response.responseAPI(
            res,
            response.statusCodeBadRequest,
            'customerId is required',
            []
        );
        return;
    }

    if (limit.toLowerCase() !== 'all' && !isNumeric(limit)) {
        response.responseAPI(
            res,
            response.statusCodeBadRequest,
            'Params is \'all\' or is a number',
            []
        );
    } else {
        const products = limit.toLowerCase() === 'all'
            ? await Products.findAll({
                order: [['totalView', 'DESC']]
            })
            : await Products.findAll({
                limit: parseInt(limit),
                order: [['totalView', 'DESC']]
            });

        const result = await Promise.all(
            products.map(async e => {
                const imageRes = await ProductDetails.findAll({
                    attributes: ['productImage'],
                    where: {
                        productId: e.id
                    }
                });
                const images = imageRes.map(e => {
                    return e.productImage;
                });
                let discountPercent = 0;
                if (isNumeric(e.discountPrice) && isNumeric(e.price) && parseInt(e.discountPrice) > 0) {
                    discountPercent = 100 - ((parseInt(e.discountPrice) / parseInt(e.price)) * 100);
                }
                const sizes = await getProductSizes(e.id);
                const colors = await getProductColors(e.id);
                const isLiked = await isLikedProduct(e.id, customerId);
                return {
                    id: e.id,
                    name: e.name,
                    productLinesId: e.productLinesId,
                    price: e.price,
                    description: e.description,
                    discountPrice: e.discountPrice,
                    totalView: e.totalView,
                    images: images,
                    discountPercent: `${ Math.round(discountPercent) }%`,
                    brand: e.brands,
                    stuff: e.stuff,
                    store: e.store,
                    isLiked,
                    colors,
                    sizes,
                    createdAt: e.createdAt,
                    updatedAt: e.updatedAt
                };
            })
        );
        response.responseAPI(
            res,
            response.statusCodeSuccess,
            'Success',
            result
        );
    }
};

exports.getLatestProducts = async (req, res) => {
    const limit = req.params.limit;
    const customerId = req.params.customerId;

    if (!customerId) {
        response.responseAPI(
            res,
            response.statusCodeBadRequest,
            'customerId is required',
            []
        );
        return;
    }
    if (limit.toLowerCase() !== 'all' && !isNumeric(limit)) {
        response.responseAPI(
            res,
            response.statusCodeBadRequest,
            'Params is \'all\' or is a number',
            []
        );
    } else {
        const products = limit.toLowerCase() === 'all'
            ? await Products.findAll({
                order: [['createdAt', 'DESC']]
            })
            : await Products.findAll({
                limit: parseInt(limit),
                order: [['createdAt', 'DESC']]
            });

        const result = await Promise.all(
            products.map(async e => {
                const imageRes = await ProductDetails.findAll({
                    attributes: ['productImage'],
                    where: {
                        productId: e.id
                    }
                });
                const images = imageRes.map(e => {
                    return e.productImage;
                });
                let discountPercent = 0;
                if (isNumeric(e.discountPrice) && isNumeric(e.price) && parseInt(e.discountPrice) > 0) {
                    discountPercent = 100 - ((parseInt(e.discountPrice) / parseInt(e.price)) * 100);
                }
                const sizes = await getProductSizes(e.id);
                const colors = await getProductColors(e.id);
                const isLiked = await isLikedProduct(e.id, customerId);
                return {
                    id: e.id,
                    name: e.name,
                    productLinesId: e.productLinesId,
                    price: e.price,
                    description: e.description,
                    discountPrice: e.discountPrice,
                    totalView: e.totalView,
                    images: images,
                    discountPercent: `${ Math.round(discountPercent) }%`,
                    brand: e.brands,
                    stuff: e.stuff,
                    store: e.store,
                    isLiked,
                    colors,
                    sizes,
                    createdAt: e.createdAt,
                    updatedAt: e.updatedAt
                };
            })
        );
        response.responseAPI(
            res,
            response.statusCodeSuccess,
            'Success',
            result
        );
    }
};

exports.getProductLines = async (req, res) => {
    ProductLines.findAll().then((data) => {
        response.responseAPI(
            res,
            response.statusCodeSuccess,
            'Success',
            data
        );
    }).catch((err) => {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message,
            []
        );
    });
};

exports.getProductsFollowProductLine = async (req, res) => {
    const productLineId = req.params.productLineId;
    const customerId = req.params.customerId;

    if (!customerId) {
        response.responseAPI(
            res,
            response.statusCodeBadRequest,
            'customerId is required',
            []
        );
        return;
    }
    Products.findAll({
        where: {
            productlineId: productLineId
        }
    }).then(async (products) => {
        const result = await Promise.all(
            products.map(async e => {
                const imageRes = await ProductDetails.findAll({
                    attributes: ['productImage'],
                    where: {
                        productId: e.id
                    }
                });
                const images = imageRes.map(e => {
                    return e.productImage;
                });
                let discountPercent = 0;
                if (isNumeric(e.discountPrice) && isNumeric(e.price) && parseInt(e.discountPrice) > 0) {
                    discountPercent = 100 - ((parseInt(e.discountPrice) / parseInt(e.price)) * 100);
                }
                const sizes = await getProductSizes(e.id);
                const colors = await getProductColors(e.id);
                const isLiked = await isLikedProduct(e.id, customerId);
                return {
                    id: e.id,
                    name: e.name,
                    productLinesId: e.productLinesId,
                    price: e.price,
                    description: e.description,
                    discountPrice: e.discountPrice,
                    totalView: e.totalView,
                    images: images,
                    discountPercent: `${ Math.round(discountPercent) }%`,
                    brand: e.brands,
                    stuff: e.stuff,
                    store: e.store,
                    isLiked,
                    colors,
                    sizes,
                    createdAt: e.createdAt,
                    updatedAt: e.updatedAt
                };
            })
        );
        response.responseAPI(
            res,
            response.statusCodeSuccess,
            'Success',
            result
        );
    }).catch((err) => {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message,
            []
        );
    });
};

exports.getAllProducts = async (req, res) => {
    const customerId = req.params.customerId;

    if (!customerId) {
        response.responseAPI(
            res,
            response.statusCodeBadRequest,
            'customerId is required',
            []
        );
        return;
    }
    Products.findAll().then(async (products) => {
        const result = await Promise.all(
            products.map(async e => {
                const imageRes = await ProductDetails.findAll({
                    attributes: ['productImage'],
                    where: {
                        productId: e.id
                    }
                });
                const images = imageRes.map(e => {
                    return e.productImage;
                });
                let discountPercent = 0;
                if (isNumeric(e.discountPrice) && isNumeric(e.price) && parseInt(e.discountPrice) > 0) {
                    discountPercent = 100 - ((parseInt(e.discountPrice) / parseInt(e.price)) * 100);
                }
                const sizes = await getProductSizes(e.id);
                const colors = await getProductColors(e.id);
                const isLiked = await isLikedProduct(e.id, customerId);
                return {
                    id: e.id,
                    name: e.name,
                    productLinesId: e.productLinesId,
                    price: e.price,
                    description: e.description,
                    discountPrice: e.discountPrice,
                    totalView: e.totalView,
                    images: images,
                    discountPercent: `${ Math.round(discountPercent) }%`,
                    brand: e.brands,
                    stuff: e.stuff,
                    store: e.store,
                    isLiked,
                    colors,
                    sizes,
                    createdAt: e.createdAt,
                    updatedAt: e.updatedAt
                };
            })
        );
        response.responseAPI(
            res,
            response.statusCodeSuccess,
            'Success',
            result
        );
    }).catch((err) => {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message,
            []
        );
    });
};

exports.incrementView = async (req, res) => {
    const productId = req.params.productId;

    Products.findOne({
        where: { id: productId }
    }).then((data) => {
        const newView = parseInt(data.totalView) + 1;
        Products.update(
            { totalView: newView },
            { where: { id: productId } }
        ).then((result) => {
            response.responseAPI(
                res,
                response.statusCodeSuccess,
                'Increment view success',
                null
            );
        });
    }).catch((err) => {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message,
            null
        );
    });
};

async function getProductSizes(productId) {
    const sizes = await ProductSizes.findAll({
        where: { productId: productId },
        order: [['sizes', 'ASC']]
    });
    const result = sizes.map(e => e.sizes);
    return result;
}

async function getProductColors(productId) {
    const colors = await ProductColors.findAll({
        where: { productId: productId },
        order: [['color', 'ASC']]
    });
    const result = colors.map(e => e.color);
    return result;
}

async function isLikedProduct(productId, customerId) {
    const result = await ProductLiked.findAll({
        where: { productId, customerId }
    });
    return result.length > 0;
}
