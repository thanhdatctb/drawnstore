const response = require('../utils/response_api');
const {
    productLiked: ProductLiked,
    product: Products,
    productDetail: ProductDetails,
    productSizes: ProductSizes,
    productColors: ProductColors
} = require('../model/');
const { isNumeric } = require('../utils/common');

exports.addLike = (req, res) => {
    const customerId = req.body.customerId;
    const productId = req.body.productId;

    if (!customerId || !customerId) {
        response.responseAPI(
            res,
            403,
            'Bad request',
            null
        );
    } else {
        ProductLiked.create({
            productId, customerId
        }).then((data) => {
            response.responseAPI(
                res,
                response.statusCodeSuccess,
                'Liked',
                null
            );
        }).catch((err) => {
            response.responseAPI(
                res,
                response.statusCodeServerError,
                err.message,
                null
            );
        });
    }
};

exports.disLike = (req, res) => {
    const customerId = req.body.customerId;
    const productId = req.body.productId;

    if (!customerId || !customerId) {
        response.responseAPI(
            res,
            403,
            'Bad request',
            null
        );
    } else {
        ProductLiked.destroy({
            where: {
                productId, customerId
            }
        }).then((data) => {
            response.responseAPI(
                res,
                response.statusCodeSuccess,
                'Dislike success',
                null
            );
        }).catch((err) => {
            response.responseAPI(
                res,
                response.statusCodeServerError,
                err.message,
                null
            );
        });
    }
};

exports.likes = (req, res) => {
    const customerId = req.params.customerId;
    ProductLiked.findAll({
        where: { customerId }
    }).then(async (data) => {
        const reuslt = await Promise.all(
            data.map(async ele => {
                return await getProductInfo(ele.productId);
            })
        );
        response.responseAPI(
            res,
            response.statusCodeSuccess,
            'Success',
            reuslt
        );
    }).catch((err) => {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message,
            null
        );
    });
};

async function getProductInfo(productId) {
    const products = await Products.findOne({
        where: { id: productId }
    });
    const imageRes = await ProductDetails.findAll({
        attributes: ['productImage'],
        where: {
            productId: products.id
        }
    });
    const images = imageRes.map(e => {
        return e.productImage;
    });
    let discountPercent = 0;
    if (isNumeric(products.discountPrice) && isNumeric(products.price) && parseInt(products.discountPrice) > 0) {
        discountPercent = 100 - ((parseInt(products.discountPrice) / parseInt(products.price)) * 100);
    }
    const sizes = await getProductSizes(products.id);
    const colors = await getProductColors(products.id);
    const result = {
        id: products.id,
        name: products.name,
        productLinesId: products.productLinesId,
        price: products.price,
        description: products.description,
        discountPrice: products.discountPrice,
        totalView: products.totalView,
        images: images,
        discountPercent: `${ Math.round(discountPercent) }%`,
        brand: products.brands,
        stuff: products.stuff,
        store: products.store,
        isLiked: true,
        colors,
        sizes,
        createdAt: products.createdAt,
        updatedAt: products.updatedAt
    };

    return result;
}

async function getProductSizes(productId) {
    const sizes = await ProductSizes.findAll({
        where: { productId: productId },
        order: [['sizes', 'ASC']]
    });
    const result = sizes.map(e => e.sizes);
    return result;
}

async function getProductColors(productId) {
    const colors = await ProductColors.findAll({
        where: { productId: productId },
        order: [['color', 'ASC']]
    });
    const result = colors.map(e => e.color);
    return result;
}
