const response = require('../utils/response_api');
const {
    customerOrderInfo: CustomerOrderInfo
} = require('../model/');
const fs = require('fs');

exports.addAccountOderInfo = async (req, res) => {
    const request = req.body;
    // validate request
    if (!request) {
        response.responseAPI(
            res,
            response.statusCodeBadRequest,
            'Content can not be empty!',
            null
        );
    }
    if (request.isDefault === true) {
        await CustomerOrderInfo.update({ isDefault: false }, { where: { customerId: request.userId } });
    }
    const orderInfo = {
        customerId: request.userId,
        name: request.name,
        phoneNumber: request.phoneNumber,
        address: request.address,
        isDefault: request.isDefault,
        districtCode: request.districtCode,
        provinceCode: request.provinceCode
    };

    CustomerOrderInfo.create(orderInfo).then((data) => {
        response.responseAPI(
            res,
            response.statusCodeSuccess,
            'Success',
            data
        );
    }).catch((err) => {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message || '',
            null
        );
    });
};

exports.updateAccountOderInfo = async (req, res) => {
    const request = req.body;
    // validate request
    if (!request) {
        response.responseAPI(
            res,
            response.statusCodeBadRequest,
            'Content can not be empty!',
            null
        );
    }

    if (request.isDefault === true) {
        await CustomerOrderInfo.update({ isDefault: false }, { where: { customerId: request.userId } });
    }

    CustomerOrderInfo.findOne({
        where: { id: request.id }
    }).then(async (data) => {
        if (data != null) {
            const orderInfo = {
                customerId: request.userId ? request.userId : data.customerId,
                name: request.name ? request.name : data.name,
                phoneNumber: request.phoneNumber ? request.phoneNumber : data.phoneNumber,
                address: request.address ? request.address : data.address,
                provinceCode: request.provinceCode ? request.provinceCode : data.provinceCode,
                districtCode: request.districtCode ? request.districtCode : data.districtCode,
                isDefault: request.isDefault !== 'undefined' ? request.isDefault : data.isDefault
            };
            CustomerOrderInfo.update(orderInfo, { where: { id: request.id } }).then((restult) => {
                response.responseAPI(
                    res,
                    response.statusCodeSuccess,
                    'Success',
                    orderInfo
                );
            }).catch((err) => {
                response.responseAPI(
                    res,
                    response.statusCodeError,
                    err.message,
                    null
                );
            });
        } else {
            response.responseAPI(
                res,
                response.statusCodeNotFound,
                'Not found!',
                null
            );
        }
    }).catch((err) => {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message || '',
            null
        );
    });
};

exports.getAccountOderInfo = (req, res) => {
    const request = req.params;
    // validate request
    if (!request) {
        response.responseAPI(
            res,
            response.statusCodeBadRequest,
            'Content can not be empty!',
            null
        );
    }

    CustomerOrderInfo.findAll({
        where: { customerId: request.userId }
    }).then((data) => {
        if (data.length > 0) {
            response.responseAPI(
                res,
                response.statusCodeSuccess,
                'Success',
                data
            );
        } else {
            response.responseAPI(
                res,
                response.statusCodeNotFound,
                'List info is empty',
                data
            );
        }
    }).catch((err) => {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message || '',
            null
        );
    });
};

exports.deleteAccountOderInfo = (req, res) => {
    const request = req.params;
    // validate request
    if (!request) {
        response.responseAPI(
            res,
            response.statusCodeBadRequest,
            'Content can not be empty!',
            null
        );
    }

    CustomerOrderInfo.destroy({
        where: { id: request.infoId }
    }).then((data) => {
        response.responseAPI(
            res,
            response.statusCodeSuccess,
            'Success',
            null
        );
    }).catch((err) => {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message || '',
            null
        );
    });
};

exports.getProvinceList = (req, res) => {
    try {
        const provinces = fs.readFileSync('./src/public/db/province.json');
        const provinceJson = JSON.parse(provinces);
        response.responseAPI(
            res,
            response.statusCodeSuccess,
            'Sucess',
            provinceJson
        );
    } catch (err) {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message,
            null
        );
    }
};

exports.getDistrictList = (req, res) => {
    const parentCode = req.params.code;
    try {
        const districts = fs.readFileSync('./src/public/db/district.json');
        const districtsJson = JSON.parse(districts);
        const reuslt = districtsJson.filter(e => e.parent_code === parentCode);
        response.responseAPI(
            res,
            response.statusCodeSuccess,
            'Sucess',
            reuslt
        );
    } catch (err) {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message,
            null
        );
    }
};

exports.getWardsList = (req, res) => {
    const parentCode = req.params.code;
    try {
        const wards = fs.readFileSync('./src/public/db/wards.json');
        const wardsJson = JSON.parse(wards);
        const reuslt = wardsJson.filter(e => e.parent_code === parentCode);
        response.responseAPI(
            res,
            response.statusCodeSuccess,
            'Sucess',
            reuslt
        );
    } catch (err) {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message,
            null
        );
    }
};
