const response = require('../utils/response_api');
const Resize = require('../utils/Resize');
const path = require('path');
const fs = require('fs');

const axios = require('axios');
const urlTranslateAPI = 'https://translate.googleapis.com/translate_a/single?client=gtx&sl=';
const cloudsight = require('cloudsight')({
    apikey: process.env.CLOUD_SIGHT_API_KEY
});
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const {
    product: Products,
    productDetail: ProductDetails,
    productSizes: ProductSizes,
    productColors: ProductColors,
    productLiked: ProductLiked
} = require('../model/');
const { isNumeric } = require('../utils/common');

exports.searchWithImage = async (req, res) => {
    const request = req.body;
    // validate request
    if (!request) {
        response.responseAPI(
            res,
            response.statusCodeBadRequest,
            'Content can not be empty!',
            null
        );
    }
    const fileNameRaw = req.file.originalname;
    const imagePath = path.join(__dirname, '../public/images/images-search');
    const fileUpload = new Resize(imagePath);
    const filename = await fileUpload.save(req.file.buffer, `image-${ fileNameRaw.substring(0, fileNameRaw.length - 4) }-`);

    // Describe image
    const image = {
        image: `./src/public/images/images-search/${ filename }`,
        locale: 'en-US'
    };

    // Upload image to cloudsight and detect
    cloudsight.request(image, myCallback);

    async function myCallback(err, data) {
        if (err) {
            console.log(err);
            return;
        }
        if (data.status !== 'completed') {
            // Describe image
            const image = {
                image: `./src/public/images/images-search/${ filename }`,
                locale: 'en-US'
            };
            cloudsight.request(image, myCallback);
        } else {
            if (filename) {
                // delete old thumbnail
                fs.unlinkSync(`./src/public/images/images-search/${ filename }`, (err) => {
                    if (err) {
                        console.error(err);
                    }
                    // file removed
                });
            }
            const configTranslateTitle = urlTranslateAPI + 'en' + '&tl=' + 'vi' + '&dt=t&q=' +
        encodeURI(data.name);

            const colors = [];
            const genders = [];
            if (data.structured_output) {
                if (Array.isArray(data.structured_output.color)) {
                    data.structured_output.color.forEach(async e => {
                        const configTranslateColor = urlTranslateAPI + 'en' + '&tl=' + 'vi' + '&dt=t&q=' +
                encodeURI(e);

                        const titleTranslateResult = await axios.get(configTranslateColor)
                            .catch(function(error) {
                                console.log(error);
                            });

                        colors.push(titleTranslateResult.data[0][0][0]);
                    });
                }
                if (Array.isArray(data.structured_output.gender)) {
                    data.structured_output.gender.forEach(async e => {
                        const configTranslateGender = urlTranslateAPI + 'en' + '&tl=' + 'vi' + '&dt=t&q=' +
                encodeURI(e);

                        const genderTranslateResult = await axios.get(configTranslateGender)
                            .catch(function(error) {
                                console.log(error);
                            });

                        genders.push(genderTranslateResult.data[0][0][0]);
                    });
                }
            }
            const titleTranslateResult = await axios.get(configTranslateTitle)
                .catch(function(error) {
                    console.log(error);
                });

            const recognitionResult = {
                detected: titleTranslateResult.data[0][0][0],
                colors,
                genders
            };

            const productTypes = [
                'quần', 'áo', 'giày', 'dép', 'ủng', 'tất', 'mũ', 'nón', 'khẩu trang',
                'kẹp tóc', 'nhẫn', 'bông tai', 'hoa tai', 'son', 'túi xách', 'giỏ xách',
                'váy', 'đầm', 'đồ ngủ'
            ];

            let recognitionResultName;
            let productTypeResult;

            for (const productType of productTypes) {
                if (recognitionResult.detected.includes(productType)) {
                    productTypeResult = productType;
                    const nameSplit = recognitionResult.detected.split(productType)[1].split(' ');
                    let name = '';
                    for (let index = 0; index < 3; index++) {
                        name += nameSplit[index] + ' ';
                    }
                    recognitionResultName = `${ productType }${ name }`;
                    break;
                }
            }

            let result = [];

            Products.findAll({
                where: {
                    name: {
                        [Op.like]: `%${ recognitionResultName }%`
                    }
                }
            }).then(async data => {
                if (data.length === 0) {
                    const abc = await Products.findAll({
                        where: {
                            name: {
                                [Op.like]: `%${ productTypeResult }%`
                            }
                        }
                    });
                    result = await Promise.all(abc.map(async e => {
                        const imageRes = await ProductDetails.findAll({
                            attributes: ['productImage'],
                            where: {
                                productId: e.id
                            }
                        });
                        const images = imageRes.map(e => {
                            return e.productImage;
                        });
                        let discountPercent = 0;
                        if (isNumeric(e.discountPrice) && isNumeric(e.price) && parseInt(e.discountPrice) > 0) {
                            discountPercent = 100 - ((parseInt(e.discountPrice) / parseInt(e.price)) * 100);
                        }
                        const sizes = await getProductSizes(e.id);
                        const colors = await getProductColors(e.id);
                        const isLiked = await isLikedProduct(e.id, request.customerId);
                        return {
                            id: e.id,
                            name: e.name,
                            productLinesId: e.productLinesId,
                            price: e.price,
                            description: e.description,
                            discountPrice: e.discountPrice,
                            totalView: e.totalView,
                            images: images,
                            discountPercent: `${ Math.round(discountPercent) }%`,
                            brand: e.brands,
                            stuff: e.stuff,
                            store: e.store,
                            isLiked,
                            colors,
                            sizes,
                            createdAt: e.createdAt,
                            updatedAt: e.updatedAt
                        };
                    }));

                    const resultWithFilterColor = result.filter(e => e.colors.includes(colors[0].replace('màu', '').trim()));

                    const finalresult = {
                        ...recognitionResult,
                        results: colors.length > 0 ? resultWithFilterColor : result
                    };
                    response.responseAPI(
                        res,
                        response.statusCodeSuccess,
                        'Success',
                        finalresult
                    );
                } else {
                    result = await Promise.all(data.map(async e => {
                        const imageRes = await ProductDetails.findAll({
                            attributes: ['productImage'],
                            where: {
                                productId: e.id
                            }
                        });
                        const images = imageRes.map(e => {
                            return e.productImage;
                        });
                        let discountPercent = 0;
                        if (isNumeric(e.discountPrice) && isNumeric(e.price) && parseInt(e.discountPrice) > 0) {
                            discountPercent = 100 - ((parseInt(e.discountPrice) / parseInt(e.price)) * 100);
                        }
                        const sizes = await getProductSizes(e.id);
                        const colors = await getProductColors(e.id);
                        const isLiked = await isLikedProduct(e.id, request.customerId);
                        return {
                            id: e.id,
                            name: e.name,
                            productLinesId: e.productLinesId,
                            price: e.price,
                            description: e.description,
                            discountPrice: e.discountPrice,
                            totalView: e.totalView,
                            images: images,
                            discountPercent: `${ Math.round(discountPercent) }%`,
                            brand: e.brands,
                            stuff: e.stuff,
                            store: e.store,
                            isLiked,
                            colors,
                            sizes,
                            createdAt: e.createdAt,
                            updatedAt: e.updatedAt
                        };
                    }));

                    const resultWithFilterColor = result.filter(e => e.colors.includes(colors[0].replace('màu', '').trim()));

                    const finalresult = {
                        ...recognitionResult,
                        results: colors.length > 0 ? resultWithFilterColor : result
                    };
                    response.responseAPI(
                        res,
                        response.statusCodeSuccess,
                        'Success',
                        finalresult
                    );
                }
            });

            // console.log('myCallback -> data', data);
        }
    }
};

async function getProductSizes(productId) {
    const sizes = await ProductSizes.findAll({
        where: { productId: productId },
        order: [['sizes', 'ASC']]
    });
    const result = sizes.map(e => e.sizes);
    return result;
}

async function getProductColors(productId) {
    const colors = await ProductColors.findAll({
        where: { productId: productId },
        order: [['color', 'ASC']]
    });
    const result = colors.map(e => e.color);
    return result;
}

async function isLikedProduct(productId, customerId) {
    const result = await ProductLiked.findAll({
        where: { productId, customerId }
    });
    return result.length > 0;
}
