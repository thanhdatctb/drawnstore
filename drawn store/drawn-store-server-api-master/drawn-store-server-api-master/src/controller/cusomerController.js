const response = require('../utils/response_api');
const md5 = require('md5');
const {
    customer: Customers
} = require('../model/');
const nodemailer = require('nodemailer');
const Resize = require('../utils/Resize');
const path = require('path');
const fs = require('fs');

exports.register = async (req, res) => {
    const paramBody = req.body;

    if (!paramBody.name || !paramBody.email || !paramBody.password) {
        response.responseAPI(
            res,
            403,
            'Bad request',
            null
        );
    } else {
        const name = paramBody.name;
        const email = paramBody.email;
        const password = md5(paramBody.password);
        const avatar = paramBody.avatar;
        Customers.findOne({
            attributes: ['id'],
            where: {
                email: email
            }
        }).then((user) => {
            if (user != null) {
                response.responseAPI(
                    res,
                    response.statusCodeBadRequest,
                    'This email already exist!',
                    null
                );
            } else {
                Customers.create(
                    {
                        name: name,
                        email: email,
                        password: password,
                        avatar: avatar
                    }
                ).then((result) => {
                    response.responseAPI(
                        res,
                        200,
                        'Register Success',
                        {
                            id: result.id,
                            name: result.name,
                            email: result.email,
                            avatar: result.avatar,
                            createdAt: result.createdAt,
                            updatedAt: result.updatedAt
                        }
                    );
                }).catch((err) => {
                    response.responseAPI(
                        res,
                        500,
                        err.message,
                        null
                    );
                });
            }
        }).catch((err) => {
            response.responseAPI(
                res,
                500,
                err.message,
                null
            );
        });
    }
};

exports.getUserInfo = async (req, res) => {
    const userId = req.params.userId;
    if (!userId) {
        response.responseAPI(
            res,
            403,
            'Bad request',
            null
        );
    } else {
        Customers.findOne({
            attributes: ['id', 'name', 'email', 'avatar', 'createdAt', 'updatedAt'],
            where: {
                id: userId
            }
        }).then((user) => {
            if (user != null) {
                response.responseAPI(
                    res,
                    response.statusCodeSuccess,
                    'Success',
                    user
                );
            } else {
                response.responseAPI(
                    res,
                    response.statusCodeNotFound,
                    'User not found!',
                    null
                );
            }
        }).catch((err) => {
            response.responseAPI(
                res,
                500,
                err.message,
                null
            );
        });
    }
};

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

exports.fogotPassword = async (req, res) => {
    const email = req.body.email;
    if (!email) {
        response.responseAPI(
            res,
            403,
            'Bad request',
            null
        );
    } else {
        Customers.findOne({
            where: {
                email: email
            }
        }).then(async (user) => {
            if (user != null) {
                const theCode = Math.round(getRandomArbitrary(1000, 9999));
                var transporter = nodemailer.createTransport({ // config mail server
                    service: 'Gmail',
                    auth: {
                        user: 'dawnshop1997@gmail.com',
                        pass: 'binhminh1997'
                    }
                });
                var mainOptions = { // thiết lập đối tượng, nội dung gửi mail
                    from: 'Drawn Store',
                    to: email,
                    subject: 'Drawn Store - Reset your passowrd',
                    text: 'You recieved message from Drawn Store',
                    html: '<h3>You are requesting to reset password.</h3>' + `<h4>The code to reset your password is:</h4> <h2>${ theCode }</h2>`
                };
                transporter.sendMail(mainOptions, function(err, info) {
                    if (err) {
                        response.responseAPI(
                            res,
                            response.statusCodeError,
                            'Can not send email to ' + email,
                            null
                        );
                    } else {
                        response.responseAPI(
                            res,
                            response.statusCodeSuccess,
                            'Send mail success',
                            {
                                message: 'An email have been send to ' + email + ' please check your email',
                                code: theCode
                            }
                        );
                    }
                });
            } else {
                response.responseAPI(
                    res,
                    response.statusCodeNotFound,
                    'User not found!',
                    null
                );
            }
        }).catch((err) => {
            response.responseAPI(
                res,
                500,
                err.message,
                null
            );
        });
    }
};

exports.changePassword = async (req, res) => {
    const email = req.body.email;
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;
    if (!email || !oldPassword || !newPassword) {
        response.responseAPI(
            res,
            403,
            'Bad request',
            null
        );
    } else {
        Customers.findOne({
            attributes: ['id'],
            where: {
                email: email,
                password: md5(oldPassword)
            }
        }).then(async (user) => {
            if (user != null) {
                Customers.update(
                    { password: md5(newPassword) },
                    { where: { email } }
                ).then((result) => {
                    response.responseAPI(
                        res,
                        response.statusCodeSuccess,
                        'Update success',
                        null
                    );
                }).catch((err) => {
                    response.responseAPI(
                        res,
                        err.message,
                        'Can not update your password',
                        null
                    );
                });
            } else {
                response.responseAPI(
                    res,
                    response.statusCodeNotFound,
                    'Old password incorrect',
                    null
                );
            }
        }).catch((err) => {
            response.responseAPI(
                res,
                500,
                err.message,
                null
            );
        });
    }
};

exports.fogotPasswordChange = async (req, res) => {
    const email = req.body.email;
    const newPassword = req.body.newPassword;
    if (!email || !newPassword) {
        response.responseAPI(
            res,
            403,
            'Bad request',
            null
        );
    } else {
        Customers.findOne({
            attributes: ['id'],
            where: {
                email: email
            }
        }).then(async (user) => {
            if (user != null) {
                Customers.update(
                    { password: md5(newPassword) },
                    { where: { email } }
                ).then((result) => {
                    response.responseAPI(
                        res,
                        response.statusCodeSuccess,
                        'Update success',
                        null
                    );
                }).catch((err) => {
                    response.responseAPI(
                        res,
                        err.message,
                        'Can not update your password',
                        null
                    );
                });
            } else {
                response.responseAPI(
                    res,
                    response.statusCodeNotFound,
                    'Account does not exist',
                    null
                );
            }
        }).catch((err) => {
            response.responseAPI(
                res,
                500,
                err.message,
                null
            );
        });
    }
};

exports.updateAccountInfo = (req, res) => {
    const request = req.body;
    // validate request
    if (!request) {
        response.responseAPI(
            res,
            response.statusCodeBadRequest,
            'Content can not be empty!',
            null
        );
    }

    let avatarUrl;

    Customers.findOne({
        where: { email: request.email }
    }).then(async (data) => {
        if (data != null) {
            if (!req.file || !request.email) {
                avatarUrl = '';
            } else {
                if (data.avatar) {
                    // delete old thumbnail
                    fs.unlinkSync('./src/public/' + data.avatar, (err) => {
                        if (err) {
                            console.error(err);
                        }
                        // file removed
                    });
                }
                const imagePath = path.join(__dirname, '../public/images');
                const fileUpload = new Resize(imagePath);
                const filename = await fileUpload.save(req.file.buffer, 'avatar');
                avatarUrl = 'images/' + filename;
            }
            const dish = {
                name: request.name ? request.name : data.name,
                avatar: avatarUrl === '' ? data.avatar : avatarUrl
            };
            Customers.update(dish, { where: { email: request.email } }).then((restult) => {
                response.responseAPI(
                    res,
                    response.statusCodeSuccess,
                    'Success',
                    dish
                );
            }).catch((err) => {
                response.responseAPI(
                    res,
                    response.statusCodeError,
                    'Can not update',
                    err
                );
            });
        } else {
            response.responseAPI(
                res,
                response.statusCodeNotFound,
                'User not found!',
                null
            );
        }
    }).catch((err) => {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message || '',
            null
        );
    });
};

exports.deleteAccount = (req, res) => {
    const userId = req.params.userId;

    Customers.destroy({
        where: { id: userId }
    }).then(async (data) => {
        if (data != null) {
            response.responseAPI(
                res,
                response.statusCodeSuccess,
                'Delete success',
                null
            );
        } else {
            response.responseAPI(
                res,
                response.statusCodeNotFound,
                'User not found!',
                null
            );
        }
    }).catch((err) => {
        response.responseAPI(
            res,
            response.statusCodeServerError,
            err.message || '',
            null
        );
    });
};
