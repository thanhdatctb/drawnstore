const response = require('../utils/response_api');
const {
    ratingStar: RatingStars,
    customer: Customers
} = require('../model/');

exports.ratting = (req, res) => {
    const customerId = req.body.customerId;
    const productId = req.body.productId;
    const stars = req.body.stars;
    const comments = req.body.comments;

    if (!customerId || !productId || !stars || !comments) {
        response.responseAPI(
            res,
            403,
            'Bad request',
            null
        );
    } else {
        RatingStars.findOne({
            where: {
                productId, customerId
            }
        }).then(data => {
            if (data != null) {
                RatingStars.update(
                    { stars, comments },
                    {
                        where: {
                            productId, customerId
                        }
                    }
                ).then((data) => {
                    response.responseAPI(
                        res,
                        response.statusCodeSuccess,
                        'Ratting success',
                        null
                    );
                });
            } else {
                RatingStars.create({
                    productId, customerId, stars, comments
                }).then((data) => {
                    response.responseAPI(
                        res,
                        response.statusCodeSuccess,
                        'Ratting success',
                        null
                    );
                });
            }
        }).catch((err) => {
            response.responseAPI(
                res,
                response.statusCodeServerError,
                err.message,
                null
            );
        });
    }
};

exports.getRatting = (req, res) => {
    const productId = req.params.productId;

    if (!productId) {
        response.responseAPI(
            res,
            403,
            'Bad request',
            null
        );
    } else {
        RatingStars.findAll({
            where: {
                productId
            }
        }).then(async data => {
            if (data != null) {
                const reuslt = await Promise.all(
                    data.map(async ele => {
                        const userInfo = await getCustomerInfo(ele.customerId);
                        return {
                            name: userInfo.name,
                            avatarUrl: userInfo.avatar,
                            stars: parseFloat(ele.stars),
                            comment: ele.comments
                        };
                    })
                );
                response.responseAPI(
                    res,
                    response.statusCodeSuccess,
                    'Success',
                    reuslt
                );
            } else {
                response.responseAPI(
                    res,
                    response.statusCodeNotFound,
                    'Not found',
                    []
                );
            }
        }).catch((err) => {
            response.responseAPI(
                res,
                response.statusCodeServerError,
                err.message,
                null
            );
        });
    }
};

async function getCustomerInfo(customerId) {
    const customer = await Customers.findOne({
        attributes: ['id', 'name', 'avatar'],
        where: { id: customerId }
    });

    return customer;
}
