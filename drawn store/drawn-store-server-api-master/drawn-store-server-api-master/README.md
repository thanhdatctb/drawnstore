# Drawn Store Server API

## Install docker-compose
- Run commands:
1. `sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)"  -o /usr/local/bin/docker-compose`
2. `sudo mv /usr/local/bin/docker-compose /usr/bin/docker-compose`
3. `sudo chmod +x /usr/bin/docker-compose`

## Config environment
1. Create `.env` file from `.env.example` - change `RUN_ENV` value to 'dev' or 'prod'.
2. Run command `npm install`.
3. Run command `docker-compose up` (waitting for this process is finish)
4. Open another commandline and run `docker-compose start`
5. Run command `npm start`.